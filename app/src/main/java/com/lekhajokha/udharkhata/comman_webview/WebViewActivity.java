package com.lekhajokha.udharkhata.comman_webview;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class WebViewActivity extends BaseActivity {

    @BindView(R.id.imageViewBackButton)
    ImageView imageViewBackButton;
    @BindView(R.id.textViewToolbarTitle)
    TextView textViewToolbarTitle;
    @BindView(R.id.webView)
    WebView webView;
    private Unbinder unbinder ;
    private String url , title ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        unbinder = ButterKnife.bind(this);

        url = getIntent().getStringExtra("url");
        title = getIntent().getStringExtra("title");
        setWebView();

    }


    private void setWebView() {
        textViewToolbarTitle.setText(title);
        webView.setWebViewClient(new MyWebViewClient(this));
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.loadUrl(url);
    }

    @OnClick(R.id.imageViewBackButton)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
