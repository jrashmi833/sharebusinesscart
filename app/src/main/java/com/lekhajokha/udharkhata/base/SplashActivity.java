package com.lekhajokha.udharkhata.base;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.login_screen.LoginThroughActivity;
import com.lekhajokha.udharkhata.sidemenu.security.PasswordActivity;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView( R.layout.activity_splash );


        new Handler().postDelayed( new Runnable() {
            @Override
            public void run() {
                if (!SharedPreference.getInstance(SplashActivity.this).getBoolean(Constant.IS_USER_LOGIN, false)) {
                    Intent i = new Intent(SplashActivity.this, LoginThroughActivity.class);
                    startActivity(i);
                    AppUtils.startFromRightToLeft(SplashActivity.this);
                    finish();
                } else {

                   if (SharedPreference.getInstance(SplashActivity.this).getBoolean(Constant.AppLock,false)){
                       Intent i = new Intent(SplashActivity.this, PasswordActivity.class);
                       AppUtils.startFromRightToLeft(SplashActivity.this);
                       i.putExtra( Constant.WHERE_FROM, Constant.AppLock );
                       startActivity(i);
                       finish();
                   }
                   else {
                       Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                       intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                       AppUtils.startFromRightToLeft(SplashActivity.this);
                       startActivity(intent);
                   }
                }
            }

        },3000);

    }
}
