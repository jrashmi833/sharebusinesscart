package com.lekhajokha.udharkhata.base;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.lekhajokha.udharkhata.custom_view.ProgressDialog;
import com.lekhajokha.udharkhata.utility.Constant;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.RequestBody;

@SuppressLint("SimpleDateFormat")
public class BaseActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;


    public RequestBody getRequestBody(String value) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), value);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(BaseActivity.this);
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void hideLoader() {
        progressDialog.dismiss();
    }

    public void showLoader() {
        progressDialog.show();
    }

    public void showLog(String tag, String msg) {
        Log.v(tag,msg);
    }

   /* public void showDailogForError(String msg) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(msg);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void showDailogForWarnings(String msg) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(msg);
        pDialog.setCancelable(false);
        pDialog.show();
    }*/

    public boolean isInternetConneted() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting() && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        }
        return false;
    }

    public File getUserImageFile(Bitmap bitmap) {
        try {
            File f = new File( getCacheDir(), "images.png" );
            f.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress( Bitmap.CompressFormat.JPEG, 80 /*ignored for PNG*/, bos );
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream( f );
            fos.write( bitmapdata );
            fos.flush();
            fos.close();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String convertIntoServerDateTimeFormat (String inputDateString){
        String outputStringDate = inputDateString; ;
        try {
            outputStringDate = Constant.ServerDateTimeFormat.format(Objects.requireNonNull(Constant.DisplayFormat.parse(inputDateString)));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputStringDate ;
    }



}
