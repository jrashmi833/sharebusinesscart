package com.lekhajokha.udharkhata.base;

/**
 * Created by Ravi Thakur on 9/12/2018.
 */

public interface BaseInterface {
    void onError(String errorMessage);
    void onShowLoader();
    void onHideLoader();

}
