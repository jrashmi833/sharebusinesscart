package com.lekhajokha.udharkhata.login_screen;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.SignUp.LoginSignUpPresenter;
import com.lekhajokha.udharkhata.SignUp.LoginSignUpView;
import com.lekhajokha.udharkhata.SignUp.model.GetSignUpResponse;
import com.lekhajokha.udharkhata.SignUp.model.SignUpParameter;
import com.lekhajokha.udharkhata.base.BaseActivity;
import com.lekhajokha.udharkhata.registration.RegistrationActivity;
import com.lekhajokha.udharkhata.login_screen.adapter.ImageAdapter;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;
import com.truecaller.android.sdk.ITrueCallback;
import com.truecaller.android.sdk.TrueError;
import com.truecaller.android.sdk.TrueException;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.android.sdk.TruecallerSDK;
import com.truecaller.android.sdk.TruecallerSdkScope;
import com.truecaller.android.sdk.clients.VerificationCallback;
import com.truecaller.android.sdk.clients.VerificationDataBundle;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import me.relex.circleindicator.CircleIndicator;

import static android.os.Build.VERSION_CODES.M;
import static com.lekhajokha.udharkhata.contact_list.ContactListFragment.PERMISSION_REQUEST_CODE;

public class LoginThroughActivity extends BaseActivity implements LoginSignUpView {

    private static final int RC_SIGN_IN = 101;
    private static final String TAG = LoginThroughActivity.class.getSimpleName();
    @BindView(R.id.viewPage)
    ViewPager viewPage;
    @BindView(R.id.linearPhone)
    LinearLayout linearPhone;
    @BindView(R.id.lineartruecaller)
    LinearLayout lineartruecaller;
    @BindView(R.id.linearGoogle)
    LinearLayout linearGoogle;
    @BindView(R.id.spring_dots_indicator)
    CircleIndicator springDotsIndicator;
    GoogleSignInClient mGoogleSignInClient;
    private LoginSignUpPresenter loginSignUpPresenter;
    String fcmTOken;
    private Unbinder unbinder ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_through);
        unbinder = ButterKnife.bind(this);
        ImageAdapter adapterView = new ImageAdapter(this);
        viewPage.setAdapter(adapterView);

        springDotsIndicator.setViewPager(viewPage);
        TruecallerSdkScope trueScope = new TruecallerSdkScope.Builder(this, sdkCallback)
                .consentMode(TruecallerSdkScope.CONSENT_MODE_POPUP)
                .consentTitleOption(TruecallerSdkScope.SDK_CONSENT_TITLE_VERIFY)
                .footerType(TruecallerSdkScope.FOOTER_TYPE_SKIP)
                .sdkOptions(TruecallerSdkScope.SDK_OPTION_WITH_OTP)
                .build();

        TruecallerSDK.init(trueScope);

      /*  GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();*/

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(LoginThroughActivity.this.getResources().getString(R.string.server_client_id))
                .requestEmail().build();

        fcmTOken = FirebaseInstanceId.getInstance().getToken() ;
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        loginSignUpPresenter = new LoginSignUpPresenter(this,LoginThroughActivity.this);
        if (Build.VERSION.SDK_INT >= M) {
            if (!checkreadContactPermission()) {
                requestContactPermission();
            }
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TruecallerSDK.getInstance().onActivityResultObtained(this, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
                try {
                    Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                    handleSignInResult(task);
                } catch (Exception e) {
                    // The ApiException status code indicates the detailed failure reason.
                    e.printStackTrace();
                }

        }
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            onLoggedIn(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            Toast.makeText(this, e.getStatusCode(), Toast.LENGTH_SHORT).show();
           // updateUI(null);
        }


    }
    private final ITrueCallback sdkCallback = new ITrueCallback() {

        @Override
        public void onSuccessProfileShared(@NonNull final TrueProfile trueProfile) {

           /* SharedPreference.getInstance(LoginThroughActivity.this).setBoolean(Constant.IS_USER_LOGIN, true);
            Intent intent = new Intent(LoginThroughActivity.this, HomeActivity.class);
            intent.putExtra(Constant.PHONE, trueProfile.phoneNumber);
            intent.putExtra(Constant.NAME, trueProfile.firstName + " " + trueProfile.lastName);
            intent.putExtra(Constant.EMAIL, trueProfile.email);
            intent.putExtra(Constant.ABOUT, trueProfile.jobTitle);
            intent.putExtra(Constant.PROFILE, trueProfile.avatarUrl);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/

           SignUpParameter signUpParameter = new SignUpParameter();
           signUpParameter.setPhone(trueProfile.phoneNumber);
           signUpParameter.setFullname(trueProfile.firstName+" "+trueProfile.lastName);
           signUpParameter.setEmail(trueProfile.email);
           signUpParameter.setAbout(trueProfile.jobTitle);
           signUpParameter.setAddress(trueProfile.street);
           signUpParameter.setLoginType("truecaller");
           signUpParameter.setAvatar(trueProfile.avatarUrl);
           signUpParameter.setDeviceType("1");
           signUpParameter.setFcmToken(fcmTOken);
           signUpParameter.setPhoneCode(trueProfile.countryCode);
           loginSignUpPresenter.callSendOtpApi(signUpParameter,LoginThroughActivity.this);
        }

        @Override
        public void onFailureProfileShared(@NonNull final TrueError trueError) {

          //  Toast.makeText(LoginThroughActivity.this, "error code - " + trueError.getErrorType(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onVerificationRequired() {
            //  TruecallerSDK.getInstance().requestVerification("IN", editTextNumber.getText().toString(), apiCallback);
            Intent intent = new Intent(LoginThroughActivity.this, RegistrationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            AppUtils.startFromRightToLeft(LoginThroughActivity.this);
        }

    };

    final VerificationCallback apiCallback = new VerificationCallback() {


        @Override
        public void onRequestSuccess(int requestCode, @Nullable VerificationDataBundle verificationDataBundle) {
            if (requestCode == VerificationCallback.TYPE_MISSED_CALL_INITIATED) {

                // This method is invoked when the missed call has been triggered successfully to the input mobile
                // number. You can now ask the user to input his first name and last name
                Toast.makeText(LoginThroughActivity.this, "Missed Call Triggered", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onRequestFailure(final int requestCode, @NonNull final TrueException e) {
            Toast.makeText(LoginThroughActivity.this, e.getExceptionMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    @OnClick({R.id.linearPhone, R.id.linearGoogle})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linearPhone:
//                if (TruecallerSDK.getInstance().isUsable()) {
//                    TruecallerSDK.getInstance().getUserProfile(this);
//                } else {
                    Intent intent = new Intent(LoginThroughActivity.this, RegistrationActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    AppUtils.startFromRightToLeft(LoginThroughActivity.this);
//                }
                break;

            case R.id.linearGoogle:
                signIn();
                break;
        }
    }
    @RequiresApi(api = M)
    private void requestContactPermission() {
        requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSION_REQUEST_CODE);
    }


    private boolean checkreadContactPermission() {
        int result2 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }
    private void signIn() {

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
        AppUtils.startFromRightToLeft(LoginThroughActivity.this);
    }


    private void onLoggedIn(GoogleSignInAccount account) {
        SignUpParameter signUpParameter = new SignUpParameter();
        signUpParameter.setFullname(account.getDisplayName());
        signUpParameter.setEmail(account.getEmail());
        signUpParameter.setFcmToken(fcmTOken);
        signUpParameter.setLoginType("google");
        signUpParameter.setAvatar(String.valueOf(account.getPhotoUrl()));
        signUpParameter.setGoogleId(account.getId());
        signUpParameter.setDeviceType("1");
        loginSignUpPresenter.callSendOtpApi(signUpParameter,LoginThroughActivity.this);

    }

    @Override
    public void onSuccessSignUp(GetSignUpResponse getSignUpResponse) {
        SharedPreference.getInstance(LoginThroughActivity.this).setBoolean(Constant.IS_USER_LOGIN, true);
        SharedPreference.getInstance(this).putUser(Constant.USER,getSignUpResponse.getData());
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        AppUtils.startFromRightToLeft(LoginThroughActivity.this);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {

    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
