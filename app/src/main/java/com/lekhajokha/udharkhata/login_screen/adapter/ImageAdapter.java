package com.lekhajokha.udharkhata.login_screen.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.lekhajokha.udharkhata.R;

 public  class ImageAdapter extends PagerAdapter {
        private Context context;

        public ImageAdapter(Context context) {
            this.context = context;
        }


        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == ((ImageView) object);
        }

        private int[] sliderImageId = new int[]{
                R.drawable.slider_01,
                R.drawable.slider_02};

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            ImageView imageView = new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            //  imageView.setImageResource(Integer.parseInt(sliderImageId[position]));
            Glide.with(context).load(sliderImageId[position]).into(imageView);
            ((ViewPager) container).addView(imageView, 0);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {


            ((ViewPager) container).removeView((ImageView) object);

        }

        @Override
        public int getCount() {
            return sliderImageId.length;
        }
    }

