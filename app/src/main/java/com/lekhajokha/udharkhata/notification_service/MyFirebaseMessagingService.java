package com.lekhajokha.udharkhata.notification_service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;
import androidx.core.app.NotificationCompat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.lekhajokha.udharkhata.database.DBHelperSingleton;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.notification.model.NotificationModel;
import com.lekhajokha.udharkhata.utility.Constant;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@SuppressLint("SimpleDateFormat")
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    Intent intent;
    String messageBody = "";
    String dateTime = new SimpleDateFormat("yyyy-mm-dd hh:mm:aa").format(Calendar.getInstance().getTime());
    String notification_type = "", customerID ="", title="";
    PendingIntent pending_intent;
    int color_ORANGE, notification_id;
    Uri default_sound_uri;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    private String IS_SAVE = "Y";
    private String redirect_url = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "From NOTIFICATION: " + remoteMessage.getFrom());
        sendNotification(remoteMessage.getData());



    }

    private void sendNotification(Map<String, String> data) {
        intent = new Intent();
        messageBody = data.get("message");
        title = data.get("title");
        dateTime = data.get("date_time");
        customerID = data.get("customer_id");
        notification_type = data.get("notify_type");
        notification_id =  (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        String redirection_url = "";

        NotificationModel model = new NotificationModel ();
        model.setDateTime(dateTime);
        model.setCustomerId(customerID);
        model.setNotifyType(notification_type);
        model.setNotifyTitle(title);
        model.setNotifyMessage(messageBody);
        model.setId(notification_id+"");
        model.setSeen("0");
        try{
            IS_SAVE = data.get("is_save");
            if(IS_SAVE.equalsIgnoreCase("Y")){
                if (!DBHelperSingleton.getInstance().mDBDbHelper.insertNotification(model)) {
                    Toast.makeText(this, "Notification insert failed", Toast.LENGTH_SHORT).show();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            redirect_url = data.get("redirect_url");
        }catch (Exception e){
            e.printStackTrace();
        }

        doCommonNotification(redirection_url);
    }

    private void doCommonNotification(String redirection_url) {
        intent = new Intent(MyFirebaseMessagingService.this, HomeActivity.class);
        intent.putExtra(Constant.NOTIFY_REDIRECTION_URL,redirection_url);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pending_intent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
        long[] pattern = {200,400,500,600,800,600,500};
        color_ORANGE = R.drawable.launcher_logo_lekhajokha;
        Bitmap bitmap_icon = BitmapFactory.decodeResource(getResources(), R.drawable.launcher_logo_lekhajokha);
        String result = Settings.System.DEFAULT_RINGTONE_URI.toString();
        default_sound_uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setSmallIcon(R.drawable.launcher_logo_lekhajokha)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setVibrate(pattern)
                    .setSound(Uri.parse(result))
                    .setContentIntent(pending_intent)
                    .setLargeIcon(bitmap_icon)
                    .getNotification();


            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_LEKHA_JOKHA", importance);

            notificationChannel.setDescription(messageBody);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(true);
            notificationChannel.setVibrationPattern(pattern);
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
            mNotificationManager.notify(notification_id, mBuilder.build());

        } else {
            Notification notification = new NotificationCompat.Builder(this)
                    .setColor(color_ORANGE)
                    .setSmallIcon(getNotificationIcon())
                    .setLargeIcon(bitmap_icon)
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setContentIntent(pending_intent)
                    .setSound(default_sound_uri)
                    .setAutoCancel(true)
                    .build();

            NotificationManager notification_manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notification_manager.notify(notification_id, notification);

        }
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.launcher_logo_lekhajokha : R.drawable.launcher_logo_lekhajokha;
    }


}