package com.lekhajokha.udharkhata.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ServiceGenerator {

    public static final String API_BASE_URL = "https://lekhajokha.org/api/mainapp/v2/index.php/";
    public static final String PDF_DOWNLOAD_URL = "https://lekhajokha.org/api/mainapp/v2/";
    public static final String HOW_TO_USE_URL = API_BASE_URL+"HowToUse";
    public static final String Help_URL = API_BASE_URL+"Help";
    public static final String POLICY_URL = API_BASE_URL+"PrivacyPolicy";



    private static OkHttpClient httpClient = new OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS).build();



    //OkHttpClient.Builder httpClient = new OkHttpClient.Builder();


    private static Gson gson = new GsonBuilder().setLenient().create();

    private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl(API_BASE_URL);

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient).addConverterFactory(GsonConverterFactory.create(gson)).build();
        return retrofit.create(serviceClass);
    }


    public static Retrofit getRetrofit() {
        return builder.client(httpClient).build();
    }


}
