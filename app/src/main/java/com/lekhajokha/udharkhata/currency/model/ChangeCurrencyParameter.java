
package com.lekhajokha.udharkhata.currency.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ChangeCurrencyParameter {
    @SerializedName("currency_code")
    private String currencyCode;
    @SerializedName("currency_name")
    private String currencyName;
    @SerializedName("currency_symbol")
    private String currencySymbol;
    @SerializedName("display_currency")
    private String displayCurrency;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getDisplayCurrency() {
        return displayCurrency;
    }

    public void setDisplayCurrency(String displayCurrency) {
        this.displayCurrency = displayCurrency;
    }

}
