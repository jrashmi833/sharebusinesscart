package com.lekhajokha.udharkhata.currency;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.SignUp.model.User;
import com.lekhajokha.udharkhata.currency.model.ChangeCurrencyParameter;
import com.lekhajokha.udharkhata.currency.model.ChangeCurrencyResponse;
import com.lekhajokha.udharkhata.currency.model.CurrencyResponse;
import com.lekhajokha.udharkhata.currency.model.CurrencyData;
import com.lekhajokha.udharkhata.currency.model_currency.CurrencyAdapter;
import com.lekhajokha.udharkhata.utility.ApplicationSingleton;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CurrencyFragment extends Fragment implements CurrencyPresenterView, com.lekhajokha.udharkhata.currency.model_currency.CurrencyAdapter.onClick {
    public static final String TAG = CurrencyFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recycleView)
    RecyclerView recycleView;
    @BindView(R.id.imageBackButton)
    ImageView imageBackButton;
    @BindView(R.id.edittextSerchCurrency)
    EditText edittextSerchCurrency;
    @BindView(R.id.linear1)
    LinearLayout linear1;
    @BindView(R.id.cardView)
    CardView cardView;
    private Unbinder unbinder;
    private Context context;
    private ArrayList<CurrencyData> tempCurrencyArrayList;
    private ArrayList<CurrencyData> currencyArrayList;

    private String mParam1;
    private String mParam2;
    private View view;
    private CurrencyPresenter currencyPresenter;
    private CurrencyAdapter CurrencyAdapter;
    private String Currency_code, currency_name, currency_symbol, display_type;

    public CurrencyFragment() {
    }


    public static CurrencyFragment newInstance(String param1, String param2) {
        CurrencyFragment fragment = new CurrencyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_currency, container, false);
        unbinder = ButterKnife.bind(this, view);

        currencyPresenter = new CurrencyPresenter(this, context);
        currencyPresenter.callCurrencyList();

        edittextSerchCurrency.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (null != currencyArrayList && currencyArrayList.size() != 0) {
//                    if (i2 > 0) {
                        filterData(edittextSerchCurrency.getText().toString().trim());
//                    } else {
//                        tempCurrencyArrayList.clear();
//                        tempCurrencyArrayList = new ArrayList<>();
//                        tempCurrencyArrayList.addAll(currencyArrayList);
//                        CurrencyAdapter.notifyAdapter(tempCurrencyArrayList);
//                        CurrencyAdapter.notifyDataSetChanged();
//                    }
                }
            }
        });
        return view;
    }


    private void filterData(String query) {
        query = query.toLowerCase();
        tempCurrencyArrayList = new ArrayList<>();
        if(query.equals( "" )){
            tempCurrencyArrayList.addAll(currencyArrayList);

        } else {

            for (CurrencyData currencyModal : currencyArrayList) {
                if (currencyModal.getCountry().toLowerCase().contains(query) || currencyModal.getCode().toLowerCase().contains(query)
                        || currencyModal.getName().toLowerCase().contains(query) || currencyModal.getSymbol().toLowerCase().contains(query)) {
                    tempCurrencyArrayList.add(currencyModal);
                }
            }
        }

        CurrencyAdapter.notifyAdapter(tempCurrencyArrayList);
        CurrencyAdapter.notifyDataSetChanged();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onSuccessAddCurrencyList(CurrencyResponse currencyResponse) {
        tempCurrencyArrayList = new ArrayList<>();
        currencyArrayList = new ArrayList<>();
        if(null != currencyResponse.getData()){
            tempCurrencyArrayList.addAll(currencyResponse.getData());
            currencyArrayList.addAll(tempCurrencyArrayList);
            setDataAdapter();
        }
    }


    @Override
    public void onSucessChangeCurrencyList(ChangeCurrencyResponse changeCurrencyResponse) {
        ((HomeActivity)context).showToast(changeCurrencyResponse.getMessage());
        User user = new User();
        user = SharedPreference.getInstance(context).getUser();
        user.setCurrencyCode(Currency_code);
        user.setCurrencySymbol(currency_symbol);
        user.setDisplayCurrency(display_type);
        user.setCurrencyName(currency_name);
        SharedPreference.getInstance(context).putUser(Constant.USER,user);
        ApplicationSingleton.getInstance().getCurrencyChangeCallback().onCurrencyChanged();

        getActivity().onBackPressed();
    }

    private void setDataAdapter() {
        CurrencyAdapter = new CurrencyAdapter(context, tempCurrencyArrayList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != recycleView) {
            recycleView.setAdapter(CurrencyAdapter);
            recycleView.setLayoutManager(layoutManager);
        }

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShowLoader() {
        ((HomeActivity) context).showLoader();
    }

    @Override
    public void onHideLoader() {
        ((HomeActivity) context).hideLoader();
    }


    @Override
    public void onCurrencyClick(int position) {
        progressDialog(position);

    }

    private void progressDialog(int position) {

        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.layout_dilog_currencydata);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        TextView textViewCurrencyCode = dialog.findViewById(R.id.textViewCurrencyCodedilog);
        TextView textViewCurrencyName = dialog.findViewById(R.id.textViewCurrencyNamedilog);
        TextView textViewCurrencySymbol = dialog.findViewById(R.id.textViewCurrencySymboldilog);
        RadioButton radioButtonCode = dialog.findViewById(R.id.radioButtonCode);
        RadioButton radioButtonSymbol = dialog.findViewById(R.id.radioButtonSymbol);

        Button buttonSave = dialog.findViewById(R.id.buttonSavedilog);
        ImageView buttonCancel = dialog.findViewById(R.id.buttonCanceldilog);
        currency_name = tempCurrencyArrayList.get(position).getName();
        Currency_code = tempCurrencyArrayList.get(position).getCode();
        currency_symbol = tempCurrencyArrayList.get(position).getSymbol();

        textViewCurrencyCode.setText(tempCurrencyArrayList.get(position).getCode());
        textViewCurrencyName.setText(tempCurrencyArrayList.get(position).getName());
        textViewCurrencySymbol.setText(tempCurrencyArrayList.get(position).getSymbol());

        if( SharedPreference.getInstance( context ).getUser().getDisplayCurrency().equals(Constant.symbol)){
            radioButtonSymbol.setChecked(true);
            radioButtonCode.setChecked(false);
        }else {
            radioButtonCode.setChecked(true);
            radioButtonSymbol.setChecked(false);
        }


        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                {

                    ChangeCurrencyParameter changeCurrencyParameter = new ChangeCurrencyParameter();
                    changeCurrencyParameter.setCurrencyCode(textViewCurrencyCode.getText().toString());
                    changeCurrencyParameter.setCurrencyName(textViewCurrencyName.getText().toString());
                    changeCurrencyParameter.setCurrencySymbol(textViewCurrencySymbol.getText().toString());

                    if(radioButtonCode.isChecked()){
                        display_type = Constant.code;
                        changeCurrencyParameter.setDisplayCurrency(Constant.code);
                    }else {
                        display_type = Constant.symbol;
                        changeCurrencyParameter.setDisplayCurrency(Constant.symbol);
                    }

                    currencyPresenter.callChangeCurrency(changeCurrencyParameter, SharedPreference.getInstance(context).getUser().getAccessToken(), context);
                    dialog.dismiss();


                }
            }
        });
    }


    @OnClick(R.id.imageBackButton)
    public void onViewClicked() {
        getActivity().onBackPressed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}

