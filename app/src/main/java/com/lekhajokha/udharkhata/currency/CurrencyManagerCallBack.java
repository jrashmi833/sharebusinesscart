package com.lekhajokha.udharkhata.currency;

import com.lekhajokha.udharkhata.currency.model.ChangeCurrencyResponse;
import com.lekhajokha.udharkhata.currency.model.CurrencyResponse;

public interface CurrencyManagerCallBack {
    void onSuccessAddCurrencyList(CurrencyResponse currencyResponse);
    void onSuccessChangeCurrencyList(ChangeCurrencyResponse changeCurrencyResponse);
    void onTokenChangeError(String errorMessage);
    void onError(String errorMessage);
}
