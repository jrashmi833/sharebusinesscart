package com.lekhajokha.udharkhata.currency.user_currency;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.SignUp.model.User;
import com.lekhajokha.udharkhata.currency.CurrencyFragment;
import com.lekhajokha.udharkhata.currency.CurrencyPresenter;
import com.lekhajokha.udharkhata.currency.CurrencyPresenterView;
import com.lekhajokha.udharkhata.currency.model.ChangeCurrencyParameter;
import com.lekhajokha.udharkhata.currency.model.ChangeCurrencyResponse;
import com.lekhajokha.udharkhata.currency.model.CurrencyResponse;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.utility.ApplicationSingleton;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class UserCurrencyDetailFragment extends Fragment implements CurrencyPresenterView {
    public static final String TAG = UserCurrencyDetailFragment.class.getSimpleName();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.textViewCountrySymbol)
    TextView textViewCountrySymbol;
    @BindView(R.id.textViewCountryCode)
    TextView textViewCountryCode;
    @BindView(R.id.textViewCurrencyName)
    TextView textViewCurrencyName;
    @BindView(R.id.buttonChangeCurrency)
    Button buttonChangeCurrency;
    @BindView(R.id.checkBoxCurrencySymbol)
    ImageView checkBoxCurrencySymbol;
    @BindView(R.id.checkBoxCurrencyCode)
    ImageView checkBoxCurrencyCode;
    private View view;
    private Unbinder unbinder;
    private Context context;
    private String mParam1;
    private String mParam2;
    private String display_type; //   Symbol,    Code
    private CurrencyPresenter currencyPresenter;


    public UserCurrencyDetailFragment() {

    }


    public static UserCurrencyDetailFragment newInstance(String param1, String param2) {
        UserCurrencyDetailFragment fragment = new UserCurrencyDetailFragment();
        Bundle args = new Bundle();
        args.putString( ARG_PARAM1, param1 );
        args.putString( ARG_PARAM2, param2 );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        if (getArguments() != null) {
            mParam1 = getArguments().getString( ARG_PARAM1 );
            mParam2 = getArguments().getString( ARG_PARAM2 );
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate( R.layout.fragment_user_currency_detail, container, false );
        unbinder = ButterKnife.bind( this, view );
        currencyPresenter = new CurrencyPresenter( this, context );
        System.out.println( "Access token : " + SharedPreference.getInstance( context ).getUser().getAccessToken() );
        textViewCountryCode.setText( SharedPreference.getInstance( context ).getUser().getCurrencyCode() );
        textViewCountrySymbol.setText( SharedPreference.getInstance( context ).getUser().getCurrencySymbol() );
        textViewCurrencyName.setText( SharedPreference.getInstance( context ).getUser().getCurrencyName() );

        if (SharedPreference.getInstance( context ).getUser().getDisplayCurrency().equals( Constant.symbol )) {
            display_type = Constant.symbol;
            checkBoxCurrencyCode.setImageResource( R.drawable.icon_unchecked );
            checkBoxCurrencySymbol.setImageResource( R.drawable.icon_cheked );
        } else {
            display_type = Constant.code;
            checkBoxCurrencyCode.setImageResource( R.drawable.icon_cheked );
            checkBoxCurrencySymbol.setImageResource( R.drawable.icon_unchecked );
        }

        return view;
    }


    private void callChangeCurrencyApi() {
        ChangeCurrencyParameter parameter = new ChangeCurrencyParameter();
        parameter.setCurrencyCode( SharedPreference.getInstance( context ).getUser().getCurrencyCode() );
        parameter.setCurrencyName( SharedPreference.getInstance( context ).getUser().getCurrencyName() );
        parameter.setCurrencySymbol( SharedPreference.getInstance( context ).getUser().getCurrencySymbol() );
        parameter.setDisplayCurrency( display_type );
        currencyPresenter.callChangeCurrency( parameter, SharedPreference.getInstance( context ).getUser().getAccessToken(), context );
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach( context );
        this.context = context;
    }


    @OnClick({R.id.imageViewBack, R.id.buttonChangeCurrency})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack:
                getActivity().onBackPressed();
                break;
            case R.id.buttonChangeCurrency:
                ((HomeActivity) context).replaceFragmentFragment( new CurrencyFragment(), CurrencyFragment.TAG, true );
                break;

        }
    }

    @Override
    public void onSuccessAddCurrencyList(CurrencyResponse currencyResponse) {
        // not in use 
    }

    @Override
    public void onSucessChangeCurrencyList(ChangeCurrencyResponse changeCurrencyResponse) {
        ((HomeActivity) context).showToast( changeCurrencyResponse.getMessage() );
        User user = new User();
        user = SharedPreference.getInstance( context ).getUser();
        user.setDisplayCurrency( display_type );
        SharedPreference.getInstance( context ).putUser( Constant.USER, user );
        ApplicationSingleton.getInstance().getCurrencyChangeCallback().onCurrencyChanged();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {

    }

    @Override
    public void onError(String errorMessage) {

    }

    @Override
    public void onShowLoader() {

    }

    @Override
    public void onHideLoader() {

    }

    @OnClick({R.id.checkBoxCurrencySymbol, R.id.layoutSymbol, R.id.checkBoxCurrencyCode, R.id.layoutCode})
    public void onViewCurrencyClicked(View view) {
        switch (view.getId()) {
            case R.id.checkBoxCurrencySymbol:
            case R.id.layoutSymbol:
                    display_type = Constant.symbol;
                    checkBoxCurrencyCode.setImageResource( R.drawable.icon_unchecked );
                    checkBoxCurrencySymbol.setImageResource( R.drawable.icon_cheked );
                    callChangeCurrencyApi();
                break;
                case R.id.checkBoxCurrencyCode:
            case R.id.layoutCode:
                    display_type = Constant.code;
                    checkBoxCurrencyCode.setImageResource( R.drawable.icon_cheked );
                    checkBoxCurrencySymbol.setImageResource( R.drawable.icon_unchecked );
                    callChangeCurrencyApi();
                    break;

        }
    }
}