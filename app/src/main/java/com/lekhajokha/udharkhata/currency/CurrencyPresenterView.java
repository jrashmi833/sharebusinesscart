package com.lekhajokha.udharkhata.currency;

import com.lekhajokha.udharkhata.base.BaseInterface;
import com.lekhajokha.udharkhata.currency.model.ChangeCurrencyResponse;
import com.lekhajokha.udharkhata.currency.model.CurrencyResponse;

public interface CurrencyPresenterView extends BaseInterface {
    void onSuccessAddCurrencyList(CurrencyResponse currencyResponse);
    void onSucessChangeCurrencyList(ChangeCurrencyResponse changeCurrencyResponse);
    void onTokenChangeError(String errorMessage);
}
