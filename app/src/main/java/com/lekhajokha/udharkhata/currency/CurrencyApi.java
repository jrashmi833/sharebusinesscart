package com.lekhajokha.udharkhata.currency;

import com.lekhajokha.udharkhata.currency.model.ChangeCurrencyParameter;
import com.lekhajokha.udharkhata.currency.model.ChangeCurrencyResponse;
import com.lekhajokha.udharkhata.currency.model.CurrencyResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface
CurrencyApi {
    @GET("GetCurrency")
    Call<CurrencyResponse> callCurrencyList();


    @POST("ChangeCurrency")
    Call<ChangeCurrencyResponse> callChangeCurrency(
            @Body ChangeCurrencyParameter changeCurrencyParameter, @Header("ACCESS-TOKEN") String token);



    }


