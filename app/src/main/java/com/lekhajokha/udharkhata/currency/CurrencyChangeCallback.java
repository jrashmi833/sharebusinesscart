package com.lekhajokha.udharkhata.currency;

/**
 * Created by lenovo on 6/20/2018.
 */

public interface CurrencyChangeCallback {
    void onCurrencyChanged();
}
