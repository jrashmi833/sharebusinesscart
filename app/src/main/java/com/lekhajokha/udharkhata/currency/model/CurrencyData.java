
package com.lekhajokha.udharkhata.currency.model;

import com.google.gson.annotations.Expose;
import com.lekhajokha.udharkhata.currency.model_currency.CurrencyModal;

@SuppressWarnings("unused")
public class CurrencyData {

    @Expose
    private String code;
    @Expose
    private String country;
    @Expose
    private String name;
    @Expose
    private String symbol;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

}
