package com.lekhajokha.udharkhata.currency.model_currency;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.currency.model.CurrencyData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.ViewHolder> {



    private Context context;
    private ArrayList<CurrencyData> currencyModalArrayList;
    private onClick onClick;

    public CurrencyAdapter(Context context, ArrayList<CurrencyData> currencyModalArrayList, onClick onClick) {
        this.context = context;
        this.currencyModalArrayList = currencyModalArrayList;
        this.onClick = onClick;
    }

    public void notifyAdapter(ArrayList<CurrencyData> currencyModalArrayList) {
        this.currencyModalArrayList = currencyModalArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( context ).inflate( R.layout.row_currencylist, parent, false );
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewCurrencyName.setText( currencyModalArrayList.get( position ).getName() );
        holder.textViewCountry.setText( currencyModalArrayList.get( position ).getCountry() );
        holder.textViewCode.setText( currencyModalArrayList.get( position ).getCode() );
        holder.textViewCurrencySymbol.setText( currencyModalArrayList.get( position ).getSymbol() );
    }

    @Override
    public int getItemCount() {
        return currencyModalArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewCode)
        TextView textViewCode;
        @BindView(R.id.textViewCurrencyName)
        TextView textViewCurrencyName;
        @BindView(R.id.textViewCountry)
        TextView textViewCountry;
        @BindView(R.id.textViewCurrencySymbol)
        TextView textViewCurrencySymbol;
        @BindView(R.id.layoutCurrenyList)
        LinearLayout layoutCurrenyList;

        @OnClick(R.id.layoutCurrenyList)
        public void onViewClicked() {
            onClick.onCurrencyClick( getAdapterPosition() );
        }

        public ViewHolder(@NonNull View itemView) {
            super( itemView );
            ButterKnife.bind( this, itemView );
        }
    }

    public interface onClick {
        void onCurrencyClick(int position);
    }
}

