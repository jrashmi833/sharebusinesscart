package com.lekhajokha.udharkhata.currency.model_currency;

public class CurrencyModal {
    private String code, country,name,symbol;

    public CurrencyModal() {

    }

    public CurrencyModal(String code, String country, String name, String symbol) {
        this.code = code;
        this.country = country;
        this.name = name;
        this.symbol = symbol;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
