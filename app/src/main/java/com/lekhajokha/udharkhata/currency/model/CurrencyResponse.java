
package com.lekhajokha.udharkhata.currency.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class CurrencyResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<CurrencyData> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<CurrencyData> getData() {
        return data;
    }

    public void setData(ArrayList<CurrencyData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
