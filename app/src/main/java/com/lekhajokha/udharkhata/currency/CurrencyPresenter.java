package com.lekhajokha.udharkhata.currency;

import android.content.Context;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.currency.model.ChangeCurrencyParameter;
import com.lekhajokha.udharkhata.currency.model.ChangeCurrencyResponse;
import com.lekhajokha.udharkhata.currency.model.CurrencyResponse;

public class CurrencyPresenter implements CurrencyManagerCallBack {
    private CurrencyPresenterView currencyPresenterView;
    private CurrencyManager currencyManager;
    private Context context;

    public CurrencyPresenter(CurrencyPresenterView currencyPresenterView, Context context) {
        this.currencyPresenterView = currencyPresenterView;
        this.context=context;
        this.currencyManager = new CurrencyManager(this);

    }


    public void callCurrencyList(){
        if(((HomeActivity)context).isInternetConneted()) {
            currencyPresenterView.onShowLoader();
            currencyManager.callCurrencyList();
        }else {
            currencyPresenterView.onError(context.getResources().getString( R.string.no_internet_connection));
        }

    }
    public void callChangeCurrency(ChangeCurrencyParameter changeCurrencyParameter, String accessToken, Context context) {
        if(((HomeActivity)context).isInternetConneted()) {
            currencyPresenterView.onShowLoader();
            currencyManager.callChangeCurrency(changeCurrencyParameter,accessToken);
        }else {
            currencyPresenterView.onError(context.getResources().getString( R.string.no_internet_connection));
        }
    }
    @Override
    public void onSuccessAddCurrencyList(CurrencyResponse currencyResponse) {
        currencyPresenterView.onHideLoader();
        currencyPresenterView.onSuccessAddCurrencyList(currencyResponse);
    }

    @Override
    public void onSuccessChangeCurrencyList(ChangeCurrencyResponse changeCurrencyResponse) {
        currencyPresenterView.onHideLoader();
        currencyPresenterView.onSucessChangeCurrencyList(changeCurrencyResponse);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        currencyPresenterView.onHideLoader();
        currencyPresenterView.onTokenChangeError(errorMessage);
    }

    @Override
    public void onError(String errorMessage) {
        currencyPresenterView.onHideLoader();
        currencyPresenterView.onError(errorMessage);
    }


}
