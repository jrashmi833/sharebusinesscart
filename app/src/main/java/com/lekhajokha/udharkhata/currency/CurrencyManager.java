package com.lekhajokha.udharkhata.currency;

import android.content.Context;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.currency.model.ChangeCurrencyParameter;
import com.lekhajokha.udharkhata.currency.model.ChangeCurrencyResponse;
import com.lekhajokha.udharkhata.currency.model.CurrencyResponse;
import com.lekhajokha.udharkhata.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrencyManager {
    public CurrencyManagerCallBack currencyManagerCallBack;
    private Context context;
    public CurrencyManager(CurrencyManagerCallBack currencyManagerCallBack) {
        this.currencyManagerCallBack = currencyManagerCallBack;
    }

    public void callCurrencyList() {
        CurrencyApi currencyApi = ServiceGenerator.createService( CurrencyApi.class );

        Call<CurrencyResponse> currencyResponseCall = currencyApi.callCurrencyList();
        currencyResponseCall.enqueue( new Callback<CurrencyResponse>() {
            @Override
            public void onResponse(Call<CurrencyResponse> call, Response<CurrencyResponse> response) {
                if (null != response.body()) {
                    if (response.isSuccessful()) {
                        currencyManagerCallBack.onSuccessAddCurrencyList(response.body() );
                    } else {
                        currencyManagerCallBack.onTokenChangeError( response.body().getMessage() );
                    }
                } else {
                    currencyManagerCallBack.onError( context.getResources().getString( R.string.opps_something_went_wrong) );
                }

            }

            @Override
            public void onFailure(Call<CurrencyResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    currencyManagerCallBack.onError( context.getResources().getString( R.string.no_internet_connection) );
                } else {
                    currencyManagerCallBack.onError( context.getResources().getString( R.string.opps_something_went_wrong));
                }
            }
        } );
    }
    public void callChangeCurrency(ChangeCurrencyParameter changeCurrencyParameter, String  token) {
        CurrencyApi currencyApi = ServiceGenerator.createService( CurrencyApi.class );

        Call<ChangeCurrencyResponse> changeCurrencyResponseCall = currencyApi.callChangeCurrency(changeCurrencyParameter,token);
        changeCurrencyResponseCall.enqueue( new Callback<ChangeCurrencyResponse>() {
            @Override
            public void onResponse(Call<ChangeCurrencyResponse> call, Response<ChangeCurrencyResponse> response) {
                if (null != response.body()) {
                    if (response.isSuccessful()) {
                        currencyManagerCallBack.onSuccessChangeCurrencyList(response.body() );
                    } else {
                        currencyManagerCallBack.onTokenChangeError( response.body().getMessage() );
                    }
                } else {
                    currencyManagerCallBack.onError( "opps something went wrong" );
                }
            }

            @Override
            public void onFailure(Call<ChangeCurrencyResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    currencyManagerCallBack.onError( "Network down or no internet connection" );
                } else {
                    currencyManagerCallBack.onError( "opps something went wrong" );
                }
            }
        } );
    }
}
