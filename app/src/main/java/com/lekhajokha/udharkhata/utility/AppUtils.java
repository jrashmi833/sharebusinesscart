package com.lekhajokha.udharkhata.utility;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.lekhajokha.udharkhata.R;

/**
 * Created by Ritu Patidar on 7/1/2020.
 */

public class AppUtils {
    public static void startFromRightToLeft(Context context) {
        ((Activity) context).overridePendingTransition(R.anim.trans_left_in, R.anim.no_animation);
    }

    public static void finishFromLeftToRight(Context context) {
        ((Activity) context).overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public static void hideSoftKeyboard(Activity activity) {
        View focusedView = activity.getCurrentFocus();
        if (focusedView != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(focusedView.getWindowToken(),      InputMethodManager.RESULT_UNCHANGED_SHOWN);
        }
    }

}
