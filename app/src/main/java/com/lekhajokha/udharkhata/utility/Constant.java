package com.lekhajokha.udharkhata.utility;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;


import java.text.SimpleDateFormat;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
@SuppressLint("SimpleDateFormat")
public class Constant {

    public static final String USER = "User";
    public static final String IS_USER_LOGIN = "isUserLogin";
    public static final String PASSWORD = "password";
    public static final String DATUM = "datum";
    public static final String CUSTOMER_LIST = "customerList";
    public static final String WHERE_FROM = "WHERE_FROM";
    public static String AppLock = "appPassword";
    public static String TransactionLock = "transactionlock";
    public static String PHONE = "PHONE";
    public static String EMAIL = "EMAIL";
    public static String code = "code";
    public static String symbol = "symbol";
    public static final String Credit = "CREDIT";
    public static final String Debit = "DEBIT";
    public static final String All = "ALL";
    public static final SimpleDateFormat ServerDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat DisplayFormat = new SimpleDateFormat("dd MMM yyyy");
    public static String online = "online";
    public static String cash = "cash";
    public static String other = "other";

    public static String NOTIFY_REDIRECTION_URL = "other";

    public static RequestBody getRequestBody(String value) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), value);
    }


    /*---------------------constant for database-----------------------------------*/
    /*constants for databse*/
    public static final String CONTACT_TABLE="notificationTable";

    public static final String ID = "id";
    public static final String CUSTOMER_ID = "customer_id";
    public static final String NOTIFY_TYPE = "notify_type";
    public static final String NOTIFY_TITLE = "title";
    public static final String NOTIFY_MESSAGE = "message";
    public static final String NOTIFY_DATE_TIME = "date_Time";
    public static final String IS_SEEN = "is_seen";

    public static final String CREATE_TABLE_NOTIFICATION ="CREATE TABLE IF  NOT EXISTS " + CONTACT_TABLE + "("
            + ID + " TEXT,"
            + CUSTOMER_ID + " TEXT,"
            + NOTIFY_TYPE + " TEXT,"
            + NOTIFY_TITLE + " TEXT,"
            + NOTIFY_MESSAGE + " TEXT,"
            + IS_SEEN + " TEXT,"
            + NOTIFY_DATE_TIME + " TEXT" + ")";

    public static final String DROP_TABLE_CONTACT="DROP TABLE IF EXISTS"+CONTACT_TABLE;

    public static final String SELECT_TABLE_CONTACT="SELECT * FROM "+ CONTACT_TABLE ;



}
