package com.lekhajokha.udharkhata.utility;

import android.app.Application;
import com.lekhajokha.udharkhata.database.DBHelperSingleton;
/**
 * Created by Ravi Thakur on 1/29/2019.
 */

public class LekhaJokhaApplication extends Application {



    @Override
    public void onCreate() {
        super.onCreate();
        DBHelperSingleton.getInstance().initDB(this);
//        Thread.setDefaultUncaughtExceptionHandler(new LocalFileUncaughtExceptionHandler(this,
//                Thread.getDefaultUncaughtExceptionHandler()));
    }



    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
