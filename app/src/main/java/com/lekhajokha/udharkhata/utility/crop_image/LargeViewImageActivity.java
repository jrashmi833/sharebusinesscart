package com.lekhajokha.udharkhata.utility.crop_image;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.base.BaseActivity;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LargeViewImageActivity extends BaseActivity {
    @BindView(R.id.imageViewUserImage)
    ImageView imageViewUserImage;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    String imgUrl ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_large_view_image);
        ButterKnife.bind(this);
        progressBar.setVisibility(View.VISIBLE);

        if(null != getIntent() && getIntent().hasExtra("IMG")){
            imgUrl = getIntent().getStringExtra("IMG");
        }

        Glide.with(this)
                .load(imgUrl)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imageViewUserImage);

    }

    @OnClick({R.id.imageViewBackButton})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.imageViewBackButton) {
            onBackPressed();
        }
    }


}
