package com.lekhajokha.udharkhata.utility;

import com.lekhajokha.udharkhata.currency.CurrencyChangeCallback;
import com.lekhajokha.udharkhata.notification.NotificationCountUpdateCallback;
import com.lekhajokha.udharkhata.account.ProfileUpdateCallback;

/**
 * Created by Ritu patidar 1/22/2020.
 */

public class ApplicationSingleton {

    private CurrencyChangeCallback currencyChangeCallback;
    private ProfileUpdateCallback profileUpdateCallback ;
    private NotificationCountUpdateCallback notificationCountUpdateCallback ;


    private static final ApplicationSingleton ourInstance = new ApplicationSingleton();

    public static ApplicationSingleton getInstance() {
        return ourInstance;
    }

    private ApplicationSingleton() {
    }

    public CurrencyChangeCallback getCurrencyChangeCallback() {
        return currencyChangeCallback;
    }

    public void setCurrencyChangeCallback(CurrencyChangeCallback currencyChangeCallback) {
        this.currencyChangeCallback = currencyChangeCallback;
    }

    public ProfileUpdateCallback getProfileUpdateCallback() {
        return profileUpdateCallback;
    }

    public void setProfileUpdateCallback(ProfileUpdateCallback profileUpdateCallback) {
        this.profileUpdateCallback = profileUpdateCallback;
    }

    public NotificationCountUpdateCallback getNotificationCountUpdateCallback() {
        return notificationCountUpdateCallback;
    }

    public void setNotificationCountUpdateCallback(NotificationCountUpdateCallback notificationCountUpdateCallback) {
        this.notificationCountUpdateCallback = notificationCountUpdateCallback;
    }
}
