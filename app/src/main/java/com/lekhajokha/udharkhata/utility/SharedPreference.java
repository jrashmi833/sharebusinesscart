package com.lekhajokha.udharkhata.utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.lekhajokha.udharkhata.SignUp.model.User;
import com.lekhajokha.udharkhata.currency.model.CurrencyData;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerListResponse;

public class SharedPreference {
    private static SharedPreference yourPreference;
    private SharedPreferences sharedPreferences;

    public static SharedPreference getInstance(Context context) {
        if (yourPreference == null) {
            yourPreference = new SharedPreference(context);
        }
        return yourPreference;
    }

    public void clear() {
        sharedPreferences.edit().clear().apply();
    }

    private SharedPreference(Context context) {
        sharedPreferences = context.getSharedPreferences("Prefs", Context.MODE_PRIVATE);
    }

    public void setString(String key, String value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(key, value);
        prefsEditor.apply();
    }

    public String getString(String key, String defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getString(key, defaultValue);
        }
        return "";
    }
    public User getUser() {
        return (User) getObject(Constant.USER, User.class);
    }

    public GetCustomerListResponse getCustomerListData () {
        return (GetCustomerListResponse) getObject(Constant.CUSTOMER_LIST, GetCustomerListResponse.class);
    }

    public CurrencyData getDatum(){
        return (CurrencyData)getObject( Constant.DATUM, CurrencyData.class );

    }


    public void setInt(String key, int value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putInt(key, value);
        prefsEditor.apply();
    }

    public int getInt(String key, int defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getInt(key, defaultValue);
        }
        return 0;
    }

    public void setFloat(String key, float value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putFloat(key, value);
        prefsEditor.apply();
    }


    public float getFloat(String key, float defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getFloat(key, defaultValue);
        }
        return 0;
    }

    public void setBoolean(String key, boolean value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.apply();
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getBoolean(key, defaultValue);
        }
        return false;
    }



    public void putUser(String key, Object obj) {
        checkForNullKey(key);
        Gson gson = new Gson();
        putString(key, gson.toJson(obj));
    }


    public Object getObject(String key, Class<?> classOfT) {
        String json = getString(key);
        return new Gson().fromJson(json, classOfT);
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void putObject(String key, Object obj) {
        checkForNullKey(key);
        Gson gson = new Gson();
        putString(key, gson.toJson(obj));
    }


    private void putString(String key, String value) {
        checkForNullKey(key);
        checkForNullValue(value);
        sharedPreferences.edit().putString(key, value).apply();
    }

    private void checkForNullKey(String key) {
        if (key == null) {
            throw new NullPointerException();
        }
    }

    private void checkForNullValue(String value) {
        if (value == null) {
            throw new NullPointerException();
        }
    }
}
