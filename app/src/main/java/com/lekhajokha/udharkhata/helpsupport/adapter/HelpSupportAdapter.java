package com.lekhajokha.udharkhata.helpsupport.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.helpsupport.model.HelpSupportModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HelpSupportAdapter extends RecyclerView.Adapter<HelpSupportAdapter.ViewHolder> {


    private Context context;
    private ArrayList<HelpSupportModel> helpSupportModels;
    private onClick onClick;

    public HelpSupportAdapter(Context context, ArrayList<HelpSupportModel> helpSupportModels, HelpSupportAdapter.onClick onClick) {
        this.context = context;
        this.helpSupportModels = helpSupportModels;
        this.onClick = onClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( context ).inflate( R.layout.row_helpsupport, parent, false );
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewCount.setText( helpSupportModels.get( position ).getTextCount() );
        holder.textViewDescription.setText( helpSupportModels.get( position ).getDescription() );
        holder.textViewStatus.setText( helpSupportModels.get( position ).getTextStatus() );
    }

    @Override
    public int getItemCount() {
        return helpSupportModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewDescription)
        TextView textViewDescription;
        @BindView(R.id.textViewCount)
        TextView textViewCount;
        @BindView(R.id.textViewStatus)
        TextView textViewStatus;
        @BindView(R.id.layoutQuery)
        CardView layoutQuery;
        public ViewHolder(@NonNull View itemView) {
            super( itemView );
            ButterKnife.bind( this, itemView );
        }
        @OnClick(R.id.layoutQuery)
        public void onViewClicked() {
            onClick.onQueryClick(getAdapterPosition());
        }


    }

    public interface onClick {
        void onQueryClick(int position);
    }
}
