package com.lekhajokha.udharkhata.helpsupport.manager;

import com.lekhajokha.udharkhata.helpsupport.model.HelpSupportModel;

import java.util.ArrayList;

public class HelpSupportManager {
    public ArrayList<HelpSupportModel> getHelpSupportList() {

        ArrayList<HelpSupportModel>helpSupportArrayList = new ArrayList<>();
        helpSupportArrayList.add(new HelpSupportModel("Loreum Ipsum is simply dummy text of thr printing and typesetting industry .Lorem Ipsum has been ","open","3"));
        helpSupportArrayList.add(new HelpSupportModel("A writing consisting of multiple glyphs, characters, symbols or sentences. A book, tome or other set of writings. (colloquial) A brief written message transmitted between mobile phones; an SMS text message.","open","2"));
        helpSupportArrayList.add(new HelpSupportModel("Feedback","open","6"));
        helpSupportArrayList.add(new HelpSupportModel("A writing consisting of multiple glyphs, characters, symbols or sentences. A book, tome or other set of writings. (colloquial) A brief written message transmitted between mobile phones; an SMS text message.","open","4"));

        return helpSupportArrayList;
    }
}
