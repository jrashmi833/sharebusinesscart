package com.lekhajokha.udharkhata.helpsupport.model;

public class HelpSupportModel {
    String Description,TextStatus,TextCount;

    public HelpSupportModel(String description, String textStatus, String textCount) {
        Description = description;
        TextStatus = textStatus;
        TextCount = textCount;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getTextStatus() {
        return TextStatus;
    }

    public void setTextStatus(String textStatus) {
        TextStatus = textStatus;
    }

    public String getTextCount() {
        return TextCount;
    }

    public void setTextCount(String textCount) {
        TextCount = textCount;
    }
}
