package com.lekhajokha.udharkhata.helpsupport;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.helpsupport.adapter.HelpSupportAdapter;
import com.lekhajokha.udharkhata.helpsupport.manager.HelpSupportManager;
import com.lekhajokha.udharkhata.helpsupport.model.HelpSupportModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class HelpsupportFrtragment extends Fragment implements HelpSupportAdapter.onClick {
    public static final String TAG = HelpsupportFrtragment.class.getSimpleName();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.backButton)
    ImageView backButton;
    @BindView(R.id.recycleViewHelpSupportList)
    RecyclerView recycleViewHelpSupportList;
    @BindView(R.id.buttonRasiseQuery)
    Button buttonRasiseQuery;
    private View view;
    private Context context;
    Unbinder unbinder;
    ArrayList<HelpSupportModel> helpSupportModelArrayList;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public HelpsupportFrtragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static HelpsupportFrtragment newInstance(String param1, String param2) {
        HelpsupportFrtragment fragment = new HelpsupportFrtragment();
        Bundle args = new Bundle();
        args.putString( ARG_PARAM1, param1 );
        args.putString( ARG_PARAM2, param2 );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        if (getArguments() != null) {
            mParam1 = getArguments().getString( ARG_PARAM1 );
            mParam2 = getArguments().getString( ARG_PARAM2 );
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate( R.layout.fragment_helpsupport_frtragment, container, false );
        unbinder= ButterKnife.bind( this,view );
        setAdapter();
        return view;
    }

    private void setAdapter() {
        HelpSupportManager helpSupportManager = new HelpSupportManager();
        helpSupportModelArrayList = helpSupportManager.getHelpSupportList();
        HelpSupportAdapter helpSupportAdapter = new HelpSupportAdapter( context, helpSupportModelArrayList ,this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( context, RecyclerView.VERTICAL, false );

        if (null != recycleViewHelpSupportList) {
            recycleViewHelpSupportList.setAdapter( helpSupportAdapter );
            recycleViewHelpSupportList.setLayoutManager( layoutManager );

            recycleViewHelpSupportList.addOnScrollListener( new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled( recyclerView, dx, dy );
                    if (dy > 0) {
                        buttonRasiseQuery.setVisibility( View.GONE );

                    } else {
                        buttonRasiseQuery.setVisibility( View.VISIBLE );
                    }


                }
            } );
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach( context );
        this.context = context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        unbinder.unbind();
    }


    @OnClick({R.id.backButton, R.id.buttonRasiseQuery})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                getActivity().onBackPressed();
                break;
            case R.id.buttonRasiseQuery:

                break;
        }
    }

    @Override
    public void onQueryClick(int position) {

    }
}
