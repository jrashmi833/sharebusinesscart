package com.lekhajokha.udharkhata.contact_list.add_customer;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.contact_list.add_customer.model.AddCustomerParameter;
import com.lekhajokha.udharkhata.contact_list.add_customer.model.GetCustomerAddedResponse;
import com.lekhajokha.udharkhata.home.Customer.CustomerPresenter;
import com.lekhajokha.udharkhata.home.Customer.CustomerView;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.DeleteCustomerResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerListResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerProfileUpdateResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.UpdateCustomerProfileResponse;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.login_screen.LoginThroughActivity;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class AddCustomerFragment extends Fragment implements CustomerView {

    public static final String TAG = AddCustomerFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    @BindView(R.id.backButton)
    ImageView backButton;

    Unbinder unbinder;
    @BindView(R.id.textViewName)
    EditText textViewName;
    @BindView(R.id.layoutNameButton)
    LinearLayout layoutNameButton;
    @BindView(R.id.textViewMobileNumber)
    EditText textViewMobileNumber;
    @BindView(R.id.layoutMobileNumberButton)
    LinearLayout layoutMobileNumberButton;
    @BindView(R.id.textViewEmail)
    EditText textViewEmail;
    @BindView(R.id.layoutEmailButton)
    LinearLayout layoutEmailButton;
    @BindView(R.id.buttonSave)
    Button buttonSave;


    private String name;
    private String number;
    private String image = null;
    private Context context;
    private View view;
    private CustomerPresenter customerPresenter;
    String dateToStr;

    public AddCustomerFragment() {
        // Required empty public constructor
    }


    public static AddCustomerFragment newInstance(String param1, String param2, String param3) {
        AddCustomerFragment fragment = new AddCustomerFragment();
        Bundle args = new Bundle();
        args.putString( ARG_PARAM1, param1 );
        args.putString( ARG_PARAM2, param2 );
        args.putString( ARG_PARAM3, param3 );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        if (getArguments() != null) {
            name = getArguments().getString( ARG_PARAM1 );
            number = getArguments().getString( ARG_PARAM2 );
            image = getArguments().getString( ARG_PARAM3 );
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate( R.layout.fragment_add_customer, container, false );
        unbinder = ButterKnife.bind( this, view );
        if (name != null && number != null) {
            textViewName.setText( name );
            textViewMobileNumber.setText( number );
        }
        customerPresenter = new CustomerPresenter( context, this );

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach( context );
        this.context = context;
    }


    @OnClick({R.id.backButton, R.id.buttonSave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                getActivity().onBackPressed();
                break;
            case R.id.buttonSave:
                if (textViewName.getText().toString().length() != 0) {
                    if (((HomeActivity) context).isInternetConneted()) {
                        addUser();
                    } else {
                        Toast.makeText( context, "No Internet Connection", Toast.LENGTH_SHORT ).show();
                    }
                } else {
                    Toast.makeText( context, "Please Enter Name", Toast.LENGTH_SHORT ).show();
                }

                break;

        }
    }

    private void addUser() {
   /*     Date today = new Date();
        dateToStr = Constant.DisplayFormat.format( today );
        CustomerModel customerModel = new CustomerModel();
        customerModel.setName( textViewName.getText().toString() );
        customerModel.setPhone( textViewMobileNumber.getText().toString() );
        customerModel.setEmail( textViewEmail.getText().toString() );
        customerModel.setImage( image );
        customerModel.setTxn_date( "Added On " + dateToStr );
        customerModel.setTxn_email_status( false );
        customerModel.setTxn_sms_status( false );


        if (userNotFound()) {*/

        AddCustomerParameter addCustomerParameter = new AddCustomerParameter();
        addCustomerParameter.setCustomerName( textViewName.getText().toString() );
        addCustomerParameter.setCustomerPhone( textViewMobileNumber.getText().toString() );
        addCustomerParameter.setCustomerEmail( textViewEmail.getText().toString() );
        customerPresenter.callAddCustomer( addCustomerParameter, SharedPreference.getInstance( context ).getUser().getAccessToken(), context );

      /*  } else {
            Toast.makeText( context, "Customer already available ", Toast.LENGTH_SHORT ).show();

        }*/


    }

    private boolean userNotFound() {
      /*  ArrayList<CustomerModel> customerModels = new ArrayList<>();
        customerModels.addAll(customer.getAllCustomers());
        for (int i = 0; i < customerModels.size(); i++) {
            if (customerModels.get(i).getPhone().equalsIgnoreCase(textViewMobileNumber.getText().toString())){
                return false;
            }
        }*/

        return true;
    }


    @Override
    public void onSuccessCustomerUpdate(GetCustomerProfileUpdateResponse getCustomerProfileUpdateResponse) {
        // not in use
    }

    @Override
    public void onSuccessAddCustomer(GetCustomerAddedResponse getCustomerAddedResponse) {

        ((HomeActivity) context).showToast( getCustomerAddedResponse.getMessage() );
        getActivity().finish();
        getActivity().startActivity( getActivity().getIntent() );
        AppUtils.startFromRightToLeft( context );

    }

    @Override
    public void onSuccessCustomerProfileUpdate(UpdateCustomerProfileResponse updateCustomerProfileResponse) {

    }

    @Override
    public void onSuccessDeleteCustomer(DeleteCustomerResponse deleteCustomerResponse) {
        // not in use
    }

    @Override
    public void onSuccessGetCustomerList(GetCustomerListResponse getCustomerList) {
        // not in use
    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        SharedPreference.getInstance( context ).setBoolean( Constant.IS_USER_LOGIN, false );
        Intent intent = new Intent( context, LoginThroughActivity.class );
        intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
        startActivity( intent );
        AppUtils.startFromRightToLeft( context );
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText( context, errorMessage, Toast.LENGTH_SHORT ).show();
    }

    @Override
    public void onShowLoader() {
        ((HomeActivity) context).showLoader();
    }

    @Override
    public void onHideLoader() {
        ((HomeActivity) context).hideLoader();
    }
}
