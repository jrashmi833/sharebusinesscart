package com.lekhajokha.udharkhata.contact_list.adapter;

import androidx.annotation.Nullable;

import org.shadow.apache.commons.lang3.builder.EqualsBuilder;
import org.shadow.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Comparator;

public class ContactModel {
    public String name, number,image;
    public int id;

    public ContactModel() {
    }

    public ContactModel(int id,String name, String number, String image) {
        this.name = name;
        this.number = number;
        this.image = image;
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
                // if deriving: appendSuper(super.hashCode()).
                        append(number).
//                        append(name).
//                        append(id).
//                        append(image).
                        toHashCode();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof ContactModel))
            return false;
        if (obj == this)
            return true;

        ContactModel rhs = (ContactModel) obj;
        return new EqualsBuilder().
                // if deriving: appendSuper(super.equals(obj)).
                        append(number, rhs.number).
                        isEquals();
    }

}
