package com.lekhajokha.udharkhata.contact_list;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.contact_list.adapter.ContactAdapter;
import com.lekhajokha.udharkhata.contact_list.adapter.ContactModel;
import com.lekhajokha.udharkhata.contact_list.add_customer.AddCustomerFragment;
import com.lekhajokha.udharkhata.database.DBHelper;
import com.lekhajokha.udharkhata.utility.AppUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.os.Build.VERSION_CODES.M;


public class ContactListFragment extends Fragment implements com.lekhajokha.udharkhata.contact_list.adapter.ContactAdapter.onClick {
    public static final String TAG = ContactListFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.addUserButton)
    LinearLayout addUserButton;
    private Unbinder unbinder;
    @BindView(R.id.imageViewBackButton)
    ImageView imageViewBackButton;
    @BindView(R.id.buttonAddContact)
    ImageView buttonAddContact;
    @BindView(R.id.imageViewaddUser)
    ImageView imageViewaddUser;
    @BindView(R.id.layoutContactList)
    RelativeLayout layoutContactList;
    @BindView(R.id.buttonSyncContacts)
    LinearLayout buttonSyncContacts;
    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;

    @BindView(R.id.editTextSearchCoustomer)
    EditText editTextSearchCoustomer;
    @BindView(R.id.RecycleViewList)
    RecyclerView RecycleViewList;
    private ContactAdapter ContactAdapter;

//    private HashMap<String, ContactModel> contactParamHashMapList;
    public static final int PERMISSION_REQUEST_CODE = 1121;
    private DBHelper dbHelper;
    private ArrayList<ContactModel> viewModelArrayList;
    private ArrayList<ContactModel> temporaryList;
//    private float angle = 360;
//    private Handler handle_meter;
//    private Runnable update_meter;

    private LinkedHashSet<ContactModel> linkedHashSet = new LinkedHashSet<>();

    public ContactListFragment() {
    }


    public static ContactListFragment newInstance(String param1, String param2) {
        ContactListFragment fragment = new ContactListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_contact_list, container, false);
        unbinder = ButterKnife.bind(this, view);


        buttonSyncContacts.setVisibility(View.GONE);
        editTextSearchCoustomer.setVisibility(View.GONE);

        temporaryList = new ArrayList<>();
        viewModelArrayList = new ArrayList<>();

        setDataAdapter();

        dbHelper = new DBHelper(context);
        if (null != dbHelper.getAllContacts() && dbHelper.getAllContacts().size()>0) {
                viewModelArrayList.addAll(dbHelper.getAllContacts());
                temporaryList.addAll(viewModelArrayList);
                setDataAdapter();
            editTextSearchCoustomer.setVisibility(View.VISIBLE);
        } else {

            getAllContactFromContactList();

        }

        editTextSearchCoustomer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(viewModelArrayList.size() > 0) {
                    if (i2 > 0) {
                        filterData(charSequence.toString().trim());
                    } else {
                        temporaryList.clear();
                        temporaryList.addAll(viewModelArrayList);
                        ContactAdapter.notifyAdapter(temporaryList);
                        ContactAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(viewModelArrayList.size() > 0) {
                    if (i2 > 0) {
                        filterData(charSequence.toString().trim());
                    } else {
                        temporaryList.clear();
                        temporaryList.addAll(viewModelArrayList);
                        ContactAdapter.notifyAdapter(temporaryList);
                        ContactAdapter.notifyDataSetChanged();
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        return view;
    }

    private void filterData(String query) {
        query = query.toLowerCase();
        temporaryList.clear();
        temporaryList = new ArrayList<>();
        for (ContactModel contact : viewModelArrayList) {
            try {
                if (contact.getName().toLowerCase().contains(query) || contact.getNumber().contains(query)) {
                    temporaryList.add(contact);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        ContactAdapter.notifyAdapter(temporaryList);
        ContactAdapter.notifyDataSetChanged();

    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }


    private void setDataAdapter() {

//        for (int i = 0; i < temporaryList.size(); i++) {
//            hash_map.put(temporaryList.get(i).getNumber(), temporaryList.get(i));
//        }
//
//        temporaryList = new ArrayList<>();
//        temporaryList.addAll(hash_map.values());
//        viewModelArrayList = new ArrayList<>();
//        viewModelArrayList.addAll(hash_map.values());

        ContactAdapter = new ContactAdapter(context, temporaryList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        if (null != RecycleViewList) {
            RecycleViewList.setAdapter((RecyclerView.Adapter) ContactAdapter);
            RecycleViewList.setLayoutManager(layoutManager);
        }
        RecycleViewList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    addUserButton.setVisibility(View.GONE);

                } else {
                    addUserButton.setVisibility(View.VISIBLE);
                }


            }
        });
    }

    @Override
    public void onContactClick(int position) {
        ((HomeActivity) context).replaceFragmentFragment(AddCustomerFragment.newInstance(temporaryList.get(position).getName(), temporaryList.get(position).getNumber(), temporaryList.get(position).getImage()), AddCustomerFragment.TAG, true);

    }

    private void showLoader() {
//        handle_meter = new Handler();
//        handle_meter.postDelayed(update_meter, 500);


      /*  update_meter = new Runnable() {
            @Override
            public void run() {
                angle = angle - 1;
                handle_meter.removeCallbacks(update_meter);
                if (angle >= 0) {
                    handle_meter.postDelayed(update_meter, 100);
                    buttonAddContact.setRotation(angle);
                }

            }
        };*/
    }

    private class GetSynContacts extends AsyncTask<String, String, JSONObject> {


        @Override
        protected void onPreExecute() {
            ((HomeActivity) context).showLoader();
            dbHelper.resetDatabase();
            super.onPreExecute();
        }


        @Override
        protected JSONObject doInBackground(String... params) {
            try {


                getContactsInBackground();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            updateSyncList();

            for (ContactModel model:linkedHashSet) {
                dbHelper.addContact(model);
            }




            super.onPostExecute(jsonObject);

        }
    }



    private void updateSyncList() {
        ((HomeActivity) context).hideLoader();
        editTextSearchCoustomer.setVisibility(View.VISIBLE);
        buttonSyncContacts.setVisibility(View.GONE);
        viewModelArrayList.clear();
        temporaryList.clear();
        viewModelArrayList.addAll(linkedHashSet);
        temporaryList.addAll(viewModelArrayList);
        ContactAdapter.notifyAdapter(temporaryList);
        ContactAdapter.notifyDataSetChanged();
    }

    private void getContactsInBackground() {

        try {
            linkedHashSet = new LinkedHashSet<>();
            ContentResolver cr = context.getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
            if (null != cur && cur.getCount() > 0) {
                int count = 25 ;
                while (cur.moveToNext()) {
                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[]{id},
                                null);

                        try {



                            if (null != pCur) {
                                while (pCur.moveToNext()) {
                                    ContactModel contactModel = new ContactModel();
                                    contactModel.setName(name);
                                    String phoneNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                    String params = phoneNumber.replaceAll("[^0-9]", "");
                                    contactModel.setNumber(params);

                                /*    String Image = "";
                                    try {

                                        int index = pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI);
                                        Image = pCur.getString(index);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    contactModel.setImage(Image);*/
                                    if (params.length() > 5) {
                                        linkedHashSet.add(contactModel);
                                    }

                                }
                                pCur.close();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                    try {
                        if (count == linkedHashSet.size()) {
                            getActivity().runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                     updateSyncList();
                                }
                            });

                            count = count + 25;

                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                cur.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @OnClick({R.id.imageViewBackButton, R.id.buttonAddContact, R.id.addUserButton, R.id.buttonSyncContacts})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewBackButton:
                getActivity().finish();
                getActivity().startActivity(getActivity().getIntent());
                AppUtils.startFromRightToLeft(context);
                break;
            case R.id.buttonAddContact:
            case R.id.buttonSyncContacts:
                getAllContactFromContactList();
                break;
            case R.id.addUserButton:
                ((HomeActivity) context).replaceFragmentFragment(new AddCustomerFragment(), AddCustomerFragment.TAG, true);

                break;
        }
    }

    private void getAllContactFromContactList() {
        if (Build.VERSION.SDK_INT >= M) {
            if (checkreadContactPermission())
                new GetSynContacts().execute();
            else
                requestContactPermission();
        }else {
            new GetSynContacts().execute();
        }
    }

    @RequiresApi(api = M)
    private void requestContactPermission() {
        requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSION_REQUEST_CODE);
    }


    private boolean checkreadContactPermission() {
        int result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().finish();
                    getActivity().startActivity(getActivity().getIntent());
                    AppUtils.startFromRightToLeft(context);
                    return true;

                }
                return false;
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PERMISSION_REQUEST_CODE && resultCode == Activity.RESULT_OK && null != data) {
            new GetSynContacts().execute();
        }
    }

    @RequiresApi(api = M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                new GetSynContacts().execute();
            }else {
                buttonSyncContacts.setVisibility(View.VISIBLE);
            }
        }
    }

}
