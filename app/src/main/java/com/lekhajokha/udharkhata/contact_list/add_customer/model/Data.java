
package com.lekhajokha.udharkhata.contact_list.add_customer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @Expose
    private String address;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("customer_email")
    private String customerEmail;
    @SerializedName("customer_id")
    private int customerId;
    @SerializedName("customer_name")
    private String customerName;
    @SerializedName("customer_phone")
    private String customerPhone;
    @SerializedName("txn_email_status")
    private String txnEmailStatus;
    @SerializedName("txn_sms_status")
    private String txnSmsStatus;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("user_id")
    private String userId;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getTxnEmailStatus() {
        return txnEmailStatus;
    }

    public void setTxnEmailStatus(String txnEmailStatus) {
        this.txnEmailStatus = txnEmailStatus;
    }

    public String getTxnSmsStatus() {
        return txnSmsStatus;
    }

    public void setTxnSmsStatus(String txnSmsStatus) {
        this.txnSmsStatus = txnSmsStatus;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
