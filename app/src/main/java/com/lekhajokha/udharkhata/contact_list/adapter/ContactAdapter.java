package com.lekhajokha.udharkhata.contact_list.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {


    private Context context;
    private ArrayList<ContactModel> viewModelArrayList;
    private onClick onClick;


    public ContactAdapter(Context context, ArrayList<ContactModel> viewModelArrayList, onClick onClick) {
        this.context = context;
        this.viewModelArrayList = viewModelArrayList;
        this.onClick = onClick;
    }

    public void notifyAdapter(ArrayList<ContactModel> viewModelArrayList) {
        this.viewModelArrayList = viewModelArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_viewmodel, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewName.setText(viewModelArrayList.get(position).getName());
        holder.textViewNumber.setText(viewModelArrayList.get(position).getNumber());

       /* if (null != viewModelArrayList.get(position).getImage()) {
            holder.userPhoto.setImageURI(Uri.parse(viewModelArrayList.get(position).getImage()));
        }*/

    }


    @Override
    public int getItemCount() {
        return viewModelArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewName)
        TextView textViewName;
        @BindView(R.id.textViewNumber)
        TextView textViewNumber;
        @BindView(R.id.user_photo)
        CircleImageView userPhoto;

        @OnClick(R.id.layoutContac)
        public void onViewClicked() {
            onClick.onContactClick(getAdapterPosition());
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    public interface onClick {
        void onContactClick(int position);
    }

}
