package com.lekhajokha.udharkhata.home.transaction.model;

public class TransactionModel {

    int txn_id,customer_id;
    String txn_type,txn_amount,txn_date,description,created_at,updated_at;

    public TransactionModel() {
    }

    public TransactionModel(int txn_id, int customer_id, String txn_type, String txn_amount, String txn_date, String description, String created_at, String updated_at) {
        this.txn_id = txn_id;
        this.customer_id = customer_id;
        this.txn_type = txn_type;
        this.txn_amount = txn_amount;
        this.txn_date = txn_date;
        this.description = description;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getTxn_id() {
        return txn_id;
    }

    public void setTxn_id(int txn_id) {
        this.txn_id = txn_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getTxn_type() {
        return txn_type;
    }

    public void setTxn_type(String txn_type) {
        this.txn_type = txn_type;
    }

    public String getTxn_amount() {
        return txn_amount;
    }

    public void setTxn_amount(String txn_amount) {
        this.txn_amount = txn_amount;
    }

    public String getTxn_date() {
        return txn_date;
    }

    public void setTxn_date(String txn_date) {
        this.txn_date = txn_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
