package com.lekhajokha.udharkhata.home.transaction;

import com.lekhajokha.udharkhata.home.transaction.add_transaction.TransactionAddResponse;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionListResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface TransactionApi {

    @GET("GetTxnList")
    Call<TransactionListResponse> callTransactionList(
           @Header("ACCESS-TOKEN") String token,  @Query("customer_id") String Customer_id);

    @Multipart
    @POST("AddTxn")
    Call<TransactionAddResponse> callTransationAddApi(@Header("ACCESS-TOKEN") String token,
                                                      @PartMap Map<String, RequestBody> params,
                                                      @Part MultipartBody.Part bodyImg1);

    @GET("report")
    Call<ResponseBody> callDownloadFileWithCustomerID( @Query("token")String token, @Query("customer_id") String Customer_id );

}
