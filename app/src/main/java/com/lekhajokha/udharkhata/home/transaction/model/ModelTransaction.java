package com.lekhajokha.udharkhata.home.transaction.model;

public class ModelTransaction {

    public String type,amount,date,time,descriotion;

    public ModelTransaction(String type, String amount, String date, String time, String descriotion) {
        this.type = type;
        this.amount = amount;
        this.date = date;
        this.time = time;
        this.descriotion = descriotion;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescriotion() {
        return descriotion;
    }

    public void setDescriotion(String descriotion) {
        this.descriotion = descriotion;
    }
}
