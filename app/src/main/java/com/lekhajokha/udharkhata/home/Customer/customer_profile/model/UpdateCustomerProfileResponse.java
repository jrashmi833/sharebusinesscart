
package com.lekhajokha.udharkhata.home.Customer.customer_profile.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class UpdateCustomerProfileResponse {

    @Expose
    private Long code;
    @Expose
    private UpdateCustomerProfileData updateCustomerProfileData;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public UpdateCustomerProfileData getUpdateCustomerProfileData() {
        return updateCustomerProfileData;
    }

    public void setUpdateCustomerProfileData(UpdateCustomerProfileData updateCustomerProfileData) {
        this.updateCustomerProfileData = updateCustomerProfileData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
