package com.lekhajokha.udharkhata.home.Customer.customer_profile;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.contact_list.add_customer.model.GetCustomerAddedResponse;
import com.lekhajokha.udharkhata.home.Customer.CustomerPresenter;
import com.lekhajokha.udharkhata.home.Customer.CustomerView;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.DeleteCustomerResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerProfileUpdateResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.UpdateCustomerParameter;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.CustomerList;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerListResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.UpdateCustomerProfileResponse;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.login_screen.LoginThroughActivity;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;
import com.lekhajokha.udharkhata.utility.crop_image.CropImage;
import com.lekhajokha.udharkhata.utility.crop_image.CropImageView;
import com.lekhajokha.udharkhata.utility.crop_image.LargeViewImageActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.Manifest.permission.CAMERA;
import static android.os.Build.VERSION_CODES.M;
import static com.lekhajokha.udharkhata.profile.BusinessProfileActivity.CAMERA_PERMISSION_REQUEST_CODE;
import static com.lekhajokha.udharkhata.profile.BusinessProfileActivity.CAMERA_PIC_REQUEST;
import static com.lekhajokha.udharkhata.profile.BusinessProfileActivity.GALLERY_PERMISSION_REQUEST_CODE;
import static com.lekhajokha.udharkhata.profile.BusinessProfileActivity.SELECT_FROM_GALLERY;


public class CustomerProfileFragment extends Fragment implements CustomerView {
    public static final String TAG = CustomerProfileFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.backButton)
    ImageView backButton;
    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.layoutNameButton)
    RelativeLayout layoutNameButton;
    @BindView(R.id.textViewMobileNumber)
    TextView textViewMobileNumber;
    @BindView(R.id.layoutMobileNumberButton)
    RelativeLayout layoutMobileNumberButton;
    @BindView(R.id.textViewEmail)
    TextView textViewEmail;
    @BindView(R.id.layoutEmailButton)
    RelativeLayout layoutEmailButton;
    @BindView(R.id.textViewAddress)
    TextView textViewAddress;
    @BindView(R.id.layoutAddressButton)
    RelativeLayout layoutAddressButton;
    @BindView(R.id.switchSms)
    Switch switchSms;
    @BindView(R.id.switchEmail)
    Switch switchEmail;
    @BindView(R.id.DeleteCustomerButton)
    TextView DeleteCustomerButton;
    Unbinder unbinder;
    @BindView(R.id.customerprofileImage)
    CircleImageView customerprofileImage;
    @BindView(R.id.EditCameraButton)
    RelativeLayout EditCameraButton;
    private String mParam2;
    private View view;
    private Context context;
    private CustomerList customerModel;
    private CustomerPresenter customerPresenter;
    Uri mImageCaptureUri;
    private static final int CROP_INTENT_PERMISSION = 104;
    public CustomerProfileFragment() {
    }

    public static CustomerProfileFragment newInstance(CustomerList customerList) {
        CustomerProfileFragment fragment = new CustomerProfileFragment();
        Bundle args = new Bundle();
        args.putSerializable( ARG_PARAM1, customerList );

        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        if (getArguments() != null) {
            customerModel = (CustomerList) getArguments().getSerializable( ARG_PARAM1 );
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate( R.layout.fragment_customer_profile, container, false );
        unbinder = ButterKnife.bind( this, view );
        customerPresenter = new CustomerPresenter( context, this );

        initializeData();


        switchEmail.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (((HomeActivity) context).isInternetConneted()) {
                        switchEmail.setChecked( true );
                        UpdateCustomer();
                    } else {
                        switchEmail.setChecked( false );
                        Toast.makeText( context, "No internet Connection", Toast.LENGTH_SHORT ).show();
                    }
                } else {
                    if (((HomeActivity) context).isInternetConneted()) {
                        switchEmail.setChecked( false );
                        UpdateCustomer();
                    } else {
                        switchEmail.setChecked( true );
                        Toast.makeText( context, "No internet Connection", Toast.LENGTH_SHORT ).show();
                    }
                }
            }
        } );

        switchSms.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (((HomeActivity) context).isInternetConneted()) {
                        switchSms.setChecked( true );
                        UpdateCustomer();
                    } else {
                        switchSms.setChecked( false );
                        Toast.makeText( context, "No internet Connection", Toast.LENGTH_SHORT ).show();
                    }
                } else {
                    if (((HomeActivity) context).isInternetConneted()) {
                        switchSms.setChecked( false );
                        UpdateCustomer();
                    } else {
                        switchSms.setChecked( true );
                        Toast.makeText( context, "No internet Connection", Toast.LENGTH_SHORT ).show();
                    }
                }
            }
        } );


        return view;
    }

    private void initializeData() {
        textViewName.setText( customerModel.getCustomerName() );
        textViewAddress.setText( customerModel.getAddress()+"" );
        textViewEmail.setText( customerModel.getCustomerEmail()+"" );
        textViewMobileNumber.setText( customerModel.getCustomerPhone() );
        if (customerModel.getTxnEmailStatus().equals( "1" )) {
            switchEmail.setChecked( true );
        } else {
            switchEmail.setChecked( false );
        }
        if (customerModel.getTxnSmsStatus().equals( "1" )) {
            switchSms.setChecked( true );
        } else {
            switchSms.setChecked( false );
        }

        if(null != customerModel.getCustomerPhoto() && !customerModel.getCustomerPhoto().equalsIgnoreCase("")){
            Glide.with(context).load(customerModel.getCustomerPhoto()).into(customerprofileImage);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach( context );
        this.context = context;
    }

    @OnClick(R.id.backButton)
    public void onViewClicked() {
        getActivity().finish();
        getActivity().startActivity( getActivity().getIntent() );
        AppUtils.startFromRightToLeft( context );
    }

    @OnClick({R.id.layoutNameButton, R.id.layoutMobileNumberButton, R.id.textViewSaveButton, R.id.layoutEmailButton, R.id.DeleteCustomerButton
            , R.id.layoutAddressButton, R.id.customerprofileImage, R.id.EditCameraButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.EditCameraButton:
                if (Build.VERSION.SDK_INT >= M) {
                    if (checkCropIntentPermission())
                        openCropIntent();
                    else
                        requestCropIntentPermission();
                } else
                    openCropIntent();
                break;
            case R.id.customerprofileImage:
                if(null != customerModel.getCustomerPhoto() && !customerModel.getCustomerPhoto().equalsIgnoreCase("")){
                    startActivity(new Intent(context, LargeViewImageActivity.class)
                            .putExtra("IMG",customerModel.getCustomerPhoto()));
                }
                break;
            case R.id.layoutNameButton:
                dialogName();
                break;
            case R.id.layoutEmailButton:
                dialogEmail();
                break;
            case R.id.layoutMobileNumberButton:
                dialogMobile();
                break;
            case R.id.layoutAddressButton:
                dialogAddress();
                break;
            case R.id.DeleteCustomerButton:
                deleteCustomer();
                break;
            case R.id.textViewSaveButton:
                UpdateCustomer();
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"From Camera", "From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder( context );
        builder.setTitle( "Please choose an Image" );
        builder.setItems( options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals( "From Camera" )) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkCameraPermission())
                            cameraIntent();
                        else
                            requestPermission();
                    } else
                        cameraIntent();
                } else if (options[item].equals( "From Gallery" )) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkGalleryPermission())
                            galleryIntent();
                        else
                            requestGalleryPermission();
                    } else
                        galleryIntent();
                } else if (options[item].equals( "Cancel" )) {
                    dialog.dismiss();
                }
            }
        } );
        builder.create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult( requestCode, permissions, grantResults );
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                }
                break;
            case GALLERY_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    galleryIntent();
                }
                break;
            case CROP_INTENT_PERMISSION:
                if (grantResults.length >2 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    openCropIntent();
                }
                break;

        }
    }


    private void galleryIntent() {
        Intent intent = new Intent().setType( "image/*" ).setAction( Intent.ACTION_GET_CONTENT );
        startActivityForResult( Intent.createChooser( intent, "Select Picture" ), SELECT_FROM_GALLERY );
    }

    private void cameraIntent() {
        Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
        intent.putExtra( MediaStore.EXTRA_OUTPUT, mImageCaptureUri );
        startActivityForResult( intent, CAMERA_PIC_REQUEST );

    }


    private void requestPermission() {
        requestPermissions( new String[]{CAMERA}, CAMERA_PERMISSION_REQUEST_CODE );
    }


    private void requestGalleryPermission() {
        requestPermissions( new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PERMISSION_REQUEST_CODE );
    }

    private boolean checkCameraPermission() {
        int result1 = ContextCompat.checkSelfPermission( context, CAMERA );
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkGalleryPermission() {
        int result2 = ContextCompat.checkSelfPermission( context, Manifest.permission.READ_EXTERNAL_STORAGE );
        return result2 == PackageManager.PERMISSION_GRANTED;
    }
    public File getUserImageFile(Bitmap bitmap) {
        try {
            File f = new File( context.getCacheDir(), "images.png" );
            f.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress( Bitmap.CompressFormat.JPEG, 80 /*ignored for PNG*/, bos );
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream( f );
            fos.write( bitmapdata );
            fos.flush();
            fos.close();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @SuppressLint("CheckResult")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode, resultCode, data );
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK && null != data) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Bitmap productImageBitmap = null ;
            try {
                productImageBitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), result.getUri());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (null != productImageBitmap) {
                customerprofileImage.setImageBitmap(productImageBitmap);
                callProfileImageUpdateApi(  getUserImageFile( productImageBitmap ) );
            }
        }else if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK && null != data) {
            Bitmap productImageBitmap = (Bitmap) data.getExtras().get( "data" );
            if (null != productImageBitmap) {
                callProfileImageUpdateApi(  getUserImageFile( productImageBitmap ) );
            }

        } else if (requestCode == SELECT_FROM_GALLERY && resultCode == Activity.RESULT_OK && null != data) {
            Uri galleryURI = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap( context.getContentResolver(), galleryURI );
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (null != bitmap) {
                callProfileImageUpdateApi(  getUserImageFile( bitmap ) );
            }
        }
    }




    private void callProfileImageUpdateApi(File userImageFile) {

        Map<String, RequestBody> params = new HashMap<>();
        params.put("customer_id", Constant.getRequestBody(customerModel.getCustomerId()));
        MultipartBody.Part partbody1;

        RequestBody reqFile1 = RequestBody.create( MediaType.parse("image"), userImageFile);
        partbody1 = MultipartBody.Part.createFormData("customer_photo", userImageFile.getName(), reqFile1);

        customerPresenter.callCustomerProfileUpdate( SharedPreference.getInstance( context ).getUser().getAccessToken(), params,partbody1 );
    }



    private void UpdateCustomer() {
        UpdateCustomerParameter updateCustomerParameter = new UpdateCustomerParameter();
        updateCustomerParameter.setCustomerName( textViewName.getText().toString() );
        updateCustomerParameter.setCustomerPhone( textViewMobileNumber.getText().toString() );
        updateCustomerParameter.setCustomerEmail( textViewEmail.getText().toString() );
        updateCustomerParameter.setAddress( textViewAddress.getText().toString() );
        updateCustomerParameter.setCustomerId( customerModel.getCustomerId() );
        if (switchEmail.isChecked()) {
            updateCustomerParameter.setTxnEmailStatus( "1" );
        } else {
            updateCustomerParameter.setTxnEmailStatus( "0" );
        }

        if (switchSms.isChecked()) {
            updateCustomerParameter.setTxnSmsStatus( "1" );
        } else {
            updateCustomerParameter.setTxnSmsStatus( "0" );
        }
        customerPresenter.callUpdateCustomerApi( updateCustomerParameter, SharedPreference.getInstance( context ).getUser().getAccessToken(), context );

    }

    private void deleteCustomer() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder( context );
        alertDialog.setTitle( "Confirm Delete..." );
        alertDialog.setMessage( "Are you sure you want delete this Customer ?" );
        alertDialog.setIcon( R.drawable.delete );

        alertDialog.setPositiveButton( "YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                customerPresenter.callDeleteCustomer( customerModel.getCustomerId(), SharedPreference.getInstance( context ).getUser().getAccessToken(), context );
            }
        } );

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton( "NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        } );

        alertDialog.show();

//        customerPresenter.callDeleteCustomer(String.valueOf(customerModel.getCust_id()), SharedPreference.getInstance(context).getUser().getAccessToken(), context);
    }

    private void dialogEmail() {
        final Dialog dialog = new Dialog( context );
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        dialog.getWindow().setBackgroundDrawableResource( android.R.color.transparent );
        dialog.setContentView( R.layout.layout_dialog_email );
        dialog.show();

        EditText editTextEmail = dialog.findViewById( R.id.editTextEmail );
        Button buttonSave = dialog.findViewById( R.id.buttonSave );
        ImageView buttonCancel = dialog.findViewById( R.id.buttonCancel );
        editTextEmail.setText( customerModel.getCustomerEmail()+"" );

        buttonCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        } );

        buttonSave.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextEmail.getText().toString().length() != 0) {
                    UpdateCustomerParameter updateCustomerParameter = new UpdateCustomerParameter();
                    updateCustomerParameter.setCustomerId( customerModel.getCustomerId() );
                    updateCustomerParameter.setCustomerEmail( editTextEmail.getText().toString() );
                    customerPresenter.callUpdateCustomerApi( updateCustomerParameter, SharedPreference.getInstance( context ).getUser().getAccessToken(), context );

//                    customer.upDateCustomerEmail(editTextEmail.getText().toString(), customerModel.getId());
                    dialog.cancel();
                   /* Fragment frg = null;
                    frg = getActivity().getSupportFragmentManager().findFragmentByTag(CustomerProfileFragment.TAG);
                    final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.detach(frg);
                    ft.attach(frg);
                    ft.commit();
*/
                }
            }
        } );
    }

    private void dialogMobile() {
        final Dialog dialog = new Dialog( context );
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        dialog.getWindow().setBackgroundDrawableResource( android.R.color.transparent );
        dialog.setContentView( R.layout.layout_dialog_mobile );
        dialog.show();

        EditText editTextMobileNumber = dialog.findViewById( R.id.editTextMobileNumber );
        Button buttonSave = dialog.findViewById( R.id.buttonSave );
        ImageView buttonCancel = dialog.findViewById( R.id.buttonCancel );
        editTextMobileNumber.setText( customerModel.getCustomerPhone() );

        buttonCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        } );

        buttonSave.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateCustomerParameter updateCustomerParameter = new UpdateCustomerParameter();
                updateCustomerParameter.setCustomerId( customerModel.getCustomerId() );
                updateCustomerParameter.setCustomerPhone( editTextMobileNumber.getText().toString() );
                customerPresenter.callUpdateCustomerApi( updateCustomerParameter, SharedPreference.getInstance( context ).getUser().getAccessToken(), context );
//                customer.upDateCustomerPhone(editTextMobileNumber.getText().toString(), customerModel.getCustomerId());
                dialog.cancel();
//                Fragment frg = null;
//                frg = getActivity().getSupportFragmentManager().findFragmentByTag(CustomerProfileFragment.TAG);
//                final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                ft.detach(frg);
//                ft.attach(frg);
//                ft.commit();
            }
        } );

    }

    private void dialogName() {

        final Dialog dialog = new Dialog( context );
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        dialog.getWindow().setBackgroundDrawableResource( android.R.color.transparent );
        dialog.setContentView( R.layout.layout_dialog_name );
        dialog.show();

        EditText editTextName = dialog.findViewById( R.id.editTextName );
        Button buttonSave = dialog.findViewById( R.id.buttonSave );
        ImageView buttonCancel = dialog.findViewById( R.id.buttonCancel );
        editTextName.setText( customerModel.getCustomerName() );
        buttonCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        } );

        buttonSave.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextName.getText().toString().length() != 0) {

                    UpdateCustomerParameter updateCustomerParameter = new UpdateCustomerParameter();
                    updateCustomerParameter.setCustomerId( customerModel.getCustomerId() );
                    updateCustomerParameter.setCustomerName( editTextName.getText().toString() );
                    customerPresenter.callUpdateCustomerApi( updateCustomerParameter, SharedPreference.getInstance( context ).getUser().getAccessToken(), context );

//                    customer.upDateCustomerName(editTextName.getText().toString(), customerModel.getId());
                    dialog.cancel();
           /*         Fragment frg = null;
                    frg = getActivity().getSupportFragmentManager().findFragmentByTag(CustomerProfileFragment.TAG);
                    final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.detach(frg);
                    ft.attach(frg);
                    ft.commit();*/

                }

            }
        } );

    }

    private void dialogAddress() {

        final Dialog dialog = new Dialog( context );
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        dialog.getWindow().setBackgroundDrawableResource( android.R.color.transparent );
        dialog.setContentView( R.layout.layout_dialog_address );
        dialog.show();

        EditText editTextAddress = dialog.findViewById( R.id.editTextAddress );
        Button buttonSave = dialog.findViewById( R.id.buttonSave );
        ImageView buttonCancel = dialog.findViewById( R.id.buttonCancel );
        editTextAddress.setText( customerModel.getAddress()+"");
        buttonCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        } );

        buttonSave.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextAddress.getText().toString().length() != 0) {
                    UpdateCustomerParameter updateCustomerParameter = new UpdateCustomerParameter();
                    updateCustomerParameter.setCustomerId( customerModel.getCustomerId() );
                    updateCustomerParameter.setAddress( editTextAddress.getText().toString() );
                    customerPresenter.callUpdateCustomerApi( updateCustomerParameter, SharedPreference.getInstance( context ).getUser().getAccessToken(), context );

//                    customer.upDateCustomerAddress(editTextAddress.getText().toString(), customerModel.getId());
                    dialog.cancel();
                 /*   Fragment frg = null;
                    frg = getActivity().getSupportFragmentManager().findFragmentByTag(CustomerProfileFragment.TAG);
                    final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.detach(frg);
                    ft.attach(frg);
                    ft.commit();*/

                }
            }
        } );

    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode( true );
        getView().requestFocus();
        getView().setOnKeyListener( new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().finish();
                    getActivity().startActivity( getActivity().getIntent() );
                    AppUtils.startFromRightToLeft( context );
                    return true;

                }
                return false;
            }
        } );
    }

    @Override
    public void onSuccessCustomerUpdate(GetCustomerProfileUpdateResponse response) {
//        "customer_id": "16",
//                "user_id": "5",
//                "customer_name": "ravindra",
//                "customer_phone": "917930347",
//                "customer_email": "ravi.kkb@gmail.com",
//                "customer_photo": "http://codeinfo.in/lekhajokha/uploads/avatar/default.png",
//                "address": "indore",
//                "txn_sms_status": "1",
//                "txn_email_status": "1",
//                "last_txn_type": "none",
//                "last_txn_amount": "0",
//                "last_txn_date": "2020-01-13",
//                "total_amount": "0",
//                "total_amount_type": "none",
//                "is_delete": "0",
//                "created_at": "2020-01-13 14:08:49",
//                "updated_at": "2020-01-13 14:46:31"

        customerModel.setCustomerName( response.getData().getCustomerName() );
        customerModel.setCustomerEmail( response.getData().getCustomerEmail() );
        customerModel.setCustomerPhone( response.getData().getCustomerPhone() );
        customerModel.setAddress( response.getData().getAddress() );
        customerModel.setTxnSmsStatus( response.getData().getTxnSmsStatus() );
        customerModel.setTxnEmailStatus( response.getData().getTxnEmailStatus() );
        initializeData();

    }

    private void openCropIntent() {
        try {
            CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .setMinCropResultSize(400, 400)
                    .start(context,this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @RequiresApi(api = M)
    private void requestCropIntentPermission() {
        requestPermissions(new String[]{CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, CROP_INTENT_PERMISSION);
    }

    private boolean checkCropIntentPermission() {
        int result1 = ContextCompat.checkSelfPermission(context, CAMERA);
        int result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result1 == PackageManager.PERMISSION_GRANTED
                && result2 == PackageManager.PERMISSION_GRANTED
                && result3 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onSuccessAddCustomer(GetCustomerAddedResponse getCustomerAddedResponse) {
        // not in use
    }

    @Override
    public void onSuccessCustomerProfileUpdate(UpdateCustomerProfileResponse updateCustomerProfileResponse) {
        if(null != updateCustomerProfileResponse.getUpdateCustomerProfileData()){
            Glide.with( context ).load( updateCustomerProfileResponse.getUpdateCustomerProfileData().getCustomerPhoto() ).into(customerprofileImage );

        }


    }

    @Override
    public void onSuccessDeleteCustomer(DeleteCustomerResponse deleteCustomerResponse) {
        getActivity().finish();
        getActivity().startActivity( getActivity().getIntent() );
        AppUtils.startFromRightToLeft( context );
    }

    @Override
    public void onSuccessGetCustomerList(GetCustomerListResponse getCustomerList) {
        // not in use
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        SharedPreference.getInstance( context ).setBoolean( Constant.IS_USER_LOGIN, false );
        Intent intent = new Intent( context, LoginThroughActivity.class );
        intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
        startActivity( intent );
        AppUtils.startFromRightToLeft( context );
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText( context, errorMessage, Toast.LENGTH_SHORT ).show();
    }

    @Override
    public void onShowLoader() {
        ((HomeActivity) context).showLoader();
    }

    @Override
    public void onHideLoader() {
        ((HomeActivity) context).hideLoader();
    }


}
