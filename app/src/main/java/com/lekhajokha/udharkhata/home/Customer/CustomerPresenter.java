package com.lekhajokha.udharkhata.home.Customer;

import android.content.Context;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.DeleteCustomerResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerProfileUpdateResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.UpdateCustomerParameter;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerListResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.UpdateCustomerProfileResponse;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.contact_list.add_customer.model.AddCustomerParameter;
import com.lekhajokha.udharkhata.contact_list.add_customer.model.GetCustomerAddedResponse;

import java.io.File;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.PartMap;

public class CustomerPresenter implements CustomerManagerCallBack {
    private Context context ;
    private CustomerView customerView;
    private CustomerManager customerManager;

    public CustomerPresenter(Context context ,CustomerView customerView) {
        this.context = context ;
        this.customerView = customerView;
        this.customerManager = new CustomerManager(context ,this);
    }


    public void callUpdateCustomerApi(UpdateCustomerParameter updateCustomerParameter, String token, Context context){
        if(((HomeActivity)context).isInternetConneted()) {
            customerView.onShowLoader();
            customerManager.callUpdateCustomerApi(updateCustomerParameter,token);
        }else {
            customerView.onError(context.getResources().getString( R.string.no_internet_connection));
        }

    }
    public void callAddCustomer(AddCustomerParameter addCustomerParameter, String token, Context context){
        if(((HomeActivity)context).isInternetConneted()) {
            customerView.onShowLoader();
            customerManager.callAddCustomerApi(addCustomerParameter,token);
        }else {
            customerView.onError(context.getResources().getString( R.string.no_internet_connection));
        }
    }
    public void callDeleteCustomer(String id, String token, Context context){
        if(((HomeActivity)context).isInternetConneted()) {
            customerView.onShowLoader();
            customerManager.callDeleteCustomerApi(id,token);
        }else {
            customerView.onError(context.getResources().getString( R.string.no_internet_connection));
        }
    }
    public void callGetCustomerList( boolean isShowLoader){
        if(((HomeActivity)context).isInternetConneted()) {
            if (isShowLoader){
                customerView.onShowLoader();
            }
            customerManager.callGetCustomerListApi();
        }else {
            customerView.onError(context.getResources().getString( R.string.no_internet_connection));
        }
    }


    public void callCustomerProfileUpdate(String token , Map<String, RequestBody> params,MultipartBody.Part file){
        if(((HomeActivity)context).isInternetConneted()) {
            customerView.onShowLoader();
            customerManager.callCustomerProfileUpdateApi(token ,params,file);
        }else {
            customerView.onError(context.getResources().getString( R.string.no_internet_connection));
        }
    }
    @Override
    public void onSuccessCustomerUpdate(GetCustomerProfileUpdateResponse getCustomerProfileUpdateResponse) {
        customerView.onHideLoader();
        customerView.onSuccessCustomerUpdate(getCustomerProfileUpdateResponse);
    }

    @Override
    public void onSuccessCustomerProfileUpdate(UpdateCustomerProfileResponse updateCustomerProfileResponse) {
        customerView.onHideLoader();
        customerView.onSuccessCustomerProfileUpdate(updateCustomerProfileResponse);
    }

    @Override
    public void onSuccessAddCustomer(GetCustomerAddedResponse getCustomerAddedResponse) {
        customerView.onHideLoader();
        customerView.onSuccessAddCustomer(getCustomerAddedResponse);
    }

    @Override
    public void onSuccessDeleteCustomer(DeleteCustomerResponse deleteCustomerResponse) {
        customerView.onHideLoader();
        customerView.onSuccessDeleteCustomer(deleteCustomerResponse);
    }

    @Override
    public void onSuccessGetCustomerList(GetCustomerListResponse getCustomerList) {
        customerView.onHideLoader();
        customerView.onSuccessGetCustomerList(getCustomerList);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        customerView.onHideLoader();
        customerView.onTokenChangeError(errorMessage);
    }

    @Override
    public void onError(String errorMessage) {
        customerView.onHideLoader();
        customerView.onError(errorMessage);
    }
}
