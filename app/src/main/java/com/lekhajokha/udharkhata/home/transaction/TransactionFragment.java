package com.lekhajokha.udharkhata.home.transaction;


import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.CustomerProfileFragment;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.CustomerList;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.home.transaction.Create_payment.CreatePaymentFragment;
import com.lekhajokha.udharkhata.home.transaction.adapter.AdapterTransactions;
import com.lekhajokha.udharkhata.home.transaction.add_transaction.TransactionAddResponse;
import com.lekhajokha.udharkhata.home.transaction.detail_transaction.DetailFragment;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionArrayList;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionListResponse;
import com.lekhajokha.udharkhata.login_screen.LoginThroughActivity;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.ResponseBody;


public class TransactionFragment extends Fragment implements TransactionCallBackListener.TransactionView, PopupMenu.OnMenuItemClickListener, AdapterTransactions.onClick {
    public static final String TAG = TransactionFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int CALL_REQUEST = 100;
    @BindView(R.id.recyclerViewTransaction)
    RecyclerView recyclerViewTransaction;

    Unbinder unbinder;
    @BindView(R.id.backButton)
    ImageView backButton;
    @BindView(R.id.buttonMenu)
    ImageView buttonMenu;
    @BindView(R.id.buttonCredit)
    LinearLayout buttonCredit;
    @BindView(R.id.ButtonDebit)
    LinearLayout ButtonDebit;
    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.textViewBalance)
    TextView textViewBalance;
    @BindView(R.id.layoutButton)
    LinearLayout layoutButton;
    @BindView(R.id.textViewBalanceType)
    TextView textViewBalanceType;
    @BindView(R.id.layoutCustomerProfile)
    LinearLayout layoutCustomerProfile;
    @BindView(R.id.emptyLayout)
    LinearLayout emptyLayout;
    private View view;
    private String customerName;
    private String customerId;
    private Context context;
    private AdapterTransactions adapterTransactions;
    private TransactionPresenter transactionPresenter;
    private ArrayList<TransactionArrayList> transactionArrayList;
    //    private DBHelperTransaction transaction;
//    private DBHelperCustomer customerDBHelper;
    private CustomerList customerModel;
    private String Currency;
    private String settlementType = null ;
    private Float settlementAmount = 0f ;
    public TransactionFragment() {
        // Required empty public constructor
    }


    public static TransactionFragment newInstance(CustomerList customerModel) {
        TransactionFragment fragment = new TransactionFragment();
        Bundle args = new Bundle();
        args.putSerializable( ARG_PARAM1, customerModel );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        if (getArguments() != null) {
            customerModel = (CustomerList) getArguments().getSerializable( ARG_PARAM1 );
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate( R.layout.fragment_transaction, container, false );
        unbinder = ButterKnife.bind( this, view );
//        customerDBHelper = new DBHelperCustomer(context);
//        transaction = new DBHelperTransaction(context);
        transactionPresenter = new TransactionPresenter( context, this );
        if (SharedPreference.getInstance( context ).getUser().getDisplayCurrency().equals( Constant.symbol )) {
            Currency = SharedPreference.getInstance( context ).getUser().getCurrencySymbol();
        } else {
            Currency = SharedPreference.getInstance( context ).getUser().getCurrencyCode();
        }


        customerName = customerModel.getCustomerName();
        customerId = customerModel.getCustomerId();
        textViewName.setText( customerName );
        transactionPresenter.callTransationListApi( customerId + "" );


        return view;

    }

    private void setAdapter() {
        adapterTransactions = new AdapterTransactions( this,context,transactionArrayList );
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( context, RecyclerView.VERTICAL, false );
        if (null != recyclerViewTransaction) {
            recyclerViewTransaction.setAdapter( adapterTransactions );
            recyclerViewTransaction.setLayoutManager( layoutManager );
            recyclerViewTransaction.scrollToPosition(transactionArrayList.size()-1);
//            SnapHelper snapHelper = new LinearSnapHelper();
//            snapHelper.attachToRecyclerView(recyclerViewTransaction);
        }
       showBalance();
    }

    private void showBalance() {
        float sumCredit = 0;
        float sumDebit = 0;
        float balance = 0;

        for (int i = 0; i < transactionArrayList.size(); i++) {
            if(!transactionArrayList.get(i).getIsDelete().equals("1")) {
                if (transactionArrayList.get(i).getTxnType().equalsIgnoreCase("credit")) {
                    sumCredit += Float.valueOf(transactionArrayList.get(i).getTxnAmount());
                } else {
                    sumDebit += Float.valueOf(transactionArrayList.get(i).getTxnAmount());
                }
            }
        }

        if (sumCredit > sumDebit) {
            balance = sumCredit - sumDebit;
            textViewBalance.setText( Currency + " " + balance );
            textViewBalanceType.setText( " you will give" );
//            textViewBalance.setTextColor(context.getResources().getColor(R.color.green));
            settlementType = "debit";
            settlementAmount = balance ;
        } else if (sumDebit > sumCredit) {
            balance = sumDebit - sumCredit;
            textViewBalance.setText( Currency + " " + balance );
            textViewBalanceType.setText( "you will receive" );
            settlementType = "credit";
            settlementAmount = balance ;
//            textViewBalance.setTextColor(context.getResources().getColor(R.color.red));
        } else {
            textViewBalance.setText(Currency + " 0");
            settlementType = null;
            settlementAmount = 0f ;
        }


    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach( context );
        this.context = context;
    }

    @OnClick({R.id.backButton, R.id.buttonMenu, R.id.buttonCredit, R.id.ButtonDebit, R.id.menuSmsButton, R.id.menuEmailButton, R.id.menuCallButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                getActivity().finish();
                getActivity().startActivity( getActivity().getIntent() );
                AppUtils.startFromRightToLeft( context );
                break;
            case R.id.buttonMenu:
                PopupMenu popup = new PopupMenu( context, view );
                popup.setOnMenuItemClickListener( this );
                popup.inflate( R.menu.menu_customer );
                popup.show();
                break;
            case R.id.menuCallButton:
                if (null != customerModel.getCustomerPhone() && !customerModel.getCustomerPhone().equalsIgnoreCase("")
                        && !customerModel.getCustomerPhone().equalsIgnoreCase("null")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        makeCall( customerModel.getCustomerPhone() );
                    }
                } else {
                    Toast.makeText( context, "Please enter the Number", Toast.LENGTH_SHORT ).show();
                    ((HomeActivity) context).addFragmentFragment( CustomerProfileFragment.newInstance( customerModel ), CustomerProfileFragment.TAG, true );
                }
                break;

            case R.id.menuSmsButton:
                if (null != customerModel.getCustomerPhone() && !customerModel.getCustomerPhone().equalsIgnoreCase("")
                        && !customerModel.getCustomerPhone().equalsIgnoreCase("null")) {
                    String number = customerModel.getCustomerPhone();  // The number on which you want to send SMS
                    startActivity( new Intent( Intent.ACTION_VIEW, Uri.fromParts( "sms", number, null ) ) );
                } else {
                    Toast.makeText( context, "Please enter the Number", Toast.LENGTH_SHORT ).show();

                    ((HomeActivity) context).addFragmentFragment( CustomerProfileFragment.newInstance( customerModel ), CustomerProfileFragment.TAG, true );

                }
                break;

            case R.id.menuEmailButton:
                if (null != customerModel.getCustomerEmail() && !customerModel.getCustomerEmail().equalsIgnoreCase("")
                && !customerModel.getCustomerEmail().equalsIgnoreCase("null")) {

                    String mailto = "mailto:" + customerModel.getCustomerEmail() +
                            "?cc=" + "" +
                            "&subject=" + Uri.encode( "" +
                            "Transaction Detail" ) +
                            "&body=" + Uri.encode( "Write text here ..." );
                    Intent emailIntent = new Intent( Intent.ACTION_SENDTO );
                    emailIntent.setData( Uri.parse( mailto ) );

                    try {
                        startActivity( emailIntent );
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText( context, "Please enter the Email", Toast.LENGTH_SHORT ).show();
                    ((HomeActivity) context).addFragmentFragment( CustomerProfileFragment.newInstance( customerModel ), CustomerProfileFragment.TAG, true );
                }
                break;


            case R.id.buttonCredit:
                if (null != customerModel) {
                    ((HomeActivity) context).replaceFragmentFragment( CreatePaymentFragment.newInstance( customerName, "credit", customerId, customerModel.getTxnSmsStatus(), customerModel.getTxnEmailStatus(), 0 ), CreatePaymentFragment.TAG, true );
                }
                break;
            case R.id.ButtonDebit:
                if (null != customerModel) {
                    ((HomeActivity) context).replaceFragmentFragment( CreatePaymentFragment.newInstance( customerName, "debit", customerId, customerModel.getTxnSmsStatus(), customerModel.getTxnEmailStatus(), 0 ), CreatePaymentFragment.TAG, true );
                }
                break;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void makeCall(String number) {
        Intent callIntent = new Intent( Intent.ACTION_CALL );
        callIntent.setData( Uri.parse( "tel:" + number ) );
        if (checkCallPermission()) {
            startActivity( callIntent );
        } else {
            requestCallPermission();
        }
    }

    private boolean checkCallPermission() {
        int result1 = ContextCompat.checkSelfPermission( context, Manifest.permission.CALL_PHONE );
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestCallPermission() {
        requestPermissions( new String[]{Manifest.permission.CALL_PHONE}, CALL_REQUEST );
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult( requestCode, permissions, grantResults );
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == CALL_REQUEST) {
                makeCall( customerModel.getCustomerName() );
            }
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode( true );
        getView().requestFocus();
        getView().setOnKeyListener( new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().finish();
                    getActivity().startActivity( getActivity().getIntent() );
                    AppUtils.startFromRightToLeft( context );
                    return true;

                }
                return false;
            }
        } );
    }

    @OnClick(R.id.layoutCustomerProfile)
    public void onViewClicked() {
        ((HomeActivity) context).addFragmentFragment( CustomerProfileFragment.newInstance( customerModel ), CustomerProfileFragment.TAG, true );

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onSuccessTransactionList(TransactionListResponse transactionListResponse) {
        transactionArrayList = new ArrayList<>();
        if (null != transactionListResponse.getData()) {
            transactionArrayList.addAll( transactionListResponse.getData() );
            if (transactionArrayList.size() > 0) {
                setAdapter();
                emptyLayout.setVisibility( View.GONE );
                recyclerViewTransaction.setVisibility( View.VISIBLE );
            } else {
                emptyLayout.setVisibility( View.VISIBLE );
                recyclerViewTransaction.setVisibility( View.GONE );
            }

        }
    }

    @Override
    public void onSuccessAddTransactionList(TransactionAddResponse transactionAddResponse, boolean isShare) {
        //not in use
    }

    @Override
    public void onSuccessDownloadStatement(ResponseBody responseBody) {
      writeResponseBodyToDisk(responseBody);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        SharedPreference.getInstance( context ).setBoolean( Constant.IS_USER_LOGIN, false );
        Intent intent = new Intent( context, LoginThroughActivity.class );
        intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
        startActivity( intent );
        AppUtils.startFromRightToLeft( context );
    }

    @Override
    public void onError(String errorMessage) {

        if (errorMessage.equalsIgnoreCase( "Record Not Found" )) {
            emptyLayout.setVisibility( View.VISIBLE );
            recyclerViewTransaction.setVisibility( View.GONE );
        } else {
            ((HomeActivity) context).showToast( errorMessage );
        }

    }

    @Override
    public void onShowLoader() {
        ((HomeActivity) context).onShowLoader();
    }

    @Override
    public void onHideLoader() {
        ((HomeActivity) context).onHideLoader();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSharestatment:
                if(null != customerModel && null != transactionArrayList && transactionArrayList.size() != 0) {
                    transactionPresenter.callShareStatementApi(customerModel.getCustomerId());
                }else {
                    Toast.makeText(context, "No transaction found for Share Statement", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.menuSettle:
                if (null != customerModel && null != settlementType && settlementAmount !=0 ) {
                    ((HomeActivity) context).replaceFragmentFragment( CreatePaymentFragment.newInstance( customerName, settlementType, customerId, customerModel.getTxnSmsStatus(), customerModel.getTxnEmailStatus(), settlementAmount ), CreatePaymentFragment.TAG, true );
                } else {
                    Toast.makeText(context, "No transaction found for Settlement", Toast.LENGTH_SHORT).show();
                }
                return true;

            default:
                return false;
        }
    }

    @Override
    public void onTransactionClick(int position) {
        if(!transactionArrayList.get( position).getIsDelete().equals( "1" )) {
            ((HomeActivity) context).replaceFragmentFragment( DetailFragment.newInstance( transactionArrayList.get( position ), customerName ), DetailFragment.TAG, true );
        }
    }


    private void writeResponseBodyToDisk(ResponseBody body) {

        try {
            File futureStudioIconFile = new File(context.getExternalCacheDir() + File.separator + "LekahJokhaStatement _"+customerName+" .pdf");

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();


                try {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    intent.setType("application/pdf");
                    intent.putExtra(Intent.EXTRA_SUBJECT,"Lekha Jokha Transaction Statement with "+customerName);
                    intent.putExtra(Intent.EXTRA_TEXT, "Share Transaction Statement");
                    intent.putExtra(Intent.EXTRA_STREAM, Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                            FileProvider.getUriForFile(context, "com.lekhajokha.udharkhata.fileprovider", futureStudioIconFile) : Uri.fromFile(futureStudioIconFile));
                    startActivity(Intent.createChooser(intent, "Share Transaction Statement"));
                } catch (Exception e){
                    e.printStackTrace();
                }


//                return true;
            } catch (IOException e) {
//                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
//            return false;
        }


    }

}

