package com.lekhajokha.udharkhata.home.transaction.detail_transaction;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.home.transaction.detail_transaction.model.DeleteTransactionCallBackListener;
import com.lekhajokha.udharkhata.home.transaction.detail_transaction.model.DeleteTransactionPresenter;
import com.lekhajokha.udharkhata.home.transaction.detail_transaction.model.DeleteTransactionResponse;
import com.lekhajokha.udharkhata.home.transaction.edit_transaction.EditTransactionFragment;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionArrayList;
import com.lekhajokha.udharkhata.login_screen.LoginThroughActivity;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.os.Build.VERSION_CODES.M;
import static androidx.appcompat.app.AlertDialog.Builder;

/**
 * Created by Ritu Patidar on 1/31/2020.
 */
@SuppressLint("SetTextI18n")
public class DetailFragment extends Fragment implements DeleteTransactionCallBackListener.DeleteTransactionView {
    private static final int GALLERY_PERMISSION_REQUEST_CODE = 1112;
    public static final String TAG = DetailFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.backButton)
    ImageView backButton;
    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.layoutCustomerProfile)
    LinearLayout layoutCustomerProfile;
    @BindView(R.id.menuShare)
    ImageView menuShare;
    @BindView(R.id.menuDelete)
    ImageView menuDelete;
    @BindView(R.id.linearLayoutMenus)
    LinearLayout linearLayoutMenus;
    @BindView(R.id.layoutheader)
    RelativeLayout layoutheader;
    @BindView(R.id.textViewCurrencySymbol)
    TextView textViewCurrencySymbol;
    @BindView(R.id.textViewCurrency)
    TextView textViewCurrency;
    @BindView(R.id.textViewReceivedDateTime)
    TextView textViewReceivedDateTime;
    @BindView(R.id.textLabelReturnCollect)
    TextView textLabelReturnCollect;
    @BindView(R.id.textViewReturnDateTime)
    TextView textViewReturnDateTime;
    @BindView(R.id.layoutReturnCollectDate)
    LinearLayout layoutReturnCollectDate;
    @BindView(R.id.textLabelRemainder)
    TextView textLabelRemainder;
    @BindView(R.id.textViewReminderDateTime)
    TextView textViewReminderDateTime;
    @BindView(R.id.layoutRemainderDate)
    LinearLayout layoutRemainderDate;
    @BindView(R.id.textViewAddedDateTime)
    TextView textViewAddedDateTime;
    @BindView(R.id.textViewDescription)
    TextView textViewDescription;
    @BindView(R.id.latoutDescription)
    LinearLayout latoutDescription;
    @BindView(R.id.imageDescription)
    ImageView imageDescription;
    @BindView(R.id.textLabelReceivedGiven)
    TextView textLabelReceivedGiven;
    @BindView(R.id.linearLayoutMain)
    LinearLayout linearLayoutMain;
    @BindView(R.id.menuEdit)
    ImageView menuEdit;
    @BindView(R.id.textViewPaymentType)
    TextView textViewPaymentType;
    @BindView(R.id.layoutPaymentType)
    LinearLayout layoutPaymentType;
    private DeleteTransactionPresenter deleteTransactionPresenter;
    String txn_id;
    private TransactionArrayList transactionArrayList;
    private Unbinder unbinder;
    private Context context;
    private String name;
    private View view;


    public DetailFragment() {
        // Required empty public constructor
    }


    public static DetailFragment newInstance(TransactionArrayList transactionArrayList, String name) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, transactionArrayList);
        args.putString(ARG_PARAM2, name);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            transactionArrayList = (TransactionArrayList) getArguments().getSerializable(ARG_PARAM1);
            name = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        String currency;
        deleteTransactionPresenter = new DeleteTransactionPresenter(context, this);

        if (SharedPreference.getInstance(context).getUser().getDisplayCurrency().equals(Constant.symbol)) {
            currency = SharedPreference.getInstance(context).getUser().getCurrencySymbol();

        } else {
            currency = SharedPreference.getInstance(context).getUser().getCurrencyCode();
        }

        textViewAddedDateTime.setText(transactionArrayList.getTxnDate() + "," + transactionArrayList);
        if (transactionArrayList.getTxnType().equalsIgnoreCase("credit")) {
            textViewCurrencySymbol.setText(currency);
            textViewCurrency.setText(transactionArrayList.getTxnAmount());
            textViewCurrencySymbol.setTextColor(context.getResources().getColor(R.color.green));
            textViewCurrency.setTextColor(context.getResources().getColor(R.color.green));
            textViewName.setText("Received From " + name);
            textLabelReceivedGiven.setText("Received");
            textLabelReturnCollect.setText("Return");

        } else {
            textViewCurrencySymbol.setText(currency);
            textViewCurrency.setText(transactionArrayList.getTxnAmount());
            textViewCurrencySymbol.setTextColor(context.getResources().getColor(R.color.red));
            textViewCurrency.setTextColor(context.getResources().getColor(R.color.red));
            textViewName.setText("Given To " + name);
            textLabelReceivedGiven.setText("Given");
            textLabelReturnCollect.setText("Collect");
        }

        if (null != transactionArrayList.getDescription() && !transactionArrayList.getDescription().equalsIgnoreCase("")) {
            textViewDescription.setText(transactionArrayList.getDescription());
            latoutDescription.setVisibility(View.VISIBLE);
        } else {
            latoutDescription.setVisibility(View.GONE);
        }

        if (null != transactionArrayList.getReminderDate() && !transactionArrayList.getReminderDate().equals("")) {
            textViewReminderDateTime.setText(transactionArrayList.getReminderDate() + "");
            layoutRemainderDate.setVisibility(View.VISIBLE);
        } else {
            layoutRemainderDate.setVisibility(View.GONE);
        }


        if (null != transactionArrayList.getReturnDate() && !transactionArrayList.getReturnDate().equals("")) {
            textViewReturnDateTime.setText(transactionArrayList.getReturnDate() + "");
            layoutReturnCollectDate.setVisibility(View.VISIBLE);
        } else {
            layoutReturnCollectDate.setVisibility(View.GONE);
        }

        if (null != transactionArrayList.getPaymentType() && !transactionArrayList.getPaymentType().equals("")) {
            textViewPaymentType.setText(transactionArrayList.getPaymentType() + "");
            layoutPaymentType.setVisibility(View.VISIBLE);
        } else {
            layoutPaymentType.setVisibility(View.GONE);
        }

        textViewReceivedDateTime.setText(transactionArrayList.getTxnDate());
        textViewAddedDateTime.setText(transactionArrayList.getCreatedAt());
        Glide.with(this).load(transactionArrayList.getBillPhoto()).into(imageDescription);

        return view;
    }


    private void shareTransaction() {
        Bitmap b = takeScreenshot(linearLayoutMain);
        File file = new File(context.getExternalCacheDir(), "Transaction.png");
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(file);
            b.compress(Bitmap.CompressFormat.JPEG, 80, fOut);
            fOut.flush();
            fOut.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_TEXT, "Transaction");
            intent.putExtra(Intent.EXTRA_STREAM, Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                    FileProvider.getUriForFile(context, "com.lekhajokha.udharkhata.fileprovider", file) : Uri.fromFile(file));
            startActivity(Intent.createChooser(intent, "Share Transaction Detail"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void requestGalleryPermission() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PERMISSION_REQUEST_CODE);
    }


    private boolean checkGalleryPermission() {
        int result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case GALLERY_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    shareTransaction();
                }
                break;

        }
    }

    private static Bitmap takeScreenshot(View v) {
        v.setDrawingCacheEnabled(true);
        v.buildDrawingCache(true);
        Bitmap b = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        return b;
    }

    @OnClick({R.id.backButton, R.id.menuShare, R.id.menuDelete, R.id.menuEdit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                getActivity().onBackPressed();
                break;
            case R.id.menuShare:
                if (Build.VERSION.SDK_INT >= M) {
                    if (checkGalleryPermission())
                        shareTransaction();
                    else
                        requestGalleryPermission();
                } else
                    shareTransaction();
                break;
            case R.id.menuDelete:
                deleteCustomer();
                break;
            case R.id.menuEdit:
                ((HomeActivity) context).replaceFragmentFragment( EditTransactionFragment.newInstance( transactionArrayList, name ), DetailFragment.TAG, true );

                break;
        }
    }

    private void deleteCustomer() {
        Builder alertDialog = new Builder(context);
        alertDialog.setTitle("Confirm Delete...");
        alertDialog.setMessage("Are you sure you want Delete this Transaction ?");
        alertDialog.setIcon(R.drawable.delete);

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                deleteTransactionPresenter.callDeleteTransaction(transactionArrayList.getTxnId());
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();

//        customerPresenter.callDeleteCustomer(String.valueOf(customerModel.getCust_id()), SharedPreference.getInstance(context).getUser().getAccessToken(), context);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onSuccessDeleteTransaction(DeleteTransactionResponse deleteTransactionResponse) {
        getActivity().onBackPressed();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN, false);
        Intent intent = new Intent(context, LoginThroughActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        AppUtils.startFromRightToLeft(context);
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShowLoader() {
        ((HomeActivity) context).showLoader();
    }

    @Override
    public void onHideLoader() {
        ((HomeActivity) context).hideLoader();
    }
}
