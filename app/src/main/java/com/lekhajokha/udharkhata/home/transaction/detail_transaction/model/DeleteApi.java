package com.lekhajokha.udharkhata.home.transaction.detail_transaction.model;

import com.lekhajokha.udharkhata.home.transaction.add_transaction.TransactionAddResponse;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionListResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface DeleteApi {

    @GET("DeleteTxn")
     Call<DeleteTransactionResponse> callDeleteTransaction(
            @Header("ACCESS-TOKEN") String token, @Query("txn_id") String txn_id);


}
