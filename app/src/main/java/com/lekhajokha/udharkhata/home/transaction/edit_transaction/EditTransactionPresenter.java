package com.lekhajokha.udharkhata.home.transaction.edit_transaction;

import android.content.Context;

import com.lekhajokha.udharkhata.home.HomeActivity;

import com.lekhajokha.udharkhata.home.transaction.edit_transaction.model.EditTransactionResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class EditTransactionPresenter implements EditTransactionCallBackListener.EditTransactionManagerCallback {
    private Context context ;
    private EditTransactionCallBackListener.EditTransactionView customerView;
    private EditTransactionManager customerManager;

    public EditTransactionPresenter(Context context ,EditTransactionCallBackListener.EditTransactionView customerView) {
        this.context = context ;
        this.customerView = customerView;
        this.customerManager = new EditTransactionManager(context ,this);
    }



    public void callEditTransactionAddApi(Map<String, RequestBody> params, MultipartBody.Part partbody1){
        if(((HomeActivity)context).isInternetConneted()) {
            customerView.onShowLoader();
            customerManager.callEditTransactionAddApi(params,partbody1);
        }else {
            customerView.onError("No internet connection!");
        }

    }


    @Override
    public void onSuccessEditTransactionList(EditTransactionResponse editTransactionResponse) {
        customerView.onHideLoader();
        customerView.onSuccessEditTransactionList(editTransactionResponse);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        customerView.onHideLoader();
        customerView.onTokenChangeError(errorMessage);
    }

    @Override
    public void onError(String errorMessage) {
        customerView.onHideLoader();
        customerView.onError(errorMessage);
    }
}
