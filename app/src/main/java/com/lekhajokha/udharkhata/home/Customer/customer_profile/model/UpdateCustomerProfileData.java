
package com.lekhajokha.udharkhata.home.Customer.customer_profile.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class UpdateCustomerProfileData {

    @SerializedName("customer_photo")
    private String customerPhoto;

    public String getCustomerPhoto() {
        return customerPhoto;
    }

    public void setCustomerPhoto(String customerPhoto) {
        this.customerPhoto = customerPhoto;
    }

}
