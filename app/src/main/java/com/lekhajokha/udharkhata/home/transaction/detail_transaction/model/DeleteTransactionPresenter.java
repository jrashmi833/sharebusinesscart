package com.lekhajokha.udharkhata.home.transaction.detail_transaction.model;

import android.content.Context;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.home.HomeActivity;



public class DeleteTransactionPresenter implements DeleteTransactionCallBackListener.DeleteTransactionManagerCallback {
    private Context context ;
    private DeleteTransactionCallBackListener.DeleteTransactionView deleteTransactionView;
    private DeleteTransactionManager deleteTransactionManager;

    public DeleteTransactionPresenter(Context context ,  DeleteTransactionCallBackListener.DeleteTransactionView deleteTransactionView) {
        this.context = context ;
        this.deleteTransactionView = deleteTransactionView;
        this.deleteTransactionManager = new DeleteTransactionManager(context ,this);
    }

    public void callDeleteTransaction(String txn_id){
        if(((HomeActivity)context).isInternetConneted()) {
            deleteTransactionView.onShowLoader();
            deleteTransactionManager.callDeleteTransaction(txn_id);
        }else {
            deleteTransactionView.onError(context.getResources().getString( R.string.no_internet_connection));
        }

    }


    @Override
    public void onSuccessDeleteTransaction(DeleteTransactionResponse deleteTransactionResponse) {
        deleteTransactionView.onHideLoader();
        deleteTransactionView.onSuccessDeleteTransaction(deleteTransactionResponse);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        deleteTransactionView.onHideLoader();
        deleteTransactionView.onTokenChangeError(errorMessage);
    }

    @Override
    public void onError(String errorMessage) {
        deleteTransactionView.onHideLoader();
        deleteTransactionView.onError(errorMessage);
    }

}
