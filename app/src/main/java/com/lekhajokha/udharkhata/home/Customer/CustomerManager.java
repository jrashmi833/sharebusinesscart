package com.lekhajokha.udharkhata.home.Customer;

import android.content.Context;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.DeleteCustomerResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerProfileUpdateResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.UpdateCustomerParameter;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerListResponse;
import com.lekhajokha.udharkhata.contact_list.add_customer.model.AddCustomerParameter;
import com.lekhajokha.udharkhata.contact_list.add_customer.model.GetCustomerAddedResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.UpdateCustomerProfileResponse;
import com.lekhajokha.udharkhata.rest.ServiceGenerator;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.io.IOException;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Multipart;
import retrofit2.http.PartMap;

class CustomerManager {
    private CustomerManagerCallBack customerManagerCallBack ;
    private Context context ;

    CustomerManager(Context context ,CustomerManagerCallBack customerManagerCallBack) {
        this.context = context;
        this.customerManagerCallBack = customerManagerCallBack;
    }

    void callUpdateCustomerApi(UpdateCustomerParameter updateCustomerParameter, String token) {

        CustomerApi logInApi = ServiceGenerator.createService(CustomerApi.class);

        Call<GetCustomerProfileUpdateResponse> imageUploadResponseCall = logInApi.callUpdateCustomer(updateCustomerParameter,token);
        imageUploadResponseCall.enqueue(new Callback<GetCustomerProfileUpdateResponse>() {
            @Override
            public void onResponse(Call<GetCustomerProfileUpdateResponse> call, Response<GetCustomerProfileUpdateResponse> response) {
                if (null!=response.body()){
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                    customerManagerCallBack.onSuccessCustomerUpdate(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        customerManagerCallBack.onTokenChangeError(response.body().getMessage());
                    } else {
                        customerManagerCallBack.onError(response.body().getMessage());
                    }
                }
                } else {
                customerManagerCallBack.onError(context.getResources().getString( R.string.opps_something_went_wrong));
            }


            }

            @Override
            public void onFailure(Call<GetCustomerProfileUpdateResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    customerManagerCallBack.onError(context.getResources().getString( R.string.no_internet_connection));
                } else {
                    customerManagerCallBack.onError(context.getResources().getString( R.string.opps_something_went_wrong));
                }
            }
        });
    }


    void callAddCustomerApi(AddCustomerParameter addCustomerParameter, String token) {

        CustomerApi logInApi = ServiceGenerator.createService(CustomerApi.class);

        Call<GetCustomerAddedResponse> customerAddedResponseCall = logInApi.callAddCustomer(addCustomerParameter,token);
        customerAddedResponseCall.enqueue(new Callback<GetCustomerAddedResponse>() {
            @Override
            public void onResponse(Call<GetCustomerAddedResponse> call, Response<GetCustomerAddedResponse> response) {
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        customerManagerCallBack.onSuccessAddCustomer(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            customerManagerCallBack.onTokenChangeError(response.body().getMessage());
                        } else {
                            customerManagerCallBack.onError(response.body().getMessage());
                        }
                    }
                }else {
                    customerManagerCallBack.onError("opps something went wrong");
                }

            }

            @Override
            public void onFailure(Call<GetCustomerAddedResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    customerManagerCallBack.onError("Network down or no internet connection");
                } else {
                    customerManagerCallBack.onError("opps something went wrong");
                }
            }
        });
    }


    void callDeleteCustomerApi(String id, String token) {

        CustomerApi logInApi = ServiceGenerator.createService(CustomerApi.class);

        Call<DeleteCustomerResponse> customerAddedResponseCall = logInApi.callDeleteCustomer(token,id);
        customerAddedResponseCall.enqueue(new Callback<DeleteCustomerResponse>() {
            @Override
            public void onResponse(Call<DeleteCustomerResponse> call, Response<DeleteCustomerResponse> response) {
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        customerManagerCallBack.onSuccessDeleteCustomer(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            customerManagerCallBack.onTokenChangeError(response.body().getMessage());
                        } else {
                            customerManagerCallBack.onError(response.body().getMessage());
                        }
                    }
                }else {
                    customerManagerCallBack.onError("opps something went wrong");
                }

            }

            @Override
            public void onFailure(Call<DeleteCustomerResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    customerManagerCallBack.onError("Network down or no internet connection");
                } else {
                    customerManagerCallBack.onError("opps something went wrong");
                }
            }
        });
    }

    void callGetCustomerListApi() {

        CustomerApi logInApi = ServiceGenerator.createService(CustomerApi.class);
        String token = SharedPreference.getInstance(context).getUser().getAccessToken() ;
        Call<GetCustomerListResponse> customerAddedResponseCall = logInApi.callGetCustomerList(token);
        customerAddedResponseCall.enqueue(new Callback<GetCustomerListResponse>() {
            @Override
            public void onResponse(Call<GetCustomerListResponse> call, Response<GetCustomerListResponse> response) {
              if(null != response.body()) {
                  if (response.body().getStatus().equalsIgnoreCase("success")) {
                      customerManagerCallBack.onSuccessGetCustomerList(response.body());
                  } else {
                      if (response.body().getCode() == 400) {
                          customerManagerCallBack.onTokenChangeError(response.body().getMessage());
                      } else {
                          customerManagerCallBack.onError(response.body().getMessage());
                      }
                  }
              }else {
                  customerManagerCallBack.onError("opps something went wrong");
              }

            }

            @Override
            public void onFailure(Call<GetCustomerListResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    customerManagerCallBack.onError("Network down or no internet connection");
                } else {
                    customerManagerCallBack.onError("opps something went wrong");
                }
            }
        });
    }

    public void callCustomerProfileUpdateApi(String token , Map<String, RequestBody> params, MultipartBody.Part file) {
        CustomerApi logInApi = ServiceGenerator.createService(CustomerApi.class);
        Call<UpdateCustomerProfileResponse> customerAddedResponseCall = logInApi.callCustomerProfileUpdateApi(token,params,file);
        customerAddedResponseCall.enqueue(new Callback<UpdateCustomerProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateCustomerProfileResponse> call, Response<UpdateCustomerProfileResponse> response) {
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        customerManagerCallBack.onSuccessCustomerProfileUpdate(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            customerManagerCallBack.onTokenChangeError(response.body().getMessage());
                        } else {
                            customerManagerCallBack.onError(response.body().getMessage());
                        }
                    }
                }else {
                    customerManagerCallBack.onError("opps something went wrong");
                }

            }

            @Override
            public void onFailure(Call<UpdateCustomerProfileResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    customerManagerCallBack.onError("Network down or no internet connection");
                } else {
                    customerManagerCallBack.onError("opps something went wrong");
                }
            }
        });
    }
}
