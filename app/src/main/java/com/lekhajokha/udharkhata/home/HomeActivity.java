package com.lekhajokha.udharkhata.home;

import android.Manifest;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.SignUp.LoginSignUpPresenter;
import com.lekhajokha.udharkhata.account.AccountCallbackListener;
import com.lekhajokha.udharkhata.account.AccountFragment;
import com.lekhajokha.udharkhata.account.AccountPresenter;
import com.lekhajokha.udharkhata.account.model.GetCountryResponse;
import com.lekhajokha.udharkhata.account.model.TimezoneResponse;
import com.lekhajokha.udharkhata.account.model.UpdateAccountImageResponse;
import com.lekhajokha.udharkhata.account.model.UpdateAccountParameter;
import com.lekhajokha.udharkhata.account.model.UpdateAccountResponse;
import com.lekhajokha.udharkhata.base.BaseActivity;
import com.lekhajokha.udharkhata.comman_webview.WebViewActivity;
import com.lekhajokha.udharkhata.contact_list.ContactListFragment;
import com.lekhajokha.udharkhata.contact_list.add_customer.model.GetCustomerAddedResponse;
import com.lekhajokha.udharkhata.currency.CurrencyChangeCallback;
import com.lekhajokha.udharkhata.currency.user_currency.UserCurrencyDetailFragment;
import com.lekhajokha.udharkhata.database.DBHelperSingleton;
import com.lekhajokha.udharkhata.helpsupport.HelpsupportFrtragment;
import com.lekhajokha.udharkhata.history.HistoryFragment;
import com.lekhajokha.udharkhata.home.Customer.CustomerPresenter;
import com.lekhajokha.udharkhata.home.Customer.CustomerView;
import com.lekhajokha.udharkhata.home.Customer.adapter.AddAdapter;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.CustomerProfileFragment;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.DeleteCustomerResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerProfileUpdateResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.UpdateCustomerProfileResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.CustomerList;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerListResponse;
import com.lekhajokha.udharkhata.home.transaction.TransactionFragment;
import com.lekhajokha.udharkhata.login_screen.LoginThroughActivity;
import com.lekhajokha.udharkhata.notification.NotificationCountUpdateCallback;
import com.lekhajokha.udharkhata.notification.NotificationFragment;
import com.lekhajokha.udharkhata.profile.BusinessProfileActivity;
import com.lekhajokha.udharkhata.account.ProfileUpdateCallback;
import com.lekhajokha.udharkhata.rest.ServiceGenerator;
import com.lekhajokha.udharkhata.sidemenu.MoreAppsFragment;
import com.lekhajokha.udharkhata.sidemenu.security.SecurityFragment;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.ApplicationSingleton;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Intent.ACTION_VIEW;

public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, AddAdapter.onClick, CustomerView, PopupMenu.OnMenuItemClickListener, CurrencyChangeCallback, ProfileUpdateCallback, NotificationCountUpdateCallback, AccountCallbackListener.AccountView {
    public Context context = this;

    @BindView(R.id.imageViewaddUser)
    ImageView imageViewaddUser;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.addUserButton)
    LinearLayout addUser;
    @BindView(R.id.linearLayoutProfile)
    LinearLayout linearLayoutProfile;
    @BindView(R.id.relativeLayoutFragmentMainContainer)
    RelativeLayout relativeLayoutFragmentMainContainer;
    @BindView(R.id.SecurityButton)
    LinearLayout SecurityButton;
    @BindView(R.id.linear1)
    LinearLayout linear1;
    @BindView(R.id.cardView)
    CardView cardView;
    @BindView(R.id.ProfileButton)
    LinearLayout ProfileButton;

    @BindView(R.id.HelpButton)
    LinearLayout HelpButton;
    @BindView(R.id.AboutUsButton)
    LinearLayout AboutUsButton;
    @BindView(R.id.ShareButton)
    LinearLayout ShareButton;
    @BindView(R.id.PrivacyButton)
    LinearLayout PrivacyButton;
    @BindView(R.id.LogoutButton)
    LinearLayout LogoutButton;
    @BindView(R.id.imageViewUser)
    CircleImageView imageViewUser;
    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.textViewPhone)
    TextView textViewPhone;
    @BindView(R.id.ExpenseMangerButton)
    LinearLayout ExpenseMangerButton;
    @BindView(R.id.AccStatementButton)
    LinearLayout AccStatementButton;
    @BindView(R.id.ReminderButton)
    LinearLayout ReminderButton;
    @BindView(R.id.LanguageButton)
    LinearLayout LanguageButton;
    @BindView(R.id.CurrencyButton)
    LinearLayout CurrencyButton;
    @BindView(R.id.listLayout)
    RelativeLayout listLayout;
    @BindView(R.id.addFirstUserButton)
    LinearLayout addFirstUserButton;
    @BindView(R.id.emptyLayout)
    LinearLayout emptyLayout;
    @BindView(R.id.relativeLayoutNotification)
    RelativeLayout relativeLayoutNotification;
    @BindView(R.id.textViewNotificationCount)
    TextView textViewNotificationCount;
    @BindView(R.id.imageViewHistory)
    ImageView imageViewHistory;
    @BindView(R.id.howToUseButton)
    LinearLayout howToUseButton;
    private ArrayList<CustomerList> getAddCustomerModelArrayList;
    @BindView(R.id.imageViewMenu)
    ImageView imageViewMenu;
    @BindView(R.id.edittextSerchCustomer)
    EditText edittextSerchCustomer;
    @BindView(R.id.imageViewFilter)
    ImageView imageViewFilter;
    @BindView(R.id.recycleViewAdd_Coustomer)
    RecyclerView recycleViewAddCoustomer;
    private AlertDialog.Builder alertDialogBuilder;
    private static final int CALL_REQUEST = 1337;
    private AddAdapter transcationAdapter;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private ArrayList<CustomerList> viewModelArrayList;
    private CustomerPresenter customerPresenter;
    private LoginSignUpPresenter loginSignUpPresenter;
    Unbinder unbinder;
    private String currency = "";
    String currentVersion, latestVersion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        unbinder = ButterKnife.bind(this);
        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        getCurrentVersion();
        customerPresenter = new CustomerPresenter(context, this);
        ApplicationSingleton.getInstance().setCurrencyChangeCallback(this);
        ApplicationSingleton.getInstance().setProfileUpdateCallback(this);
        ApplicationSingleton.getInstance().setNotificationCountUpdateCallback(this);
        if (null != SharedPreference.getInstance(context).getUser()) {
            System.out.println("Access token - " + SharedPreference.getInstance(context).getUser().getAccessToken() + "\t");
            System.out.println("Fcm token - " + FirebaseInstanceId.getInstance().getToken());

            textViewName.setText(SharedPreference.getInstance(context).getUser().getFullname());
            textViewPhone.setText(SharedPreference.getInstance(context).getUser().getUsername());
            Glide.with(this).load(SharedPreference.getInstance(context).getUser().getAvatar()).into(imageViewUser);

            if (SharedPreference.getInstance(context).getUser().getDisplayCurrency().equals(Constant.symbol)) {
                currency = SharedPreference.getInstance(context).getUser().getCurrencySymbol();
            } else {
                currency = SharedPreference.getInstance(context).getUser().getCurrencyCode();
            }
        }


        if (null != SharedPreference.getInstance(context).getUser()) {
            if (null == SharedPreference.getInstance(context).getUser().getFullname() ||
                    SharedPreference.getInstance(context).getUser().getFullname().equals("")) {
                dilogfullName();
            }

        }

        getAddCustomerModelArrayList = new ArrayList<>();
        setAdapter();

        if (null != SharedPreference.getInstance(context).getCustomerListData() &&
                null != SharedPreference.getInstance(context).getCustomerListData().getData() &&
                SharedPreference.getInstance(context).getCustomerListData().getData().size() != 0) {
            initializeData();
            customerPresenter.callGetCustomerList(false);

        } else {
            customerPresenter.callGetCustomerList(true);
        }


        edittextSerchCustomer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (null != viewModelArrayList && viewModelArrayList.size() != 0) {
                    if (i2 > 0) {
                        filterData(charSequence.toString().trim());

                    } else {
                        getAddCustomerModelArrayList.clear();
                        getAddCustomerModelArrayList = new ArrayList<>();
                        getAddCustomerModelArrayList.addAll(viewModelArrayList);
                        transcationAdapter.notifyAdapter(getAddCustomerModelArrayList, currency);
                        transcationAdapter.notifyDataSetChanged();
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        FirebaseMessaging.getInstance().subscribeToTopic("updates")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
//                        String msg = getString(R.string.msg_subscribed);
                        if (!task.isSuccessful()) {
//                            msg = getString(R.string.msg_subscribe_failed);
                        }
//                        Log.d(TAG, msg);
//                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });

        try {
            if(null != getIntent() && getIntent().hasExtra(Constant.NOTIFY_REDIRECTION_URL)){
                String url = getIntent().getStringExtra(Constant.NOTIFY_REDIRECTION_URL);
               if(null != url && !url.equalsIgnoreCase("")) {
                   Intent i = new Intent(Intent.ACTION_VIEW);
                   i.setData(Uri.parse(url));
                   startActivity(i);
               }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void dilogfullName() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.layout_dialog_name);
        dialog.show();
       dialog.setCancelable(false);
       dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
          InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
           imm.toggleSoftInput( InputMethodManager.SHOW_FORCED, 0 );
        EditText editTextFullName = dialog.findViewById(R.id.editTextName);
        Button buttonSave = dialog.findViewById(R.id.buttonSave);
        ImageView buttonCancel = dialog.findViewById(R.id.buttonCancel);
        buttonCancel.setVisibility(View.GONE);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
                    imm.hideSoftInputFromWindow( editTextFullName.getWindowToken(), 0 );

                dialog.dismiss();

            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextFullName.getText().toString().length() != 0) {
                    callUpdateAccountApi(editTextFullName.getText().toString().trim());

                    InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
                        imm.hideSoftInputFromWindow( editTextFullName.getWindowToken(), 0 );
                    dialog.dismiss();

                }else {
                    showToast("Enter name first");
                }
            }
        });
    }

    private void callUpdateAccountApi(String text) {
        UpdateAccountParameter updateProfilePrameter = new UpdateAccountParameter();
        updateProfilePrameter.setFullname(text );
        new AccountPresenter(this, HomeActivity.this).callUpdateAccountApi( updateProfilePrameter);
    }


    public void replaceFragmentFragment(Fragment fragment, String tag, boolean isAddToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.relativeLayoutFragmentMainContainer, fragment, tag);
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }

        fragmentTransaction.commit();
    }

    public void addFragmentFragment(Fragment fragment, String tag, boolean isAddToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.relativeLayoutFragmentMainContainer, fragment, tag);
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }

        fragmentTransaction.commit();
    }

    private void setAdapter() {

        transcationAdapter = new AddAdapter(context, getAddCustomerModelArrayList, currency, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recycleViewAddCoustomer.setAdapter(transcationAdapter);
        recycleViewAddCoustomer.setLayoutManager(layoutManager);

        recycleViewAddCoustomer.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    addUser.setVisibility(View.GONE);
                } else {
                    addUser.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    private void filterData(String query) {
        query = query.toLowerCase();
        getAddCustomerModelArrayList = new ArrayList<>();
        for (CustomerList customerModel : viewModelArrayList) {
            if (customerModel.getCustomerName().toLowerCase().contains(query)) {
                getAddCustomerModelArrayList.add(customerModel);
            }
        }
        transcationAdapter.notifyAdapter(getAddCustomerModelArrayList, currency);
        transcationAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void openSideMenuDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
        }

    }

    @OnClick({R.id.imageViewMenu, R.id.addUserButton, R.id.imageViewFilter, R.id.addFirstUserButton, R.id.imageViewHistory, R.id.relativeLayoutNotification})
    public void onViewClickedHomePage(View view) {
        switch (view.getId()) {
            case R.id.imageViewMenu:
                openSideMenuDrawer();
                break;
            case R.id.imageViewFilter:
                PopupMenu popup = new PopupMenu(HomeActivity.this, view);
                popup.setOnMenuItemClickListener(HomeActivity.this);
                popup.inflate(R.menu.menu);
                popup.show();
                break;
            case R.id.imageViewHistory:
                addFragmentFragment(new HistoryFragment(), HistoryFragment.TAG, true);
                break;
            case R.id.relativeLayoutNotification:
                replaceFragmentFragment(new NotificationFragment(), NotificationFragment.TAG, true);
//               replaceFragmentFragment( new AccountFragment(), AccountFragment.TAG, true );

                break;
            case R.id.addFirstUserButton:
            case R.id.addUserButton:
                addFragmentFragment(new ContactListFragment(), ContactListFragment.TAG, true);

                break;
        }
    }


    @Override
    public void onProfileClick(int position) {
        profileDialog(position);
    }

    @Override
    public void onUserClick(int position) {
        try {
            addFragmentFragment(TransactionFragment.newInstance(getAddCustomerModelArrayList.get(position)), TransactionFragment.TAG, true);
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    private void profileDialog(int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_profile);
        dialog.show();
        ImageView imageViewCallButton = dialog.findViewById(R.id.imageViewCallButton);
        ImageView imageViewMessageButton = dialog.findViewById(R.id.imageViewMessageButton);
        ImageView imageViewTransactionButton = dialog.findViewById(R.id.imageViewTransactionButton);
        ImageView imageViewUserInfoButton = dialog.findViewById(R.id.imageViewUserInfoButton);
        ImageView imageViewUserProfile = dialog.findViewById(R.id.imageViewUserProfile);
        TextView textViewTotalBalance = dialog.findViewById(R.id.textViewTotalBalance);
        TextView textViewTransactionDate = dialog.findViewById(R.id.textViewTransactionDate);
        TextView textViewLastTransaction = dialog.findViewById(R.id.textViewLastTransaction);
        TextView textViewTransactionType = dialog.findViewById(R.id.textViewTransactionType);
        TextView textViewCustomerName = dialog.findViewById(R.id.textViewCustomerName);


        textViewCustomerName.setText(getAddCustomerModelArrayList.get(position).getCustomerName());
        if (null != getAddCustomerModelArrayList.get(position).getCustomerPhoto() && !getAddCustomerModelArrayList.get(position).getCustomerPhoto().equals("")) {
            Glide.with(this).load(getAddCustomerModelArrayList.get(position).getCustomerPhoto()).into(imageViewUserProfile);

        }

        if (null != getAddCustomerModelArrayList.get(position).getTotalAmount()) {
            textViewTotalBalance.setText(getAddCustomerModelArrayList.get(position).getTotalAmount());
            if (getAddCustomerModelArrayList.get(position).getTotalAmountType().equalsIgnoreCase("credit")) {
                textViewTotalBalance.setTextColor(context.getResources().getColor(R.color.green));

            } else {
                textViewTotalBalance.setTextColor(context.getResources().getColor(R.color.red));
            }
        }
        if (null != getAddCustomerModelArrayList.get(position).getLastTxnAmount()) {
            textViewLastTransaction.setText(getAddCustomerModelArrayList.get(position).getLastTxnAmount());
        }
        if (null != getAddCustomerModelArrayList.get(position).getLastTxnDate()) {
            textViewTransactionDate.setText(getAddCustomerModelArrayList.get(position).getLastTxnDate());
        }
        if (null != getAddCustomerModelArrayList.get(position).getLastTxnType()) {
            textViewTransactionType.setText(getAddCustomerModelArrayList.get(position).getLastTxnType());
            if (getAddCustomerModelArrayList.get(position).getLastTxnType().equalsIgnoreCase("credit")) {
                textViewLastTransaction.setTextColor(context.getResources().getColor(R.color.green));
                textViewTransactionType.setTextColor(context.getResources().getColor(R.color.green));

            } else {
                textViewLastTransaction.setTextColor(context.getResources().getColor(R.color.red));
                textViewTransactionType.setTextColor(context.getResources().getColor(R.color.green));

            }
        }


        imageViewCallButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                dialog.cancel();
                makeCall(getAddCustomerModelArrayList.get(position).getCustomerPhone());
            }
        });


        imageViewMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();

                String number = getAddCustomerModelArrayList.get(position).getCustomerPhone();  // The number on which you want to send SMS
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));

            }
        });


        imageViewTransactionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();

                addFragmentFragment(TransactionFragment.newInstance(getAddCustomerModelArrayList.get(position)), TransactionFragment.TAG, true);

            }
        });
        imageViewUserInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                addFragmentFragment(CustomerProfileFragment.newInstance(getAddCustomerModelArrayList.get(position)), CustomerProfileFragment.TAG, true);

            }
        });
        imageViewUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                addFragmentFragment(CustomerProfileFragment.newInstance(getAddCustomerModelArrayList.get(position)), CustomerProfileFragment.TAG, true);

            }
        });

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void makeCall(String number) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + number));
        if (checkCallPermission()) {
            startActivity(callIntent);
        } else {
            requestCallPermission();
        }
    }

    private boolean checkCallPermission() {
        int result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestCallPermission() {
        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CALL_REQUEST);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == CALL_REQUEST) {
//                makeCall( "8888888888" );
            }
        }

    }


    @OnClick({R.id.ProfileButton, R.id.ExpenseMangerButton, R.id.AccStatementButton, R.id.ReminderButton, R.id.howToUseButton,
            R.id.linearLayoutProfile,R.id.imageViewUser , R.id.LanguageButton, R.id.SecurityButton, R.id.CurrencyButton,
            R.id.HelpButton, R.id.AboutUsButton, R.id.ShareButton, R.id.PrivacyButton, R.id.LogoutButton, R.id.moreAppButton})
    public void onViewClicked(View view) {
        drawer.closeDrawers();
        switch (view.getId()) {
            case R.id.ProfileButton:
                Intent i1 = new Intent(HomeActivity.this, BusinessProfileActivity.class);
                AppUtils.startFromRightToLeft(HomeActivity.this);
                startActivity(i1);
                break;
            case R.id.ExpenseMangerButton:
                break;
            case R.id.AccStatementButton:
                break;
            case R.id.moreAppButton:
                replaceFragmentFragment(new MoreAppsFragment(), MoreAppsFragment.TAG, true);
                break;
            case R.id.linearLayoutProfile:
            case R.id.imageViewUser :
            replaceFragmentFragment(new AccountFragment(), AccountFragment.TAG, true);
                break;
            case R.id.LanguageButton:
                break;
            case R.id.SecurityButton:
                replaceFragmentFragment(new SecurityFragment(), SecurityFragment.TAG, true);
                break;
            case R.id.CurrencyButton:
                replaceFragmentFragment(new UserCurrencyDetailFragment(), UserCurrencyDetailFragment.TAG, true);
                break;
            case R.id.HelpButton:
                replaceFragmentFragment(new HelpsupportFrtragment(), HelpsupportFrtragment.TAG, true);

//                Intent intent = new Intent( this, WebViewActivity.class );
//                intent.putExtra( "url", ServiceGenerator.Help_URL );
//                intent.putExtra( "title", "Help" );
//                startActivity( intent );
//                AppUtils.startFromRightToLeft( HomeActivity.this );
                break;
            case R.id.AboutUsButton:
                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName());
                Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(myAppLinkToMarket);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.ShareButton:
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, "Leakha Jokha");
                share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + getPackageName());
                startActivity(Intent.createChooser(share, "Share link!"));
                break;
            case R.id.howToUseButton:
                Intent intent1 = new Intent(this, WebViewActivity.class);
                intent1.putExtra("url", ServiceGenerator.HOW_TO_USE_URL);
                intent1.putExtra("title", "HOW TO USE");
                startActivity(intent1);
                AppUtils.startFromRightToLeft(HomeActivity.this);
                break;
            case R.id.PrivacyButton:
                Intent intent2 = new Intent(this, WebViewActivity.class);
                intent2.putExtra("url", ServiceGenerator.POLICY_URL);
                intent2.putExtra("title", "Privacy Policy");
                startActivity(intent2);
                AppUtils.startFromRightToLeft(HomeActivity.this);
                break;
            case R.id.LogoutButton:
                logout();
                break;
        }
    }

    private void logout() {
        alertDialogBuilder = new AlertDialog.Builder(context,
                R.style.AlertDialogTheme);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder.setIcon(R.drawable.launcher_logo_lekhajokha);
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.AreYouSureYouWantToLogOut))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.Yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN, false);
                        Intent intent = new Intent(context, LoginThroughActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        AppUtils.startFromRightToLeft(HomeActivity.this);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        getAddCustomerModelArrayList = new ArrayList<>();
//        setAdapter();
//
//        if (null != SharedPreference.getInstance( context ).getCustomerListData() &&
//                null != SharedPreference.getInstance( context ).getCustomerListData().getData() &&
//                SharedPreference.getInstance( context ).getCustomerListData().getData().size() != 0) {
//            initializeData();
//            customerPresenter.callGetCustomerList( false );
//
//        } else {
//            customerPresenter.callGetCustomerList( true );
//        }
//    }


    @Override
    public void onSuccessCustomerUpdate(GetCustomerProfileUpdateResponse getCustomerProfileUpdateResponse) {
        // not in use
    }

    @Override
    public void onSuccessAddCustomer(GetCustomerAddedResponse getCustomerAddedResponse) {
        // not in use
    }

    @Override
    public void onSuccessCustomerProfileUpdate(UpdateCustomerProfileResponse updateCustomerProfileResponse) {
        // not in use
    }

    @Override
    public void onSuccessDeleteCustomer(DeleteCustomerResponse deleteCustomerResponse) {
        // not in use
    }

    @Override
    public void onSuccessGetCustomerList(GetCustomerListResponse getCustomerList) {

        SharedPreference.getInstance(context).putObject(Constant.CUSTOMER_LIST, getCustomerList);

//        customer.resetDatabase();
//        if (null != getCustomerList.getUpdateCustomerProfileData()) {
//            for (int i = 0; i < getCustomerList.getUpdateCustomerProfileData().size(); i++) {
//                CustomerModel customerModel = new CustomerModel();
//                customerModel.setName(getCustomerList.getUpdateCustomerProfileData().get(i).getCustomerName());
//                customerModel.setPhone(getCustomerList.getUpdateCustomerProfileData().get(i).getCustomerPhone());
//                customerModel.setEmail(getCustomerList.getUpdateCustomerProfileData().get(i).getCustomerEmail());
//                customerModel.setCust_id(Integer.parseInt(getCustomerList.getUpdateCustomerProfileData().get(i).getCustomerId()));
//                customerModel.setImage(getCustomerList.getUpdateCustomerProfileData().get(i).getCustomerPhoto());
//                customerModel.setTxn_date(getCustomerList.getUpdateCustomerProfileData().get(i).getLastTxnDate());
//                if (getCustomerList.getUpdateCustomerProfileData().get(i).getTxnEmailStatus().equalsIgnoreCase("0")) {
//                    customerModel.setTxn_email_status(false);
//                } else {
//                    customerModel.setTxn_email_status(true);
//                }
//
//                if (getCustomerList.getUpdateCustomerProfileData().get(i).getTxnSmsStatus().equalsIgnoreCase("0")) {
//                    customerModel.setTxn_sms_status(false);
//                } else {
//                    customerModel.setTxn_sms_status(true);
//                }
//
//
//                if (customer.addCustomer(customerModel)) {
////                    Toast.makeText( context, "Customer added Successfully", Toast.LENGTH_SHORT ).show();
//
//                } else {
////                    Toast.makeText( context, "Something went wrong", Toast.LENGTH_SHORT ).show();
//                }
//            }

        initializeData();


    }

    private void initializeData() {
        if (null != listLayout) {
            if (null != SharedPreference.getInstance(context).getCustomerListData()) {
                getAddCustomerModelArrayList = SharedPreference.getInstance(context).getCustomerListData().getData();
                viewModelArrayList = new ArrayList<>();

                if (null != getAddCustomerModelArrayList && getAddCustomerModelArrayList.size() > 0) {
                    viewModelArrayList.addAll(getAddCustomerModelArrayList);
                    emptyLayout.setVisibility(View.GONE);
                    listLayout.setVisibility(View.VISIBLE);
                } else {
                    emptyLayout.setVisibility(View.VISIBLE);
                    listLayout.setVisibility(View.GONE);
                }

                transcationAdapter.notifyAdapter(getAddCustomerModelArrayList, currency);
                transcationAdapter.notifyDataSetChanged();

            }
        }



    }


    @Override
    public void onSuccessUpdateAccount(UpdateAccountResponse accountResponse) {
        SharedPreference.getInstance(this).putUser( Constant.USER,accountResponse.getData());
        ApplicationSingleton.getInstance().getProfileUpdateCallback().onProfileUpdate();
    }

    @Override
    public void onSuccessAddTimeZoneList(TimezoneResponse timezoneResponse) {

    }

    @Override
    public void onSuccessAddCountryList(GetCountryResponse getCountryResponse) {

    }

    @Override
    public void onSuccessUpdateImage(UpdateAccountImageResponse accountImageResponse) {
         // not in use
    }

    @Override
    public void onTokenChangeError(String errorMessage) {

        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN, false);
        Intent intent = new Intent(context, LoginThroughActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        AppUtils.startFromRightToLeft(HomeActivity.this);

    }

    @Override
    public void onError(String errorMessage) {
        if (errorMessage.equalsIgnoreCase("Record Not Found")) {
            SharedPreference.getInstance(context).putObject(Constant.CUSTOMER_LIST, null);
            emptyLayout.setVisibility(View.VISIBLE);
            listLayout.setVisibility(View.GONE);
        } else {
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuLatest:
                if (null != viewModelArrayList && viewModelArrayList.size() != 0) {
                    getAddCustomerModelArrayList = new ArrayList<>();
                    getAddCustomerModelArrayList.addAll(viewModelArrayList);
                    transcationAdapter.notifyAdapter(getAddCustomerModelArrayList, currency);
                    transcationAdapter.notifyDataSetChanged();
                }
                return true;
            case R.id.menuName:
                if (null != viewModelArrayList && viewModelArrayList.size() != 0) {
                    Collections.sort(getAddCustomerModelArrayList, new SortByName());
                    transcationAdapter.notifyDataSetChanged();
                }
                return true;
            case R.id.menuHavetogive:
                if (null != viewModelArrayList && viewModelArrayList.size() != 0) {
                    sortArrayListByGiven();
                }
                return true;
            case R.id.menuHavetotake:
                if (null != viewModelArrayList && viewModelArrayList.size() != 0) {
                    sortArrayListByReceived();
                }
                return true;
            default:
                return false;
        }
    }

    private void getCurrentVersion() {
        PackageManager pm = this.getPackageManager();
        PackageInfo pInfo = null;

        try {
            pInfo = pm.getPackageInfo(this.getPackageName(), 0);

        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }
        currentVersion = pInfo.versionName;

        new GetLatestVersion().execute();

    }

    private class GetLatestVersion extends AsyncTask<String, String, JSONObject> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                Document doc = Jsoup.connect("http://play.google.com/store/apps/details?id=" + getPackageName()).get();
                latestVersion = doc.getElementsByClass("htlgb").get(6).text();

            } catch (Exception e) {
                e.printStackTrace();

            }

            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if (latestVersion != null) {
                if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                    if (!isFinishing()) { //This would help to prevent Error : BinderProxy@45d459c0 is not valid; is your activity running? error
                        showUpdateDialog();
                    }
                }
            } else
                super.onPostExecute(jsonObject);
        }
    }


    private void showUpdateDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_update_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        dialog.show();


        ImageView imageViewCancelButton = (ImageView) dialog.findViewById(R.id.imageViewCancelButton);

        Button buttonSkip = (Button) dialog.findViewById(R.id.ButtonSkip);
        Button buttonUpdate = (Button) dialog.findViewById(R.id.ButtonUpdate);

        imageViewCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateFunction();
            }
        });
    }

    private void updateFunction() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent goToMarket = new Intent(ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }


    @Override
    public void onCurrencyChanged() {
        if (SharedPreference.getInstance(context).getUser().getDisplayCurrency().equals(Constant.symbol)) {
            currency = SharedPreference.getInstance(context).getUser().getCurrencySymbol();
        } else {
            currency = SharedPreference.getInstance(context).getUser().getCurrencyCode();
        }
        if (null != recycleViewAddCoustomer) {
            initializeData();
        }
    }

    private void sortArrayListByGiven() {
        Collections.sort(getAddCustomerModelArrayList, new Comparator<CustomerList>() {
            @Override
            public int compare(CustomerList o1, CustomerList o2) {
                return o1.getLastTxnType().compareToIgnoreCase(o2.getLastTxnType());
            }
        });
        transcationAdapter.notifyDataSetChanged();

    }

    private void sortArrayListByReceived() {
        ArrayList<CustomerList> givenList = new ArrayList<>();
        ArrayList<CustomerList> receivedList = new ArrayList<>();
        ArrayList<CustomerList> otherList = new ArrayList<>();
        for (int i = 0; i < viewModelArrayList.size(); i++) {
            if (viewModelArrayList.get(i).getLastTxnType().equalsIgnoreCase("debit")) {
                givenList.add(viewModelArrayList.get(i));
            } else if (viewModelArrayList.get(i).getLastTxnType().equalsIgnoreCase("credit")) {
                receivedList.add(viewModelArrayList.get(i));
            } else {
                otherList.add(viewModelArrayList.get(i));
            }
        }

        getAddCustomerModelArrayList = new ArrayList<>();
        getAddCustomerModelArrayList.addAll(givenList);
        getAddCustomerModelArrayList.addAll(receivedList);
        getAddCustomerModelArrayList.addAll(otherList);
        transcationAdapter.notifyAdapter(getAddCustomerModelArrayList, currency);
        transcationAdapter.notifyDataSetChanged();

    }

    @Override
    public void onProfileUpdate() {
        if (null != SharedPreference.getInstance(context).getUser()) {
            System.out.println("Access token - " + SharedPreference.getInstance(context).getUser().getAccessToken() + "\t");
            System.out.println("Fcm token - " + FirebaseInstanceId.getInstance().getToken());

            textViewName.setText(SharedPreference.getInstance(context).getUser().getFullname());
            textViewPhone.setText(SharedPreference.getInstance(context).getUser().getUsername());
            Glide.with(this).load(SharedPreference.getInstance(context).getUser().getAvatar()).into(imageViewUser);
            if (SharedPreference.getInstance(context).getUser().getDisplayCurrency().equals(Constant.symbol)) {
                currency = SharedPreference.getInstance(context).getUser().getCurrencySymbol();
            } else {
                currency = SharedPreference.getInstance(context).getUser().getCurrencyCode();
            }
        }
    }

    @Override
    public void onNotificationCountUpdate() {
        updateNotificationCount();
    }


    class SortByName implements Comparator<CustomerList> {

        public int compare(CustomerList a, CustomerList b) {
            return a.getCustomerName().compareToIgnoreCase(b.getCustomerName());
        }
    }

    @Override
    protected void onResume() {
        updateNotificationCount();
        super.onResume();

    }

    private void updateNotificationCount() {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.cancelAll();

        if (null != textViewNotificationCount) {
            int count = DBHelperSingleton.getInstance().mDBDbHelper.getUnreadNotificationCount();
            if (0 != count) {
                textViewNotificationCount.setVisibility(View.VISIBLE);
                if (count > 99) {
                    textViewNotificationCount.setText("99+");
                } else {
                    textViewNotificationCount.setText(count + "");
                }
            } else {
                textViewNotificationCount.setVisibility(View.GONE);
            }
        }
    }
}
