package com.lekhajokha.udharkhata.home.transaction.detail_transaction.model;

import com.lekhajokha.udharkhata.base.BaseInterface;
import com.lekhajokha.udharkhata.home.transaction.add_transaction.TransactionAddResponse;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionListResponse;

public class DeleteTransactionCallBackListener {

    public interface DeleteTransactionView extends BaseInterface {
        void onSuccessDeleteTransaction(DeleteTransactionResponse deleteTransactionResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface DeleteTransactionManagerCallback {
        void onSuccessDeleteTransaction(DeleteTransactionResponse deleteTransactionResponse);
        void onTokenChangeError(String errorMessage);
        void onError(String errorMessage);
    }

}
