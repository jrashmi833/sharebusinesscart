
package com.lekhajokha.udharkhata.home.Customer.customer_profile.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class GetCustomerProfileUpdateResponse {

    @Expose
    private Long code;
    @Expose
    private CustomerProfileUpdateData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public CustomerProfileUpdateData getData() {
        return data;
    }

    public void setData(CustomerProfileUpdateData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
