
package com.lekhajokha.udharkhata.home.transaction.detail_transaction.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class DeleteTransactionResponse {

    @Expose
    private Long code;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
