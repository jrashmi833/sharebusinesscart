
package com.lekhajokha.udharkhata.home.Customer.customer_profile.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class UpdateCustomerParameter {

    @Expose
    private String address;
    @SerializedName("customer_email")
    private String customerEmail;
    @SerializedName("customer_id")
    private String customerId;
    @SerializedName("customer_name")
    private String customerName;
    @SerializedName("customer_phone")
    private String customerPhone;
    @SerializedName("txn_email_status")
    private String txnEmailStatus;
    @SerializedName("txn_sms_status")
    private String txnSmsStatus;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getTxnEmailStatus() {
        return txnEmailStatus;
    }

    public void setTxnEmailStatus(String txnEmailStatus) {
        this.txnEmailStatus = txnEmailStatus;
    }

    public String getTxnSmsStatus() {
        return txnSmsStatus;
    }

    public void setTxnSmsStatus(String txnSmsStatus) {
        this.txnSmsStatus = txnSmsStatus;
    }

}
