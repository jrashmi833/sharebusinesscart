package com.lekhajokha.udharkhata.home.Customer;

import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.DeleteCustomerResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerProfileUpdateResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerListResponse;
import com.lekhajokha.udharkhata.contact_list.add_customer.model.GetCustomerAddedResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.UpdateCustomerProfileResponse;

public interface CustomerManagerCallBack {
    void onSuccessCustomerUpdate(GetCustomerProfileUpdateResponse getCustomerProfileUpdateResponse);
    void onSuccessCustomerProfileUpdate(UpdateCustomerProfileResponse updateCustomerProfileResponse);
    void onSuccessAddCustomer(GetCustomerAddedResponse getCustomerAddedResponse);
    void onSuccessDeleteCustomer(DeleteCustomerResponse deleteCustomerResponse);
    void onSuccessGetCustomerList(GetCustomerListResponse getCustomerList);
    void onTokenChangeError(String errorMessage);
    void onError(String errorMessage);
}
