package com.lekhajokha.udharkhata.home.transaction.edit_transaction;


import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.home.transaction.TransactionCallBackListener;
import com.lekhajokha.udharkhata.home.transaction.add_transaction.TransactionAddResponse;
import com.lekhajokha.udharkhata.home.transaction.detail_transaction.DetailFragment;
import com.lekhajokha.udharkhata.home.transaction.edit_transaction.model.EditTransactionData;
import com.lekhajokha.udharkhata.home.transaction.edit_transaction.model.EditTransactionResponse;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionArrayList;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionListResponse;
import com.lekhajokha.udharkhata.login_screen.LoginThroughActivity;
import com.lekhajokha.udharkhata.sidemenu.security.PasswordActivity;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import static android.Manifest.permission.CAMERA;
import static android.content.Context.INPUT_METHOD_SERVICE;
import static android.os.Build.VERSION_CODES.M;

/**
 * Created by Ritu Patidar on 2/18/2020.
 */
public class EditTransactionFragment extends Fragment implements EditTransactionCallBackListener.EditTransactionView {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.backButton)
    ImageView backButton;
    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.textViewAmount)
    TextView textViewAmount;
    @BindView(R.id.cardViewOnline)
    CardView cardViewOnline;
    @BindView(R.id.cardViewCash)
    CardView cardViewCash;
    @BindView(R.id.cardViewOther)
    CardView cardViewOther;
    @BindView(R.id.textViewAddedDate)
    TextView textViewAddedDate;
    @BindView(R.id.textViewUpdatedDate)
    TextView textViewUpdatedDate;
    @BindView(R.id.textViewReturnDate)
    TextView textViewReturnDate;
    @BindView(R.id.tvLabelReceivedGiven)
    TextView tvLabelReceivedGiven;
    @BindView(R.id.textViewReceivedGivenDate)
    TextView textViewReceivedGivenDate;
    @BindView(R.id.textViewRemainderDate)
    TextView textViewRemainderDate;
    @BindView(R.id.textViewAddDescription)
    TextView textViewAddDescription;
    @BindView(R.id.textViewAddAttachment)
    TextView textViewAddAttachment;
    @BindView(R.id.imgAttachment)
    ImageView imgAttachment;
    @BindView(R.id.imgEditAttachment)
    ImageView imgEditAttachment;
    @BindView(R.id.relativeLayoutAttachment)
    RelativeLayout relativeLayoutAttachment;
    @BindView(R.id.buttonSaveChanges)
    LinearLayout buttonSaveChanges;
    @BindView(R.id.tvLabelReturnCollectDate)
    TextView tvLabelReturnCollectDate;
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 1111;
    private static final int GALLERY_PERMISSION_REQUEST_CODE = 1112;
    private static int SELECT_FROM_GALLERY = 2;
    private static int CAMERA_PIC_REQUEST = 0;
    private static final int LOCK_REQUEST_CODE = 4;
    private Context context;
    private View view;
    private TransactionArrayList transactionArrayList;
    private Unbinder unbinder;
    private String name;
    private String currency;
    private File documentAttachedFile;
    private String paymentMode ;
    private Uri mImageCaptureUri;
    private EditTransactionPresenter editTransactionPresenter;
    public EditTransactionFragment() {
    }

    public static EditTransactionFragment newInstance(TransactionArrayList transactionArrayList, String name) {
        EditTransactionFragment fragment = new EditTransactionFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, transactionArrayList);
        args.putString(ARG_PARAM2, name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            transactionArrayList = (TransactionArrayList) getArguments().getSerializable(ARG_PARAM1);
            name = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_edit_transaction, container, false);
        unbinder = ButterKnife.bind(this, view);
        editTransactionPresenter = new EditTransactionPresenter( context, this );
        if (SharedPreference.getInstance(context).getUser().getDisplayCurrency().equals(Constant.symbol)) {
            currency = SharedPreference.getInstance(context).getUser().getCurrencySymbol();

        } else {
            currency = SharedPreference.getInstance(context).getUser().getCurrencyCode();
        }
        if(null != transactionArrayList ){
            initializeData();
        }
        return view;
    }

    private void initializeData() {
        if (transactionArrayList.getTxnType().equalsIgnoreCase("credit")) {
            textViewAmount.setText(currency+" "+transactionArrayList.getTxnAmount());
            textViewAmount.setTextColor(context.getResources().getColor(R.color.green));
            textViewName.setText("Received From " + name);
            tvLabelReceivedGiven.setText("Received Date");
            tvLabelReturnCollectDate.setText("Return");

       } else {
            textViewAmount.setText(currency+" "+transactionArrayList.getTxnAmount());
            textViewAmount.setTextColor(context.getResources().getColor(R.color.red));
            textViewName.setText("Given To " + name);
            tvLabelReceivedGiven.setText("Given Date");
            tvLabelReturnCollectDate.setText("Collect");
        }


        if (null != transactionArrayList.getDescription() && !transactionArrayList.getDescription().equalsIgnoreCase("")) {
            textViewAddDescription.setText(transactionArrayList.getDescription());
            textViewAddDescription.setGravity(View.TEXT_ALIGNMENT_TEXT_START);
        }

        if (null != transactionArrayList.getReminderDate() && !transactionArrayList.getReminderDate().equals("")) {
            textViewRemainderDate.setText(transactionArrayList.getReminderDate() + "");
        }


        if (null != transactionArrayList.getReturnDate() && !transactionArrayList.getReturnDate().equals("")) {
            textViewReturnDate.setText(transactionArrayList.getReturnDate() + "");
        }

        if (null != transactionArrayList.getPaymentType() && !transactionArrayList.getPaymentType().equals("")) {
            switch (transactionArrayList.getPaymentType().toLowerCase()){
                case  "online" :
                    paymentMode = Constant.online;
                   cardViewCash.setCardBackgroundColor(getResources().getColor(R.color.white));
                   cardViewOther.setCardBackgroundColor(getResources().getColor(R.color.white));
                   cardViewOnline.setCardBackgroundColor(getResources().getColor(R.color.card_selected_color));
                   break;
                case "cash" :
                    paymentMode = Constant.cash;
                    cardViewCash.setCardBackgroundColor(getResources().getColor(R.color.card_selected_color));
                    cardViewOther.setCardBackgroundColor(getResources().getColor(R.color.white));
                    cardViewOnline.setCardBackgroundColor(getResources().getColor(R.color.white));
                    break;
                case "other" :
                    paymentMode = Constant.other;
                    cardViewCash.setCardBackgroundColor(getResources().getColor(R.color.white));
                    cardViewOther.setCardBackgroundColor(getResources().getColor(R.color.card_selected_color));
                    cardViewOnline.setCardBackgroundColor(getResources().getColor(R.color.white));
            }
        }

        textViewReceivedGivenDate.setText(transactionArrayList.getTxnDate());
        textViewAddedDate.setText(transactionArrayList.getCreatedAt());


        if(null != transactionArrayList.getBillPhoto() && !transactionArrayList.getBillPhoto().equalsIgnoreCase("")) {
            relativeLayoutAttachment.setVisibility(View.VISIBLE);
            textViewAddAttachment.setVisibility(View.GONE);
            Glide.with(this).load(transactionArrayList.getBillPhoto()).into(imgAttachment);
        }else {
            relativeLayoutAttachment.setVisibility(View.GONE);
            textViewAddAttachment.setVisibility(View.VISIBLE);
        }

    }

    private void getCalendar(TextView textView) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        c.set(year, (monthOfYear), dayOfMonth);
                        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
                        String strDate = format.format(c.getTime());
                        textView.setText(strDate);
                    }
                }, year, month, day);
        datePickerDialog.show();
    }


    private void selectImage() {
        final CharSequence[] options = {"From Camera", "From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Please choose an Image");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("From Camera")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkCameraPermission())
                            cameraIntent();
                        else
                            requestPermission();
                    } else
                        cameraIntent();
                } else if (options[item].equals("From Gallery")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkGalleryPermission())
                            galleryIntent();
                        else
                            requestGalleryPermission();
                    } else
                        galleryIntent();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.create().show();
    }

    private void requestPermission() {
        requestPermissions(new String[]{CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
    }

    private void requestGalleryPermission() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PERMISSION_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        int result1 = ContextCompat.checkSelfPermission(context, CAMERA);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkGalleryPermission() {
        int result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }

    private void galleryIntent() {
        Intent intent = new Intent().setType("image/*").setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FROM_GALLERY);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        startActivityForResult(intent, CAMERA_PIC_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                }
                break;
            case GALLERY_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    galleryIntent();
                }
                break;

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK && null != data) {
            Bitmap bmp = (Bitmap) data.getExtras().get("data");
            if (null != bmp) {
                imgAttachment.setImageBitmap(bmp);
                documentAttachedFile = ((HomeActivity) context).getUserImageFile(bmp);
                textViewAddAttachment.setVisibility(View.GONE);
                relativeLayoutAttachment.setVisibility(View.VISIBLE);
            }
        } else if (requestCode == SELECT_FROM_GALLERY && resultCode == Activity.RESULT_OK && null != data) {
            Uri galleryURI = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), galleryURI);
                imgAttachment.setImageBitmap(bitmap);
                textViewAddAttachment.setVisibility(View.GONE);
                relativeLayoutAttachment.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (null != bitmap) {
                documentAttachedFile = ((HomeActivity) context).getUserImageFile(bitmap);
            }

        } else if (requestCode == LOCK_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            callEditTransaction();
        }

    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @OnClick({R.id.backButton, R.id.cardViewOnline, R.id.cardViewCash, R.id.cardViewOther, R.id.textViewReturnDate,R.id.textViewRemainderDate,
            R.id.textViewAddDescription, R.id.textViewAddAttachment, R.id.imgEditAttachment, R.id.buttonSaveChanges})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                ((HomeActivity)context).onBackPressed();
                break;
            case R.id.cardViewOnline:
                paymentMode = Constant.online;
                cardViewCash.setCardBackgroundColor(getResources().getColor(R.color.white));
                cardViewOther.setCardBackgroundColor(getResources().getColor(R.color.white));
                cardViewOnline.setCardBackgroundColor(getResources().getColor(R.color.card_selected_color));
                break;
            case R.id.cardViewCash:
                paymentMode = Constant.cash;
                cardViewCash.setCardBackgroundColor(getResources().getColor(R.color.card_selected_color));
                cardViewOther.setCardBackgroundColor(getResources().getColor(R.color.white));
                cardViewOnline.setCardBackgroundColor(getResources().getColor(R.color.white));
                break;
            case R.id.cardViewOther:
                paymentMode = Constant.other;
                cardViewCash.setCardBackgroundColor(getResources().getColor(R.color.white));
                cardViewOther.setCardBackgroundColor(getResources().getColor(R.color.card_selected_color));
                cardViewOnline.setCardBackgroundColor(getResources().getColor(R.color.white));
                break;
            case R.id.textViewRemainderDate :
                getCalendar(textViewRemainderDate);
                break;
            case R.id.textViewReturnDate:
                getCalendar(textViewReturnDate);
                break;
            case R.id.textViewAddDescription:
                dialogDEscription(textViewAddDescription.getText().toString());
                break;
            case R.id.textViewAddAttachment:
                selectImage();
                break;
            case R.id.imgEditAttachment:
                selectImage();
                break;
            case R.id.buttonSaveChanges :
                if (SharedPreference.getInstance(context).getBoolean(Constant.TransactionLock, false)) {
                    Intent i = new Intent(context, PasswordActivity.class);
                    startActivityForResult(i, LOCK_REQUEST_CODE);
                } else {
                    callEditTransaction();
                }

                break;
        }
    }

    private void dialogDEscription(String text) {

        final Dialog dialog = new Dialog( context );
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        dialog.getWindow().setBackgroundDrawableResource( android.R.color.transparent );
        dialog.setContentView( R.layout.layout_dialog_transaction_description );
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        EditText editTextAddress = dialog.findViewById( R.id.editTextDesc );
        Button buttonSave = dialog.findViewById( R.id.buttonSave );
        ImageView buttonCancel = dialog.findViewById( R.id.buttonCancel );
        editTextAddress.setText(text);
        buttonCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        } );

        buttonSave.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextAddress.getText().toString().length() != 0) {
                    try {
                        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
                        assert inputMethodManager != null;
                        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                  textViewAddDescription.setText(editTextAddress.getText().toString().trim());
                    textViewAddDescription.setGravity(View.TEXT_ALIGNMENT_TEXT_START);
                    dialog.cancel();
                }else {
                    Toast.makeText(context, "Enter description",Toast.LENGTH_SHORT).show();
                }

            }
        } );

    }


    private void callEditTransaction() {
        Map<String, RequestBody> params = new HashMap<>();
        params.put("customer_id", Constant.getRequestBody(transactionArrayList.getCustomerId()));
        params.put("txn_id", Constant.getRequestBody(transactionArrayList.getTxnId()));
        params.put("payment_type",Constant.getRequestBody(paymentMode));

        if (textViewAddDescription.getText().toString().trim().equals(context.getResources().getString(R.string.tap_to_add_description))) {
            params.put("description", Constant.getRequestBody(""));
        } else {
            params.put("description", Constant.getRequestBody(textViewAddDescription.getText().toString().trim()));
        }

        if (textViewRemainderDate.getText().toString().equals(context.getResources().getString(R.string.tap_to_add_remainder))) {
            params.put("reminder_date", Constant.getRequestBody(""));
        } else {
            params.put("reminder_date", Constant.getRequestBody(((HomeActivity) context).convertIntoServerDateTimeFormat(textViewRemainderDate.getText().toString().trim())));
        }

        if (textViewReturnDate.getText().toString().equals(context.getResources().getString(R.string.tap_to_add))) {
            params.put("return_date", Constant.getRequestBody(""));
        } else {
            params.put("return_date", Constant.getRequestBody(((HomeActivity) context).convertIntoServerDateTimeFormat(textViewReturnDate.getText().toString().trim())));

        }



        MultipartBody.Part partbody1 = null;
        if (null != documentAttachedFile) {
            RequestBody requestBodyy = RequestBody.create(MediaType.parse("image"), documentAttachedFile);
            partbody1 = MultipartBody.Part.createFormData("bill_photo", documentAttachedFile.getName(), requestBodyy);

        }

        editTransactionPresenter.callEditTransactionAddApi(params, partbody1);


    }





    @Override
    public void onSuccessEditTransactionList(EditTransactionResponse editTransactionResponse) {
        ((HomeActivity)context).showToast(editTransactionResponse.getMessage());
              FragmentManager fm = getFragmentManager();
                for (int entry = 0; entry < fm.getBackStackEntryCount(); entry++) {
                    if (DetailFragment.TAG.equals(fm.getBackStackEntryAt(entry).getName())) {
                        getFragmentManager().popBackStack();
                        break;
                    }
                }

        ((HomeActivity)context).onBackPressed();

    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN, false);
        Intent intent = new Intent(context, LoginThroughActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        AppUtils.startFromRightToLeft(context);
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShowLoader() {
        ((HomeActivity) context).onShowLoader();
    }

    @Override
    public void onHideLoader() {
        ((HomeActivity) context).onHideLoader();
    }

}
