
package com.lekhajokha.udharkhata.home.transaction.edit_transaction.model;


import com.google.gson.annotations.Expose;


public class EditTransactionResponse {

    @Expose
    private Long code;
    @Expose
    private EditTransactionData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public EditTransactionData getData() {
        return data;
    }

    public void setData(EditTransactionData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
