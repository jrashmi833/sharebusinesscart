package com.lekhajokha.udharkhata.home.transaction.Create_payment;


import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.home.transaction.TransactionCallBackListener;
import com.lekhajokha.udharkhata.home.transaction.TransactionPresenter;
import com.lekhajokha.udharkhata.home.transaction.add_transaction.TransactionAddResponse;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionListResponse;
import com.lekhajokha.udharkhata.login_screen.LoginThroughActivity;
import com.lekhajokha.udharkhata.sidemenu.security.PasswordActivity;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import static android.Manifest.permission.CAMERA;
import static android.content.Context.INPUT_METHOD_SERVICE;
import static android.os.Build.VERSION_CODES.M;


public class CreatePaymentFragment extends Fragment implements TransactionCallBackListener.TransactionView {
    public static final String TAG = CreatePaymentFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    private static final String ARG_PARAM6 = "param6";
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 1111;
    private static final int GALLERY_PERMISSION_REQUEST_CODE = 1112;
    private static int SELECT_FROM_GALLERY = 2;
    private static int CAMERA_PIC_REQUEST = 0;
    private static final int LOCK_REQUEST_CODE = 4;
    @BindView(R.id.backButton)
    ImageView backButton;
    @BindView(R.id.textViewname)
    TextView textViewname;
    @BindView(R.id.textViewRupee)
    TextView textViewRupee;
    @BindView(R.id.EditTextAmount)
    EditText EditTextAmount;
    @BindView(R.id.layoutAmount)
    LinearLayout layoutAmount;
    @BindView(R.id.textViewDate)
    TextView textViewDate;
    @BindView(R.id.layoutCalendar)
    LinearLayout layoutCalendar;
    @BindView(R.id.editTextDescription)
    EditText editTextDescription;
    @BindView(R.id.layoutDescription)
    LinearLayout layoutDescription;
    @BindView(R.id.imgCamera)
    ImageView imgCamera;
    @BindView(R.id.img_document)
    ImageView imgDocument;
    @BindView(R.id.switchSms)
    Switch switchSms;
    @BindView(R.id.switchEmail)
    Switch switchEmail;
    @BindView(R.id.buttonSaveAndShare)
    LinearLayout buttonSaveAndShare;
    @BindView(R.id.buttonSave)
    LinearLayout buttonSave;
    Unbinder unbinder;
    @BindView(R.id.textViewReturnDate)
    TextView textViewReturnDate;
    @BindView(R.id.layoutCalendarReturnDate)
    LinearLayout layoutCalendarReturnDate;
    @BindView(R.id.textViewRemainderDate)
    TextView textViewRemainderDate;
    @BindView(R.id.layoutCalendarReminder)
    LinearLayout layoutCalendarReminder;
    @BindView(R.id.linearLayoutExtraDebitDate)
    LinearLayout linearLayoutExtraDebitDate;
    @BindView(R.id.layoutSendSMS)
    LinearLayout layoutSendSMS;
    @BindView(R.id.layoutButton)
    LinearLayout layoutButton;
    @BindView(R.id.cardViewOnline)
    CardView cardViewOnline;
    @BindView(R.id.cardViewCash)
    CardView cardViewCash;
    @BindView(R.id.cardViewOther)
    CardView cardViewOther;
    private Uri mImageCaptureUri;
    private TransactionPresenter transactionPresenter;
    private String Name, Type, id;
    private String isTxnSMS, isTxnEmail;
    private View view;
    private Context context;
    private float settleAmount;
    private File documentAttachedFile;
    private String paymentMode  ;
    public CreatePaymentFragment() {
    }


    public static CreatePaymentFragment newInstance(String name, String type, String id, String isTxnSMS, String isTxnEmail, float settleAmount) {
        CreatePaymentFragment fragment = new CreatePaymentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, name);
        args.putString(ARG_PARAM2, type);
        args.putString(ARG_PARAM3, id);
        args.putString(ARG_PARAM4, isTxnSMS);
        args.putString(ARG_PARAM5, isTxnEmail);
        args.putFloat(ARG_PARAM6, settleAmount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Name = getArguments().getString(ARG_PARAM1);
            Type = getArguments().getString(ARG_PARAM2);
            id = getArguments().getString(ARG_PARAM3);
            isTxnSMS = getArguments().getString(ARG_PARAM4);
            isTxnEmail = getArguments().getString(ARG_PARAM5);
            settleAmount = getArguments().getFloat(ARG_PARAM6);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_create_payment, container, false);
        unbinder = ButterKnife.bind(this, view);

        EditTextAmount.requestFocus();
        EditTextAmount.setFocusable(true);


        if (Type.equalsIgnoreCase("debit")) {
            textViewRupee.setTextColor(context.getResources().getColor(R.color.red));
            EditTextAmount.setTextColor(context.getResources().getColor(R.color.red));
            textViewname.setText("Give Balance to " + Name);
        } else {
            textViewRupee.setTextColor(context.getResources().getColor(R.color.green));
            EditTextAmount.setTextColor(context.getResources().getColor(R.color.green));
            textViewname.setText("Receive Balance from " + Name);
        }

        initialPayment();

        if (settleAmount != 0) {
            EditTextAmount.setText(settleAmount + "");
            editTextDescription.setText("Transactions Settlement");
            layoutDescription.setVisibility(View.VISIBLE);
            linearLayoutExtraDebitDate.setVisibility(View.GONE);
        } else {
            layoutDescription.setVisibility(View.GONE);
            linearLayoutExtraDebitDate.setVisibility(View.VISIBLE);
        }

        if (isTxnEmail.equals("1")) {
            switchEmail.setChecked(true);
        } else {
            switchEmail.setChecked(false);
        }

        if (null != SharedPreference.getInstance(context).getUser().getPhoneCode() &&
                SharedPreference.getInstance(context).getUser().getPhoneCode().equals("+91")) {
            layoutSendSMS.setVisibility(View.VISIBLE);
            if (isTxnSMS.equals("1")) {
                switchSms.setChecked(true);
            } else {
                switchSms.setChecked(false);
            }
        } else {
            layoutSendSMS.setVisibility(View.GONE);
        }


        editTextDescription.setImeOptions(EditorInfo.IME_ACTION_DONE);
        editTextDescription.setRawInputType(InputType.TYPE_CLASS_TEXT);

        if (SharedPreference.getInstance(context).getUser().getDisplayCurrency().equals(Constant.symbol)) {
            textViewRupee.setText(SharedPreference.getInstance(context).getUser().getCurrencySymbol());
        } else {
            textViewRupee.setText(SharedPreference.getInstance(context).getUser().getCurrencyCode());
        }


        transactionPresenter = new TransactionPresenter(context, this);
        Date today = new Date();
        String dateToStr = Constant.DisplayFormat.format(today);
        textViewDate.setText(dateToStr);
        EditTextAmount.setFilters(new InputFilter[]{filter});


        EditTextAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (EditTextAmount.getText().toString().length() >= 11) {
                    EditTextAmount.clearFocus();
                    editTextDescription.requestFocus();
                }

                if (EditTextAmount.getText().toString().length() != 0) {
                    layoutDescription.setVisibility(View.VISIBLE);
                } else {
                    layoutDescription.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        return view;
    }

    private void initialPayment() {
        paymentMode = Constant.online;
        cardViewCash.setCardBackgroundColor(getResources().getColor(R.color.white));
        cardViewOther.setCardBackgroundColor(getResources().getColor(R.color.white));
        cardViewOnline.setCardBackgroundColor(getResources().getColor(R.color.card_selected_color));
    }


    InputFilter filter = new InputFilter() {
        final int maxDigitsBeforeDecimalPoint = 8;
        final int maxDigitsAfterDecimalPoint = 2;

        @Override
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            StringBuilder builder = new StringBuilder(dest);
            builder.replace(dstart, dend, source
                    .subSequence(start, end).toString());
            if (!builder.toString().matches(
                    "(([1-9]{1})([0-9]{0," + (maxDigitsBeforeDecimalPoint - 1) + "})?)?(\\.[0-9]{0," + maxDigitsAfterDecimalPoint + "})?"

            )) {
                if (source.length() == 0)
                    return dest.subSequence(dstart, dend);
                return "";
            }

            return null;

        }
    };


    private Runnable mShowSoftInputRunnable = new Runnable() {
        @Override
        public void run() {
            FragmentActivity activity = getActivity();
            if (activity == null)
                return;

            InputMethodManager input = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            assert input != null;
            input.showSoftInput(EditTextAmount, InputMethodManager.SHOW_IMPLICIT);
        }
    };

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @OnClick({R.id.backButton, R.id.layoutCalendar, R.id.buttonSave, R.id.buttonSaveAndShare, R.id.imgCamera,
            R.id.layoutCalendarReminder, R.id.layoutCalendarReturnDate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                getActivity().onBackPressed();
                break;
            case R.id.layoutCalendar:
                getCalendar(textViewDate);
                break;
            case R.id.layoutCalendarReminder:
                getCalendar(textViewRemainderDate);
                break;
            case R.id.layoutCalendarReturnDate:
                getCalendar(textViewReturnDate);
                break;
            case R.id.buttonSave:
                if (validate()) {
                    if (SharedPreference.getInstance(context).getBoolean(Constant.TransactionLock, false)) {
                        Intent i = new Intent(context, PasswordActivity.class);
                        i.putExtra(Constant.WHERE_FROM, Constant.TransactionLock);
                        startActivityForResult(i, LOCK_REQUEST_CODE);
                    } else {
                        callAddTransaction(false);

                    }
                }
                break;
            case R.id.buttonSaveAndShare:
                if (validate()) {
                    if (SharedPreference.getInstance(context).getBoolean(Constant.TransactionLock, false)) {
                        Intent i = new Intent(context, PasswordActivity.class);
                        startActivityForResult(i, LOCK_REQUEST_CODE);
                    } else {
                        callAddTransaction(false);

                    }

                }
                break;
            case R.id.imgCamera:
                selectImage();
                break;
        }
    }

    private boolean validate() {
        if (EditTextAmount.getText().toString().length() == 0) {
            Toast.makeText(context,"Enter Detail First", Toast.LENGTH_SHORT).show();
            return false ;
        } else if(null == paymentMode || paymentMode.equalsIgnoreCase("")){
            Toast.makeText(context,"Select payment mode", Toast.LENGTH_SHORT).show();
            return false ;
        }
        return true;
    }


    private void callAddTransaction(boolean isShare) {
        Map<String, RequestBody> params = new HashMap<>();
        params.put("customer_id", Constant.getRequestBody(id + ""));
        params.put("txn_type", Constant.getRequestBody(Type));
        params.put("txn_amount", Constant.getRequestBody(EditTextAmount.getText().toString()));
        params.put("description", Constant.getRequestBody(editTextDescription.getText().toString().trim()));
        params.put("send_sms", Constant.getRequestBody(switchSms.isChecked() ? "1" : "0"));
        params.put("send_email", Constant.getRequestBody(switchEmail.isChecked() ? "1" : "0"));
        params.put("txn_date", Constant.getRequestBody(((HomeActivity) context).convertIntoServerDateTimeFormat(textViewDate.getText().toString())));
        params.put("payment_type",Constant.getRequestBody(paymentMode));
//        if (Type.equalsIgnoreCase("debit")) {

        if (textViewRemainderDate.getText().toString().equals(context.getResources().getString(R.string.remainder_date))) {
            params.put("reminder_date", Constant.getRequestBody(""));
        } else {
            params.put("reminder_date", Constant.getRequestBody(((HomeActivity) context).convertIntoServerDateTimeFormat(textViewRemainderDate.getText().toString().trim())));
        }

        if (textViewReturnDate.getText().toString().equals(context.getResources().getString(R.string.return_date))) {
            params.put("return_date", Constant.getRequestBody(""));
        } else {
            params.put("return_date", Constant.getRequestBody(((HomeActivity) context).convertIntoServerDateTimeFormat(textViewReturnDate.getText().toString().trim())));

        }

//        } else {
//            params.put("return_date", Constant.getRequestBody(""));
//            params.put("reminder_date", Constant.getRequestBody(""));
//        }


        MultipartBody.Part partbody1 = null;
        if (null != documentAttachedFile) {
            RequestBody requestBodyy = RequestBody.create(MediaType.parse("image"), documentAttachedFile);
            partbody1 = MultipartBody.Part.createFormData("bill_photo", documentAttachedFile.getName(), requestBodyy);

        }


        transactionPresenter.callTransationAddApi(params, partbody1, isShare);

    }

 /*   private void saveTransaction() {
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        String dateToStr = format.format(today);

        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setCustomer_id(id);
        transactionModel.setCreated_at(dateToStr);
        transactionModel.setDescription(editTextDescription.getText().toString());
        transactionModel.setTxn_amount(EditTextAmount.getText().toString());
        transactionModel.setTxn_date(textViewDate.getText().toString());
        transactionModel.setTxn_type(Type);


        if (transaction.addTransaction(transactionModel)) {
            Toast.makeText(context, "Transaction added Successfully", Toast.LENGTH_SHORT).show();
            TransitionAddParameter transitionAddParameter = new TransitionAddParameter();
            transitionAddParameter.setCustomerId(String.valueOf(id));
            transitionAddParameter.setDescription(editTextDescription.getText().toString());
            transitionAddParameter.setTxnAmount(EditTextAmount.getText().toString());
            transitionAddParameter.setTxnType(Type);

            transactionPresenter.callTransationAddApi(transitionAddParameter, SharedPreference.getInstance(context).getUser().getAccessToken(), context);

            getActivity().onBackPressed();
        } else {
            Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
        }

    }*/

    private void getCalendar(TextView textView) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        c.set(year, (monthOfYear), dayOfMonth);
                        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
                        String strDate = format.format(c.getTime());
                        textView.setText(strDate);
                    }
                }, year, month, day);
        datePickerDialog.show();
    }

    @Override
    public void onResume() {
        EditTextAmount.postDelayed(mShowSoftInputRunnable, 300);
        super.onResume();

    }

    @Override
    public void onSuccessTransactionList(TransactionListResponse transactionListResponse) {
        // not in use
    }

    @Override
    public void onSuccessAddTransactionList(TransactionAddResponse transactionAddResponse, boolean isShare) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        MediaPlayer player = new MediaPlayer();
        player = MediaPlayer.create(context, R.raw.txnring);
        player.start();
        if (isShare) {
            getActivity().onBackPressed();
        } else {
            getActivity().onBackPressed();

        }


    }

    @Override
    public void onSuccessDownloadStatement(ResponseBody responseBody) {
        //not in use
    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN, false);
        Intent intent = new Intent(context, LoginThroughActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        AppUtils.startFromRightToLeft(context);
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShowLoader() {
        ((HomeActivity) context).onShowLoader();
    }

    @Override
    public void onHideLoader() {
        ((HomeActivity) context).onHideLoader();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    private void selectImage() {
        final CharSequence[] options = {"From Camera", "From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Please choose an Image");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("From Camera")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkCameraPermission())
                            cameraIntent();
                        else
                            requestPermission();
                    } else
                        cameraIntent();
                } else if (options[item].equals("From Gallery")) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkGalleryPermission())
                            galleryIntent();
                        else
                            requestGalleryPermission();
                    } else
                        galleryIntent();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.create().show();
    }

    private void requestPermission() {
        requestPermissions(new String[]{CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
    }

    private void requestGalleryPermission() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PERMISSION_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        int result1 = ContextCompat.checkSelfPermission(context, CAMERA);
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkGalleryPermission() {
        int result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }

    private void galleryIntent() {
        Intent intent = new Intent().setType("image/*").setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FROM_GALLERY);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        startActivityForResult(intent, CAMERA_PIC_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                }
                break;
            case GALLERY_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    galleryIntent();
                }
                break;

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK && null != data) {
            Bitmap bmp = (Bitmap) data.getExtras().get("data");
            if (null != bmp) {
                imgDocument.setImageBitmap(bmp);
                documentAttachedFile = ((HomeActivity) context).getUserImageFile(bmp);
            }
        } else if (requestCode == SELECT_FROM_GALLERY && resultCode == Activity.RESULT_OK && null != data) {
            Uri galleryURI = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), galleryURI);
                imgDocument.setImageBitmap(bitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }
            if (null != bitmap) {
                documentAttachedFile = ((HomeActivity) context).getUserImageFile(bitmap);
            }

        } else if (requestCode == LOCK_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            callAddTransaction(false);
        }

    }


    @OnClick({R.id.cardViewOnline, R.id.cardViewCash, R.id.cardViewOther})
    public void onCardViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cardViewOnline:
                paymentMode = Constant.online;
                cardViewCash.setCardBackgroundColor(getResources().getColor(R.color.white));
                cardViewOther.setCardBackgroundColor(getResources().getColor(R.color.white));
                cardViewOnline.setCardBackgroundColor(getResources().getColor(R.color.card_selected_color));
                break;
            case R.id.cardViewCash:
                paymentMode = Constant.cash;
                cardViewCash.setCardBackgroundColor(getResources().getColor(R.color.card_selected_color));
                cardViewOther.setCardBackgroundColor(getResources().getColor(R.color.white));
                cardViewOnline.setCardBackgroundColor(getResources().getColor(R.color.white));
                break;
            case R.id.cardViewOther:
                paymentMode = Constant.other;
                cardViewCash.setCardBackgroundColor(getResources().getColor(R.color.white));
                cardViewOther.setCardBackgroundColor(getResources().getColor(R.color.card_selected_color));
                cardViewOnline.setCardBackgroundColor(getResources().getColor(R.color.white));
                break;
        }
    }
}
