package com.lekhajokha.udharkhata.home.transaction;

import android.content.Context;

import com.lekhajokha.udharkhata.home.transaction.add_transaction.TransactionAddResponse;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionListResponse;
import com.lekhajokha.udharkhata.rest.ServiceGenerator;
import com.lekhajokha.udharkhata.rest.ServiceGeneratorDownloadPdf;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.io.IOException;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class TransactionManager {
    private TransactionCallBackListener.TransactionManagerCallback customerManagerCallBack ;
    private Context context ;

    TransactionManager(Context context ,TransactionCallBackListener.TransactionManagerCallback customerManagerCallBack ) {
        this.context = context;
        this.customerManagerCallBack = customerManagerCallBack;
    }

    void callTransactionList(String customerId) {
        TransactionApi transactionApi = ServiceGenerator.createService(TransactionApi.class);
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Call<TransactionListResponse> imageUploadResponseCall = transactionApi.callTransactionList(token, customerId);
        imageUploadResponseCall.enqueue(new Callback<TransactionListResponse>() {
            @Override
            public void onResponse(Call<TransactionListResponse> call, Response<TransactionListResponse> response) {
                if (null!=response.body()){
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                    customerManagerCallBack.onSuccessTransactionList(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        customerManagerCallBack.onTokenChangeError(response.body().getMessage());
                    } else {
                        customerManagerCallBack.onError(response.body().getMessage());
                    }
                }
                } else {
                customerManagerCallBack.onError("opps something went wrong");
            }


            }

            @Override
            public void onFailure(Call<TransactionListResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    customerManagerCallBack.onError("Network down or no internet connection");
                } else {
                    customerManagerCallBack.onError("opps something went wrong");
                }
            }
        });
    }

    public void callTransationAddApi(Map<String, RequestBody> params, MultipartBody.Part partbody1, boolean isShare) {
        TransactionApi transactionAddApi = ServiceGenerator.createService( TransactionApi.class );
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Call<TransactionAddResponse> transactionAddResponseCall = transactionAddApi.callTransationAddApi( token, params, partbody1 );
        transactionAddResponseCall.enqueue( new Callback<TransactionAddResponse>() {
            @Override
            public void onResponse(Call<TransactionAddResponse> call, Response<TransactionAddResponse> response) {

                if (null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        customerManagerCallBack.onSuccessAddTransactionList(response.body(), isShare);
                    } else {
                        if (response.body().getCode() == 400) {
                            customerManagerCallBack.onTokenChangeError(response.body().getMessage());
                        } else {
                            customerManagerCallBack.onError(response.body().getMessage());
                        }
                    }
                } else {
                    customerManagerCallBack.onError("opps something went wrong");
                }
            }

            @Override
            public void onFailure(Call<TransactionAddResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    customerManagerCallBack.onError( "Network down or no internet connection" );
                } else {
                    customerManagerCallBack.onError( "opps something went wrong" );
                }
            }
        } );
    }

    void callDownloadFileWithCustomerID(String customerId){
        TransactionApi downloadPdfApi = ServiceGeneratorDownloadPdf.createService( TransactionApi.class );
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();

        Call<ResponseBody> call = downloadPdfApi.callDownloadFileWithCustomerID(token, customerId);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

//                    boolean writtenToDisk = writeResponseBodyToDisk(response.body());
                    customerManagerCallBack.onSuccessDownloadStatement( response.body());

                } else {
                    customerManagerCallBack.onError( "opps something went wrong" );                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                customerManagerCallBack.onError( "opps something went wrong" );
            }
        });
    }


}
