
package com.lekhajokha.udharkhata.home.transaction.transaction_list;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.lekhajokha.udharkhata.home.transaction.add_transaction.TransactionAddResponse;


public class TransactionListResponse implements Serializable {

    @Expose
    private Long code;
    @Expose
    private ArrayList<TransactionArrayList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<TransactionArrayList> getData() {
        return data;
    }

    public void setData(ArrayList<TransactionArrayList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
