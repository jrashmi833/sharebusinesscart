package com.lekhajokha.udharkhata.home.transaction.detail_transaction.model;

import android.content.Context;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.home.transaction.TransactionApi;
import com.lekhajokha.udharkhata.home.transaction.TransactionCallBackListener;
import com.lekhajokha.udharkhata.home.transaction.add_transaction.TransactionAddResponse;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionListResponse;
import com.lekhajokha.udharkhata.rest.ServiceGenerator;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.io.IOException;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class DeleteTransactionManager {
    private DeleteTransactionCallBackListener.DeleteTransactionManagerCallback deleteTransactionManagerCallback ;
    private Context context ;

    DeleteTransactionManager(Context context , DeleteTransactionCallBackListener.DeleteTransactionManagerCallback deleteTransactionManagerCallback ) {
        this.context = context;
        this.deleteTransactionManagerCallback = deleteTransactionManagerCallback;
    }

    void callDeleteTransaction(String txn_id ) {
        DeleteApi deleteApi = ServiceGenerator.createService(DeleteApi.class);
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Call<DeleteTransactionResponse> deleteTransactionResponseCall = deleteApi.callDeleteTransaction(token, txn_id);
        deleteTransactionResponseCall.enqueue(new Callback<DeleteTransactionResponse>() {
            @Override
            public void onResponse(Call<DeleteTransactionResponse> call, Response<DeleteTransactionResponse> response) {
                if (null!=response.body()){
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                    deleteTransactionManagerCallback.onSuccessDeleteTransaction(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        deleteTransactionManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        deleteTransactionManagerCallback.onError(response.body().getMessage());
                    }
                }
                } else {
                deleteTransactionManagerCallback.onError( context.getResources().getString(R.string.opps_something_went_wrong) );
            }


            }

            @Override
            public void onFailure(Call<DeleteTransactionResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    deleteTransactionManagerCallback.onError(context.getResources().getString(R.string.no_internet_connection));
                } else {
                    deleteTransactionManagerCallback.onError(context.getResources().getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }




}
