package com.lekhajokha.udharkhata.home.transaction;

import android.content.Context;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.home.transaction.add_transaction.TransactionAddResponse;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionListResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class TransactionPresenter implements TransactionCallBackListener.TransactionManagerCallback {
    private Context context ;
    private TransactionCallBackListener.TransactionView customerView;
    private TransactionManager customerManager;

    public TransactionPresenter(Context context , TransactionCallBackListener.TransactionView customerView) {
        this.context = context ;
        this.customerView = customerView;
        this.customerManager = new TransactionManager(context ,this);
    }

    public void callTransationListApi(String customerID){
        if(((HomeActivity)context).isInternetConneted()) {
            customerView.onShowLoader();
            customerManager.callTransactionList(customerID);
        }else {
            customerView.onError("No internet connection!");
        }

    }



    public void callShareStatementApi(String customerId){
        if(((HomeActivity)context).isInternetConneted()) {
            customerView.onShowLoader();
            customerManager.callDownloadFileWithCustomerID(customerId);
        }else {
            customerView.onError("No internet connection!");
        }

    }



    public void callTransationAddApi(Map<String, RequestBody> params, MultipartBody.Part partbody1, boolean isShare){
        if(((HomeActivity)context).isInternetConneted()) {
            customerView.onShowLoader();
            customerManager.callTransationAddApi(params,partbody1, isShare);
        }else {
            customerView.onError("No internet connection!");
        }

    }


    @Override
    public void onSuccessTransactionList(TransactionListResponse transactionListResponse) {
        customerView.onHideLoader();
        customerView.onSuccessTransactionList(transactionListResponse);
    }

    @Override
    public void onSuccessAddTransactionList(TransactionAddResponse transactionAddResponse,  boolean isShare) {
        customerView.onHideLoader();
        customerView.onSuccessAddTransactionList(transactionAddResponse ,  isShare);
    }

    @Override
    public void onSuccessDownloadStatement(ResponseBody responseBody) {
        customerView.onHideLoader();
        customerView.onSuccessDownloadStatement(responseBody);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        customerView.onHideLoader();
        customerView.onTokenChangeError(errorMessage);
    }

    @Override
    public void onError(String errorMessage) {
        customerView.onHideLoader();
        customerView.onError(errorMessage);
    }

}
