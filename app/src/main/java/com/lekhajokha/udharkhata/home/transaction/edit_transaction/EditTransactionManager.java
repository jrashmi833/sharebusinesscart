package com.lekhajokha.udharkhata.home.transaction.edit_transaction;

import android.content.Context;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.home.transaction.edit_transaction.model.EditTransactionResponse;
import com.lekhajokha.udharkhata.rest.ServiceGenerator;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.io.IOException;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class EditTransactionManager {
    private EditTransactionCallBackListener.EditTransactionManagerCallback customerManagerCallBack ;
    private Context context ;

    EditTransactionManager(Context context ,EditTransactionCallBackListener.EditTransactionManagerCallback customerManagerCallBack ) {
        this.context = context;
        this.customerManagerCallBack = customerManagerCallBack;
    }



    void callEditTransactionAddApi(Map<String, RequestBody> params, MultipartBody.Part partbody1) {
        EditApi transactionAddApi = ServiceGenerator.createService( EditApi.class );
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Call<EditTransactionResponse> transactionAddResponseCall = transactionAddApi.callEditTransactionApi( token, params, partbody1 );
        transactionAddResponseCall.enqueue( new Callback<EditTransactionResponse>() {
            @Override
            public void onResponse(Call<EditTransactionResponse> call, Response<EditTransactionResponse> response) {

                if (null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        customerManagerCallBack.onSuccessEditTransactionList(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            customerManagerCallBack.onTokenChangeError(response.body().getMessage());
                        } else {
                            customerManagerCallBack.onError(response.body().getMessage());
                        }
                    }
                } else {
                    customerManagerCallBack.onError(context.getResources().getString(R.string.opps_something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<EditTransactionResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    customerManagerCallBack.onError( context.getResources().getString(R.string.no_internet_connection) );
                } else {
                    customerManagerCallBack.onError( context.getResources().getString(R.string.opps_something_went_wrong));
                }
            }
        } );
    }




}
