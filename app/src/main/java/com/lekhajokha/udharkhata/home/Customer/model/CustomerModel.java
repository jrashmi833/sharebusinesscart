package com.lekhajokha.udharkhata.home.Customer.model;

import java.io.Serializable;

public class CustomerModel implements Serializable {

    private int id,Cust_id;
    private String name;
    private String phone;
    private String email;
    private String address,txn_type,txn_amount,balance,txn_date;
    private String image;
    private boolean txn_sms_status,txn_email_status;

    public CustomerModel() {
    }

    public CustomerModel(int id, String name, String phone,int Cust_id, String email, String address, String txn_type, String txn_amount, String balance, String txn_date, String image, boolean txn_sms_status, boolean txn_email_status) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.Cust_id = Cust_id;
        this.email = email;
        this.address = address;
        this.txn_type = txn_type;
        this.txn_amount = txn_amount;
        this.balance = balance;
        this.txn_date = txn_date;
        this.image = image;
        this.txn_sms_status = txn_sms_status;
        this.txn_email_status = txn_email_status;
    }

    public int getCust_id() {
        return Cust_id;
    }

    public void setCust_id(int cust_id) {
        Cust_id = cust_id;
    }

    public String getName() {
        return name;
    }

    public String getTxn_type() {
        return txn_type;
    }

    public void setTxn_type(String txn_type) {
        this.txn_type = txn_type;
    }

    public String getTxn_amount() {
        return txn_amount;
    }

    public void setTxn_amount(String txn_amount) {
        this.txn_amount = txn_amount;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getTxn_date() {
        return txn_date;
    }

    public void setTxn_date(String txn_date) {
        this.txn_date = txn_date;
    }

    public void setName(String name) {
        this.name = name;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isTxn_sms_status() {
        return txn_sms_status;
    }

    public void setTxn_sms_status(boolean txn_sms_status) {
        this.txn_sms_status = txn_sms_status;
    }

    public boolean isTxn_email_status() {
        return txn_email_status;
    }

    public void setTxn_email_status(boolean txn_email_status) {
        this.txn_email_status = txn_email_status;
    }
}
