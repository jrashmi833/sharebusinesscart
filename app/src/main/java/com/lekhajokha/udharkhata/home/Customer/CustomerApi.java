package com.lekhajokha.udharkhata.home.Customer;

import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.DeleteCustomerResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerProfileUpdateResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.UpdateCustomerParameter;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.UpdateCustomerProfileResponse;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.GetCustomerListResponse;
import com.lekhajokha.udharkhata.contact_list.add_customer.model.AddCustomerParameter;
import com.lekhajokha.udharkhata.contact_list.add_customer.model.GetCustomerAddedResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface CustomerApi {
    @POST("UpdateCustomerDetail")
    Call<GetCustomerProfileUpdateResponse> callUpdateCustomer(
            @Body UpdateCustomerParameter signUpParameter,@Header("ACCESS-TOKEN") String token);

    @POST("AddCustomer")
    Call<GetCustomerAddedResponse> callAddCustomer(
            @Body AddCustomerParameter addCustomerParameter, @Header("ACCESS-TOKEN") String token);

    @GET("DeleteCustomer")
    Call<DeleteCustomerResponse> callDeleteCustomer(@Header("ACCESS-TOKEN") String token,@Query("customer_id") String Customer_id );


    @GET("GetCustomerList")
    Call<GetCustomerListResponse> callGetCustomerList(@Header("ACCESS-TOKEN") String token);

    @Multipart
    @POST("ChangeCustomerPhoto")
    Call<UpdateCustomerProfileResponse> callCustomerProfileUpdateApi(@Header("ACCESS-TOKEN") String token, @PartMap Map<String, RequestBody> params, @Part MultipartBody.Part file);

}
