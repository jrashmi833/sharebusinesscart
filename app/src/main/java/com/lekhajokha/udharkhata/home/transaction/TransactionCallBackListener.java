package com.lekhajokha.udharkhata.home.transaction;

import com.lekhajokha.udharkhata.base.BaseInterface;
import com.lekhajokha.udharkhata.home.transaction.add_transaction.TransactionAddResponse;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionListResponse;

import okhttp3.ResponseBody;

public class TransactionCallBackListener {

    public interface TransactionView extends BaseInterface {
        void onSuccessTransactionList(TransactionListResponse transactionListResponse);
        void onSuccessAddTransactionList(TransactionAddResponse transactionAddResponse, boolean isShare);
        void onSuccessDownloadStatement(ResponseBody responseBody);
        void onTokenChangeError(String errorMessage);
    }

    public interface TransactionManagerCallback {
        void onSuccessTransactionList(TransactionListResponse transactionListResponse);
        void onSuccessAddTransactionList(TransactionAddResponse transactionAddResponse, boolean isShare);
        void onSuccessDownloadStatement(ResponseBody responseBody);
        void onTokenChangeError(String errorMessage);
        void onError(String errorMessage);
    }

}
