package com.lekhajokha.udharkhata.home.Customer.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.home.Customer.customer_profile.model.CustomerList;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class AddAdapter extends RecyclerView.Adapter<AddAdapter.ViewHolder> {



    private Context context;
    private ArrayList<CustomerList> getAddCustomerAraylist;
    private onClick onClick;
    private int count = 0;
    private String currency;
    private String[] colorArray = new String[]{
            "#FF7F50", "#FFEBCD", "#A9A9A9", "#DEB887", "#E9967A", "#FA8072", "#778899", "#BDB76B", "#F0E68C", "#90EE90",
            "#8FBC8F", "#00FA9A", "#66CDAA", "#3CB371", "#20B2AA", "#ADD8E6", "#87CEFA", "#D8BFD8", "#DDA0DD", "#DA70D6",
            "#DB7093", "#FFB6C1", "#FFE4C4", "#F5DEB3", "#F4A460", "#BC8F8F", "#FFA07A", "#B0C4DE", "#CD5C5C",
            "#C0C0C0", "#D3D3D3"};

    public AddAdapter(Context context, ArrayList<CustomerList> getAddCustomerAraylist, String currency, AddAdapter.onClick onClick) {
        this.context = context;
        this.getAddCustomerAraylist = getAddCustomerAraylist;
        this.onClick = onClick;
        this.currency = currency ;

    }

    public void notifyAdapter(ArrayList<CustomerList> customerModels,String currency) {
        this.getAddCustomerAraylist = customerModels;
        this.currency = currency ;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( context ).inflate( R.layout.row_listmodel, parent, false );
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewCustomerName.setText( getAddCustomerAraylist.get( position ).getCustomerName() );
        if (null != getAddCustomerAraylist.get( position ).getLastTxnDate()) {
            holder.textViewDetail.setText( getAddCustomerAraylist.get( position ).getLastTxnDate() );
        }


        char first = getAddCustomerAraylist.get( position ).getCustomerName().charAt( 0 );
        holder.textViewId.setText( first + "" );
//        setCololur( holder, position );


        if (null != getAddCustomerAraylist.get( position ).getTotalAmount()) {
            holder.textViewBalance.setText( currency + " " + getAddCustomerAraylist.get( position ).getTotalAmount() );
        } else {
            holder.textViewBalance.setText( currency + " 0" );
        }

        if (getAddCustomerAraylist.get( position ).getLastTxnType().equalsIgnoreCase( "credit" )) {
            holder.textViewtype.setTextColor( context.getResources().getColor( R.color.green ) );
            holder.textViewBalance.setTextColor( context.getResources().getColor( R.color.green ) );
            holder.textViewtype.setText( "Received " );
            holder.textViewAmount.setText(  getAddCustomerAraylist.get( position ).getLastTxnAmount());
            holder.textViewAmount.setTextColor( context.getResources().getColor( R.color.green ) );
        } else if (getAddCustomerAraylist.get( position ).getLastTxnType().equalsIgnoreCase( "debit" )) {
            holder.textViewtype.setTextColor( context.getResources().getColor( R.color.red ) );
            holder.textViewBalance.setTextColor( context.getResources().getColor( R.color.red ) );
            holder.textViewtype.setText( "Given -" );
            holder.textViewAmount.setText(  getAddCustomerAraylist.get( position ).getLastTxnAmount());
            holder.textViewAmount.setTextColor( context.getResources().getColor( R.color.red ) );
        } else {
            holder.textViewtype.setText( "" );
            holder.textViewAmount.setText("");
            holder.textViewBalance.setTextColor( context.getResources().getColor( R.color.black ) );
        }


    }

    private void setCololur(ViewHolder viewHolder, int position) {
        if (count > position) {
            count = 0;
        }
        viewHolder.linearLayoutId.setBackground( context.getResources().getDrawable( R.drawable.shape ) );
        Drawable unwrappedDrawable = AppCompatResources.getDrawable( context, R.drawable.shape );
        Drawable wrappedDrawable = DrawableCompat.wrap( unwrappedDrawable );
        DrawableCompat.setTint( wrappedDrawable, Color.parseColor( colorArray[count] ) );

//        viewHolder.imageCustomerProfile.setBackgroundColor(Color.parseColor(colorArray[count]));
        count++;
    }

    @Override
    public int getItemCount() {
        return getAddCustomerAraylist.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewId)
        TextView textViewId;
        @BindView(R.id.linearLayoutId)
        RelativeLayout linearLayoutId;
        @BindView(R.id.textViewCustomerName)
        TextView textViewCustomerName;
        @BindView(R.id.TextViewDetail)
        TextView textViewDetail;
        @BindView(R.id.textViewBalance)
        TextView textViewBalance;
        @BindView(R.id.TextViewtype)
        TextView textViewtype;
        @BindView(R.id.imageCustomerProfile)
        CircleImageView imageCustomerProfile;
        @BindView(R.id.TextViewAmount)
        TextView textViewAmount;
        @OnClick({R.id.linearLayoutId, R.id.layoutUser})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.linearLayoutId:
                    onClick.onProfileClick( getAdapterPosition() );
                    break;
                case R.id.layoutUser:
                    onClick.onUserClick( getAdapterPosition() );

                    break;
            }
        }

        public ViewHolder(@NonNull View itemView) {
            super( itemView );
            ButterKnife.bind( this, itemView );
        }
    }

    public interface onClick {
        void onProfileClick(int position);

        void onUserClick(int position);
    }
}
