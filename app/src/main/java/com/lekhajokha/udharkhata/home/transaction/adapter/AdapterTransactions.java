package com.lekhajokha.udharkhata.home.transaction.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.SignUp.model.User;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionArrayList;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdapterTransactions extends RecyclerView.Adapter<AdapterTransactions.ViewHolder> {



    private String Currency;
    private onClick onClick;
    private Context context;
    private ArrayList<TransactionArrayList> transactionArrayList;

    public AdapterTransactions(AdapterTransactions.onClick onClick, Context context, ArrayList<TransactionArrayList> transactionArrayList) {
        this.onClick = onClick;
        this.context = context;
        this.transactionArrayList = transactionArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( context ).inflate( R.layout.row_transaction, parent, false );
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (SharedPreference.getInstance( context ).getUser().getDisplayCurrency().equals( Constant.symbol )) {
            Currency = SharedPreference.getInstance( context ).getUser().getCurrencySymbol();
        } else {
            Currency = SharedPreference.getInstance( context ).getUser().getCurrencyCode();
        }

        if(!transactionArrayList.get(position).getIsDelete().equals("1")) {
            holder.layoutDeleteTransaction.setVisibility(View.GONE);
            if (transactionArrayList.get(position).getTxnType().equalsIgnoreCase("Debit")) {
                holder.layoutDebit.setVisibility(View.VISIBLE);
                holder.layoutCredit.setVisibility(View.GONE);
                holder.textViewAmoutDebit.setText("-" + Currency + " " + transactionArrayList.get(position).getTxnAmount());
                holder.textViewDateDebit.setText(transactionArrayList.get(position).getTxnDate());
                holder.textViewDescriptionDebit.setText(transactionArrayList.get(position).getDescription());
                if (transactionArrayList.get(position).getDescription().equalsIgnoreCase("")) {
                    holder.layoutDisDebit.setVisibility(View.GONE);
                }
            } else {
                holder.layoutDebit.setVisibility(View.GONE);
                holder.layoutCredit.setVisibility(View.VISIBLE);
                holder.textViewAmoutCredit.setText("+" + Currency + " " + transactionArrayList.get(position).getTxnAmount());
                holder.textViewDateCredit.setText(transactionArrayList.get(position).getTxnDate());
                holder.textViewDescriptionCredit.setText(transactionArrayList.get(position).getDescription());
                if (transactionArrayList.get(position).getDescription().equalsIgnoreCase("")) {
                    holder.layoutDisCredit.setVisibility(View.GONE);
                }
            }
        }else {
            holder.textViewDeleteDate.setText(transactionArrayList.get(position).getDeletedAt());
            holder.layoutDeleteTransaction.setVisibility(View.VISIBLE);
            holder.layoutDebit.setVisibility(View.GONE);
            holder.layoutCredit.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return transactionArrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewAmoutCredit)
        TextView textViewAmoutCredit;
        @BindView(R.id.textViewDateCredit)
        TextView textViewDateCredit;
        @BindView(R.id.textViewTimeCredit)
        TextView textViewTimeCredit;
        @BindView(R.id.textViewDescriptionCredit)
        TextView textViewDescriptionCredit;
        @BindView(R.id.layoutCredit)
        RelativeLayout layoutCredit;
        @BindView(R.id.textViewAmoutDebit)
        TextView textViewAmoutDebit;
        @BindView(R.id.layoutDisCredit)
        LinearLayout layoutDisCredit;
        @BindView(R.id.layoutDisDebit)
        LinearLayout layoutDisDebit;
        @BindView(R.id.textViewDateDebit)
        TextView textViewDateDebit;
        @BindView(R.id.textViewTimeDebit)
        TextView textViewTimeDebit;
        @BindView(R.id.textViewDescriptionDebit)
        TextView textViewDescriptionDebit;
        @BindView(R.id.layoutDebit)
        RelativeLayout layoutDebit;
        @BindView(R.id.trasactionlist)
        LinearLayout trasactionlist;
        @BindView(R.id.layoutDeleteTransaction)
        LinearLayout layoutDeleteTransaction;
        @BindView(R.id.textViewDeleteDate)
        TextView textViewDeleteDate;


        public ViewHolder(@NonNull View itemView) {
            super( itemView );
            ButterKnife.bind( this, itemView );

        }
        @OnClick(R.id.trasactionlist)
        public void onViewClicked() {
            onClick.onTransactionClick( getAdapterPosition() );

        }


    }

    public interface onClick {

        void onTransactionClick(int position);
    }
}
