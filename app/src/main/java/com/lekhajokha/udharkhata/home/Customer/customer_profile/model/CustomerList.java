
package com.lekhajokha.udharkhata.home.Customer.customer_profile.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Comparator;

@SuppressWarnings("unused")
public class CustomerList implements Serializable {

    @Expose
    private String address;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("customer_email")
    private String customerEmail;
    @SerializedName("customer_id")
    private String customerId;
    @SerializedName("customer_name")
    private String customerName;
    @SerializedName("customer_phone")
    private String customerPhone;
    @SerializedName("customer_photo")
    private String customerPhoto;
    @SerializedName("is_delete")
    private String isDelete;
    @SerializedName("last_txn_amount")
    private String lastTxnAmount;
    @SerializedName("last_txn_date")
    private String lastTxnDate;
    @SerializedName("last_txn_type")
    private String lastTxnType;
    @SerializedName("total_amount")
    private String totalAmount;
    @SerializedName("total_amount_type")
    private String totalAmountType;
    @SerializedName("txn_email_status")
    private String txnEmailStatus;
    @SerializedName("txn_sms_status")
    private String txnSmsStatus;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("user_id")
    private String userId;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerPhoto() {
        return customerPhoto;
    }

    public void setCustomerPhoto(String customerPhoto) {
        this.customerPhoto = customerPhoto;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getLastTxnAmount() {
        return lastTxnAmount;
    }

    public void setLastTxnAmount(String lastTxnAmount) {
        this.lastTxnAmount = lastTxnAmount;
    }

    public String getLastTxnDate() {
        return lastTxnDate;
    }

    public void setLastTxnDate(String lastTxnDate) {
        this.lastTxnDate = lastTxnDate;
    }

    public String getLastTxnType() {
        return lastTxnType;
    }

    public void setLastTxnType(String lastTxnType) {
        this.lastTxnType = lastTxnType;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalAmountType() {
        return totalAmountType;
    }

    public void setTotalAmountType(String totalAmountType) {
        this.totalAmountType = totalAmountType;
    }

    public String getTxnEmailStatus() {
        return txnEmailStatus;
    }

    public void setTxnEmailStatus(String txnEmailStatus) {
        this.txnEmailStatus = txnEmailStatus;
    }

    public String getTxnSmsStatus() {
        return txnSmsStatus;
    }

    public void setTxnSmsStatus(String txnSmsStatus) {
        this.txnSmsStatus = txnSmsStatus;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public static Comparator<CustomerList> COMPARE_BY_NAME = new Comparator<CustomerList>() {
        public int compare(CustomerList one, CustomerList other) {
            return one.customerName.compareToIgnoreCase(other.customerName);
        }
    };


}
