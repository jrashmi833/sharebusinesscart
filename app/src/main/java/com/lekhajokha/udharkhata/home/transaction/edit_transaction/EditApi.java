package com.lekhajokha.udharkhata.home.transaction.edit_transaction;

import com.lekhajokha.udharkhata.home.transaction.edit_transaction.model.EditTransactionResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface EditApi {

    @Multipart
    @POST("UpdateTxnDetail")
    Call<EditTransactionResponse> callEditTransactionApi(@Header("ACCESS-TOKEN") String token,
                                                            @PartMap Map<String, RequestBody> params,
                                                            @Part MultipartBody.Part bodyImg1);

}
