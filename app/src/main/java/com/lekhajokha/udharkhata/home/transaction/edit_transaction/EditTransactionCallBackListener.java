package com.lekhajokha.udharkhata.home.transaction.edit_transaction;

import com.lekhajokha.udharkhata.base.BaseInterface;
import com.lekhajokha.udharkhata.home.transaction.edit_transaction.model.EditTransactionResponse;

public class EditTransactionCallBackListener {

    public interface EditTransactionView extends BaseInterface {
        void onSuccessEditTransactionList(EditTransactionResponse editTransactionResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface EditTransactionManagerCallback {
        void onSuccessEditTransactionList(EditTransactionResponse editTransactionResponse);
        void onTokenChangeError(String errorMessage);
        void onError(String errorMessage);
    }

}
