package com.lekhajokha.udharkhata.profile;

import android.content.Context;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.profile.business_profile.GetBusinessProfileResponse;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileImageResponse;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileParameter;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileResponse;
import java.io.File;

public class BusinessProfilePresenter implements BusinessProfileCallbackListener.UserProfileManagerCallBack {
    private BusinessProfileCallbackListener.UserProfileView userProfileView;
    private BusinessProfileManager userProfileManager;
    private Context context;

    public BusinessProfilePresenter(BusinessProfileCallbackListener.UserProfileView userProfileView , Context context) {
        this.userProfileView = userProfileView;
        this.context= context;
        this.userProfileManager= new BusinessProfileManager( context,this );
    }
    public void callGetBusinessProfile(){
        if(((BusinessProfileActivity)context).isInternetConneted()) {
            userProfileView.onShowLoader();
            userProfileManager.callGetBusinessProfile();
        }else {
            userProfileView.onError(context.getResources().getString( R.string.no_internet_connection));
        }

    }

    public void callUpdateBusinessProfileApi( UpdateBusinessProfileParameter parameter){
        if(((BusinessProfileActivity)context).isInternetConneted()) {
            userProfileView.onShowLoader();
            userProfileManager.callUpdateBusinessProfileApi(parameter);
        }else {
            userProfileView.onError(context.getResources().getString( R.string.no_internet_connection));
        }

    }

    public void callImageUploadBusinessProfileApi(File file){
        if(((BusinessProfileActivity)context).isInternetConneted()) {
            userProfileView.onShowLoader();
            userProfileManager.callImageUploadBusinessProfileApi( file);
        }else {
            userProfileView.onError(context.getResources().getString( R.string.no_internet_connection));
        }

    }


    @Override
    public void onSuccessGetBusinessProfile(GetBusinessProfileResponse getUserProfileResponse) {
        userProfileView.onHideLoader();
        userProfileView.onSuccessGetBusinessProfile(getUserProfileResponse);
    }

    @Override
    public void onSuccessBusinessImageUpdate(UpdateBusinessProfileImageResponse getUserImageResponse) {
        userProfileView.onHideLoader();
        userProfileView.onSuccessBusinessImageUpdate(getUserImageResponse);
    }

    @Override
    public void onSuccessUpdateBusinessProfile(UpdateBusinessProfileResponse getUpdateProfileresponse) {
        userProfileView.onHideLoader();
        userProfileView.onSuccessUpdateBusinessProfile(getUpdateProfileresponse);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        userProfileView.onHideLoader();
        userProfileView.onTokenChangeError(errorMessage);
    }

    @Override
    public void onError(String errorMessage) {
        userProfileView.onHideLoader();
        userProfileView.onError(errorMessage);
    }
}
