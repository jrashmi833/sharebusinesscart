package com.lekhajokha.udharkhata.profile;

import android.content.Context;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.profile.business_profile.GetBusinessProfileResponse;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileImageResponse;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileParameter;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileResponse;
import com.lekhajokha.udharkhata.rest.ServiceGenerator;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.io.File;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessProfileManager {
    public BusinessProfileCallbackListener.UserProfileManagerCallBack userProfileManagerCallBack ;
private Context context;
    public BusinessProfileManager(Context context ,BusinessProfileCallbackListener.UserProfileManagerCallBack userProfileManagerCallBack) {
        this.userProfileManagerCallBack = userProfileManagerCallBack;
        this.context = context;
    }

    public void callGetBusinessProfile() {
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        BusinessProfileApi getUserProfileApi = ServiceGenerator.createService(BusinessProfileApi.class);
        Call<GetBusinessProfileResponse> getUserProfileResponseCall = getUserProfileApi.callBusinessProfileAPi(token);
        getUserProfileResponseCall.enqueue(new Callback<GetBusinessProfileResponse>() {
            @Override
            public void onResponse(Call<GetBusinessProfileResponse> call, Response<GetBusinessProfileResponse> response) {
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        userProfileManagerCallBack.onSuccessGetBusinessProfile(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            userProfileManagerCallBack.onTokenChangeError(response.body().getMessage());
                        } else {
                            userProfileManagerCallBack.onError(response.body().getMessage());
                        }
                    }
                }else {
                    userProfileManagerCallBack.onError(context.getResources().getString(R.string.no_internet_connection));

                }
            }
            @Override
            public void onFailure(Call<GetBusinessProfileResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    userProfileManagerCallBack.onError(context.getResources().getString(R.string.no_internet_connection));
                } else {
                    userProfileManagerCallBack.onError(context.getResources().getString( R.string.opps_something_went_wrong));
                }
            }
        });
    }

    void callUpdateBusinessProfileApi(UpdateBusinessProfileParameter updateProfilePrameter){
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        BusinessProfileApi getUserProfileApi = ServiceGenerator.createService(BusinessProfileApi.class);
        final Call<UpdateBusinessProfileResponse> loginResponseCall = getUserProfileApi.callUpdateBusinessProfile(token,updateProfilePrameter);
        loginResponseCall.enqueue(new Callback<UpdateBusinessProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateBusinessProfileResponse> call, Response<UpdateBusinessProfileResponse> response) {
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        userProfileManagerCallBack.onSuccessUpdateBusinessProfile(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            userProfileManagerCallBack.onTokenChangeError(response.body().getMessage());
                        } else {
                            userProfileManagerCallBack.onError(response.body().getMessage());
                        }
                    }
                }else {
                    userProfileManagerCallBack.onError(context.getResources().getString(R.string.opps_something_went_wrong));
                }



            }

            @Override
            public void onFailure(Call<UpdateBusinessProfileResponse> call, Throwable t) {
                if(t instanceof IOException){
                    userProfileManagerCallBack.onError(context.getResources().getString(R.string.no_internet_connection));
                }else {
                    userProfileManagerCallBack.onError(context.getResources().getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }

    void callImageUploadBusinessProfileApi( File file) {
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        BusinessProfileApi getUserProfileApi = ServiceGenerator.createService(BusinessProfileApi.class);
        RequestBody requestBodyy = RequestBody.create(MediaType.parse("image"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), requestBodyy);

        Call<UpdateBusinessProfileImageResponse> imageUploadResponseCall = getUserProfileApi.callUpdateBusinessImage(token, body);
        imageUploadResponseCall.enqueue(new Callback<UpdateBusinessProfileImageResponse>() {
            @Override
            public void onResponse(Call<UpdateBusinessProfileImageResponse> call, Response<UpdateBusinessProfileImageResponse> response) {
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        userProfileManagerCallBack.onSuccessBusinessImageUpdate(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            userProfileManagerCallBack.onTokenChangeError(response.body().getMessage());
                        } else {
                            userProfileManagerCallBack.onError(response.body().getMessage());
                        }
                    }
                }else {
                    userProfileManagerCallBack.onError(context.getResources().getString(R.string.opps_something_went_wrong));

                }

            }

            @Override
            public void onFailure(Call<UpdateBusinessProfileImageResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    userProfileManagerCallBack.onError(context.getResources().getString(R.string.no_internet_connection));
                } else {
                    userProfileManagerCallBack.onError(context.getResources().getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }
    }

