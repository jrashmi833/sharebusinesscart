
package com.lekhajokha.udharkhata.profile.business_profile;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class UpdateBusinessProfileImageResponse {

    @Expose
    private Long code;
    @Expose
    private BusinessImageData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public BusinessImageData getData() {
        return data;
    }

    public void setData(BusinessImageData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
