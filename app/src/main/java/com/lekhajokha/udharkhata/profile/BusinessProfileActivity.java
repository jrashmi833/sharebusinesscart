package com.lekhajokha.udharkhata.profile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import com.bumptech.glide.Glide;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.base.BaseActivity;
import com.lekhajokha.udharkhata.login_screen.LoginThroughActivity;
import com.lekhajokha.udharkhata.profile.business_profile.BusinessProfileData;
import com.lekhajokha.udharkhata.profile.business_profile.GetBusinessProfileResponse;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileImageResponse;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileParameter;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileResponse;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;
import com.lekhajokha.udharkhata.utility.crop_image.CropImage;
import com.lekhajokha.udharkhata.utility.crop_image.CropImageView;
import com.lekhajokha.udharkhata.utility.crop_image.LargeViewImageActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.Manifest.permission.CAMERA;
import static android.os.Build.VERSION_CODES.M;

public class BusinessProfileActivity extends BaseActivity implements BusinessProfileCallbackListener.UserProfileView {

    @BindView(R.id.backButton)
    ImageView back;
    public static final int CAMERA_PERMISSION_REQUEST_CODE = 1111;
    public static final int GALLERY_PERMISSION_REQUEST_CODE = 1112;
    public static final int  SELECT_FROM_GALLERY = 2;
    public static  final int CAMERA_PIC_REQUEST = 0;
    Uri mImageCaptureUri;
    @BindView(R.id.profileImage)
    CircleImageView profileImage;
    @BindView(R.id.email)
    ImageView email;
    @BindView(R.id.shareBuisnessCard)
    LinearLayout shareBuisnessCard;
    @BindView(R.id.BuisnessCard)
    CardView BuisnessCard;
    @BindView(R.id.CameraButton)
    ImageView CameraButton;
    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.layoutNameButton)
    RelativeLayout layoutNameButton;
    @BindView(R.id.textViewMobileNumber)
    TextView textViewMobileNumber;
    @BindView(R.id.layoutMobileNumberButton)
    RelativeLayout layoutMobileNumberButton;
    @BindView(R.id.textViewEmail)
    TextView textViewEmail;
    @BindView(R.id.layoutEmailButton)
    RelativeLayout layoutEmailButton;
    @BindView(R.id.textViewAddress)
    TextView textViewAddress;
    @BindView(R.id.layoutAddressButton)
    RelativeLayout layoutAddressButton;
    @BindView(R.id.textViewAbout)
    TextView textViewAbout;
    @BindView(R.id.layoutAboutButton)
    RelativeLayout layoutAboutButton;
    @BindView(R.id.textViewCardEmail)
    TextView textViewCardEmail;
    @BindView(R.id.textViewCardPhone)
    TextView textViewCardPhone;
    @BindView(R.id.imageViewUserProfileCard)
    CircleImageView imageViewUserProfileCard;
    @BindView(R.id.textViewCardName)
    TextView textViewCardName;
    @BindView(R.id.textViewCardAbout)
    TextView textViewCardAbout;
    @BindView(R.id.textViewCardAddress)
    TextView textViewCardAddress;
    @BindView(R.id.relativeEditProfile)
    RelativeLayout relativeEditProfile ;
    private static final int CROP_INTENT_PERMISSION = 104;
    private BusinessProfileData businessProfileData = null ;
    private String accessToken ;
    private BusinessProfilePresenter userProfilePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_profile );
        ButterKnife.bind( this );

        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService( INPUT_METHOD_SERVICE );
            inputMethodManager.hideSoftInputFromWindow( getCurrentFocus().getWindowToken(), 0 );
        }

        accessToken =  SharedPreference.getInstance( BusinessProfileActivity.this ).getUser().getAccessToken();
       userProfilePresenter = new BusinessProfilePresenter(this, this);
        userProfilePresenter.callGetBusinessProfile( );
//        initializeData();
    }

    private void initializeData() {
        textViewName.setText( businessProfileData.getContactPerson() );
        textViewCardName.setText( businessProfileData.getContactPerson() );

        textViewMobileNumber.setText( businessProfileData.getContactNumber() );
        textViewCardPhone.setText( businessProfileData.getContactNumber() );

        if (null != businessProfileData.getAbout()) {
            textViewAbout.setText( businessProfileData.getAbout() );
            textViewCardAbout.setText( businessProfileData.getAbout() );
        }
        if (null != businessProfileData.getAddress()) {
            textViewAddress.setText( businessProfileData.getAddress() );
            textViewCardAddress.setText(businessProfileData.getAddress() );
        }
        if (null != businessProfileData.getContactEmail()) {
            textViewEmail.setText( businessProfileData.getContactEmail() );
            textViewCardEmail.setText( businessProfileData.getContactEmail() );
        }
        if (null != businessProfileData.getAvatar() && !businessProfileData.getAvatar().equals("") ) {
            Glide.with( this ).load( businessProfileData.getAvatar() ).placeholder(R.drawable.user).error(R.drawable.user).into( profileImage );

            Glide.with( this ).load( businessProfileData.getAvatar() ).placeholder(R.drawable.user).error(R.drawable.user).into( imageViewUserProfileCard );
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"From Camera", "From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setTitle( "Please choose an Image" );
        builder.setItems( options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals( "From Camera" )) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkCameraPermission())
                            cameraIntent();
                        else
                            requestPermission();
                    } else
                        cameraIntent();
                } else if (options[item].equals( "From Gallery" )) {
                    if (Build.VERSION.SDK_INT >= M) {
                        if (checkGalleryPermission())
                            galleryIntent();
                        else
                            requestGalleryPermission();
                    } else
                        galleryIntent();
                } else if (options[item].equals( "Cancel" )) {
                    dialog.dismiss();
                }
            }
        } );
        builder.create().show();
    }

    @OnClick(R.id.backButton)
    public void onViewClicked() {
        finish();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions( this ,new String[]{CAMERA}, CAMERA_PERMISSION_REQUEST_CODE );
    }

    private void requestGalleryPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PERMISSION_REQUEST_CODE );
    }

    private boolean checkCameraPermission() {
        int result1 = ContextCompat.checkSelfPermission( this, CAMERA );
        return result1 == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkGalleryPermission() {
        int result2 = ContextCompat.checkSelfPermission( this, Manifest.permission.READ_EXTERNAL_STORAGE );
        return result2 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult( requestCode, permissions, grantResults );
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                }
                break;
            case GALLERY_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    galleryIntent();
                }
                break;
            case CROP_INTENT_PERMISSION:
                if (grantResults.length >2 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    openCropIntent();
                }
                break;

        }
    }

    private void galleryIntent() {
        Intent intent = new Intent().setType( "image/*" ).setAction( Intent.ACTION_GET_CONTENT );
        startActivityForResult( Intent.createChooser( intent, "Select Picture" ), SELECT_FROM_GALLERY );
    }

    private void cameraIntent() {
        Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
        intent.putExtra( MediaStore.EXTRA_OUTPUT, mImageCaptureUri );
        startActivityForResult( intent, CAMERA_PIC_REQUEST );

    }


    @RequiresApi(api = M)
    @OnClick({R.id.profileImage, R.id.CameraButton, R.id.shareBuisnessCard, R.id.textViewSaveButton,R.id.relativeEditProfile ,
            R.id.layoutNameButton, R.id.layoutMobileNumberButton, R.id.layoutEmailButton, R.id.layoutAddressButton, R.id.layoutAboutButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.profileImage:
                if(null != businessProfileData.getAvatar() && !businessProfileData.getAvatar().equalsIgnoreCase("")){
                    startActivity(new Intent(this, LargeViewImageActivity.class)
                    .putExtra("IMG", businessProfileData.getAvatar()));
                }
                break;
            case R.id.relativeEditProfile:
            case R.id.CameraButton:
                if (Build.VERSION.SDK_INT >= M) {
                    if (checkCropIntentPermission())
                        openCropIntent();
                    else
                        requestCropIntentPermission();
                } else
                    openCropIntent();
                break;
            case R.id.shareBuisnessCard:
                ShareCard();
                break;
            case R.id.textViewSaveButton:
                EditProfile();
                break;
            case R.id.layoutNameButton:
                if(null != businessProfileData)
                    dialogName();
                break;
            case R.id.layoutMobileNumberButton:
                if(null != businessProfileData)
                dialogMobile();
                break;
            case R.id.layoutEmailButton:
                if(null != businessProfileData)
                    dialogEmail();
                break;
            case R.id.layoutAddressButton:
                if(null != businessProfileData)
                    dialogAddress();
                break;
            case R.id.layoutAboutButton:
                if(null != businessProfileData)
                    dialogAbout();
                break;
        }
    }

    private void dialogEmail() {
        final Dialog dialog = new Dialog( this );
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView( R.layout.layout_dialog_email );
        dialog.show();
        dialog.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE );
        InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
        imm.toggleSoftInput( InputMethodManager.SHOW_FORCED, 0 );

        EditText editTextEmail = dialog.findViewById( R.id.editTextEmail );
        Button buttonSave = dialog.findViewById( R.id.buttonSave );
        ImageView buttonCancel = dialog.findViewById( R.id.buttonCancel );
        editTextEmail.setText(businessProfileData.getContactEmail() );
        editTextEmail.requestFocus();

        buttonCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
                imm.hideSoftInputFromWindow( editTextEmail.getWindowToken(), 0 );
            }
        } );

        buttonSave.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextEmail.getText().toString().length() != 0) {

                    UpdateBusinessProfileParameter updateProfilePrameter = new UpdateBusinessProfileParameter();
                    updateProfilePrameter.setContactEmail( editTextEmail.getText().toString() );
                    userProfilePresenter.callUpdateBusinessProfileApi( updateProfilePrameter);
                    InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
                    imm.hideSoftInputFromWindow( editTextEmail.getWindowToken(), 0 );

                    dialog.dismiss();


                }
            }
        } );
    }

    private void dialogMobile() {
        final Dialog dialog = new Dialog( this );
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView( R.layout.layout_dialog_mobile );
        dialog.show();
        dialog.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE );

        InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
        imm.toggleSoftInput( InputMethodManager.SHOW_FORCED, 0 );

        EditText editTextMobileNumber = dialog.findViewById( R.id.editTextMobileNumber );
        Button buttonSave = dialog.findViewById( R.id.buttonSave );
        ImageView buttonCancel = dialog.findViewById( R.id.buttonCancel );
        editTextMobileNumber.setText( businessProfileData.getContactNumber() );
        editTextMobileNumber.requestFocus();
        buttonCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
                imm.hideSoftInputFromWindow( editTextMobileNumber.getWindowToken(), 0 );
            }
        } );

        buttonSave.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateBusinessProfileParameter updateProfilePrameter = new UpdateBusinessProfileParameter();
                updateProfilePrameter.setContactNumber( editTextMobileNumber.getText().toString() );

                userProfilePresenter.callUpdateBusinessProfileApi( updateProfilePrameter);

                InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
                imm.hideSoftInputFromWindow( editTextMobileNumber.getWindowToken(), 0 );
                dialog.dismiss();

            }
        } );

    }

    private void dialogName() {
        final Dialog dialog = new Dialog( this );
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView( R.layout.layout_dialog_name );
        dialog.show();
        dialog.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE );
        InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
        imm.toggleSoftInput( InputMethodManager.SHOW_FORCED, 0 );

        EditText editTextName = dialog.findViewById( R.id.editTextName );
        Button buttonSave = dialog.findViewById( R.id.buttonSave );
        ImageView buttonCancel = dialog.findViewById( R.id.buttonCancel );

        editTextName.setText( businessProfileData.getContactPerson() );
        editTextName.requestFocus();

        buttonCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
                imm.hideSoftInputFromWindow( editTextName.getWindowToken(), 0 );
            }
        } );

        buttonSave.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextName.getText().toString().length() != 0) {
                    UpdateBusinessProfileParameter updateProfilePrameter = new UpdateBusinessProfileParameter();
                    updateProfilePrameter.setContactPerson( editTextName.getText().toString() );

                    userProfilePresenter.callUpdateBusinessProfileApi( updateProfilePrameter);

                    InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
                    imm.hideSoftInputFromWindow( editTextName.getWindowToken(), 0 );
                    dialog.dismiss();

                }

            }
        } );

    }

    private void dialogAddress() {

        final Dialog dialog = new Dialog( this );
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView( R.layout.layout_dialog_address );
        dialog.show();
        dialog.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE );
        InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
        imm.toggleSoftInput( InputMethodManager.SHOW_FORCED, 0 );

        EditText editTextAddress = dialog.findViewById( R.id.editTextAddress );
        Button buttonSave = dialog.findViewById( R.id.buttonSave );
        ImageView buttonCancel = dialog.findViewById( R.id.buttonCancel );

        editTextAddress.setImeOptions(EditorInfo.IME_ACTION_DONE);
        editTextAddress.setRawInputType(InputType.TYPE_CLASS_TEXT);

        editTextAddress.setText( businessProfileData.getAddress());
        editTextAddress.requestFocus();

        buttonCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
                imm.hideSoftInputFromWindow( editTextAddress.getWindowToken(), 0 );
            }
        } );

        buttonSave.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextAddress.getText().toString().length() != 0) {
                    UpdateBusinessProfileParameter updateProfilePrameter = new UpdateBusinessProfileParameter();
                    updateProfilePrameter.setAddress( editTextAddress.getText().toString() );

                    userProfilePresenter.callUpdateBusinessProfileApi( updateProfilePrameter);
                    InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
                    imm.hideSoftInputFromWindow( editTextAddress.getWindowToken(), 0 );

                    dialog.dismiss();

                }
            }
        } );

    }

    private void dialogAbout() {

        final Dialog dialog = new Dialog( this );
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView( R.layout.layout_dialog_about );
        dialog.show();

        dialog.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE );
        InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
        imm.toggleSoftInput( InputMethodManager.SHOW_FORCED, 0 );

        EditText editTextAbout = dialog.findViewById( R.id.editTextAbout );
        Button buttonSave = dialog.findViewById( R.id.buttonSave );
        ImageView buttonCancel = dialog.findViewById( R.id.buttonCancel );

        editTextAbout.setImeOptions(EditorInfo.IME_ACTION_DONE);
        editTextAbout.setRawInputType(InputType.TYPE_CLASS_TEXT);

        editTextAbout.setText( businessProfileData.getAbout() );
        editTextAbout.requestFocus();

        buttonCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
                imm.hideSoftInputFromWindow( editTextAbout.getWindowToken(), 0 );
                dialog.dismiss();

            }
        } );

        buttonSave.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextAbout.getText().toString().length() != 0) {
                    UpdateBusinessProfileParameter updateProfilePrameter = new UpdateBusinessProfileParameter();
                    updateProfilePrameter.setAbout( editTextAbout.getText().toString() );

                    userProfilePresenter.callUpdateBusinessProfileApi( updateProfilePrameter );
                    InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
                    imm.hideSoftInputFromWindow( editTextAbout.getWindowToken(), 0 );
                    dialog.dismiss();

                }
            }
        } );

    }

    private void EditProfile() {
        UpdateBusinessProfileParameter updateProfilePrameter = new UpdateBusinessProfileParameter();
        updateProfilePrameter.setAddress( textViewAddress.getText().toString() );
        updateProfilePrameter.setAbout( textViewAbout.getText().toString() );
        updateProfilePrameter.setContactEmail( textViewEmail.getText().toString() );
        updateProfilePrameter.setContactPerson( textViewName.getText().toString() );

        userProfilePresenter.callUpdateBusinessProfileApi( updateProfilePrameter );


    }

    @RequiresApi(api = M)
    private void ShareCard() {
        if (checkGalleryPermission()) {
            Bitmap b = takeScreenshot( BuisnessCard );

            File file = new File( this.getExternalCacheDir(), "share.png" );
            FileOutputStream fOut = null;
            try {
                fOut = new FileOutputStream( file );
                b.compress( Bitmap.CompressFormat.JPEG, 80, fOut );
                fOut.flush();
                fOut.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_TEXT, "Business card");
                intent.putExtra(Intent.EXTRA_STREAM, Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                        FileProvider.getUriForFile(this, "com.lekhajokha.udharkhata.fileprovider", file) : Uri.fromFile(file));
                startActivity(Intent.createChooser(intent, "Share Business card Using"));
            } catch (Exception e){
                e.printStackTrace();
            }
        } else {
            requestGalleryPermission();
        }
    }

    public static Bitmap takeScreenshot(View v) {
        v.setDrawingCacheEnabled( true );
        v.buildDrawingCache( true );
        Bitmap b = Bitmap.createBitmap( v.getDrawingCache() );
        v.setDrawingCacheEnabled( false );
        return b;
    }

    private void openCropIntent() {
        try {
            CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .setMinCropResultSize(400, 400)
                    .start(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @RequiresApi(api = M)
    private void requestCropIntentPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, CROP_INTENT_PERMISSION);
    }

    private boolean checkCropIntentPermission() {
        int result1 = ContextCompat.checkSelfPermission(this, CAMERA);
        int result2 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result1 == PackageManager.PERMISSION_GRANTED
                && result2 == PackageManager.PERMISSION_GRANTED
                && result3 == PackageManager.PERMISSION_GRANTED;
    }

    @SuppressLint("CheckResult")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode, resultCode, data );
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK && null != data) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Bitmap productImageBitmap = null ;
            try {
                productImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), result.getUri());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (null != productImageBitmap) {
                File userImageFile = getUserImageFile( productImageBitmap );
                userProfilePresenter.callImageUploadBusinessProfileApi(userImageFile );
            }
        } else if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK && null != data) {
            Bitmap productImageBitmap = (Bitmap) data.getExtras().get( "data" );
            if (null != productImageBitmap) {
                File userImageFile = getUserImageFile( productImageBitmap );
                userProfilePresenter.callImageUploadBusinessProfileApi(userImageFile );

            }
        } else if (requestCode == SELECT_FROM_GALLERY && resultCode == Activity.RESULT_OK && null != data) {
            Uri galleryURI = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap( getContentResolver(), galleryURI );
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (null != bitmap) {
                File userImageFile = getUserImageFile( bitmap );
                userProfilePresenter.callImageUploadBusinessProfileApi(userImageFile );


            }
        }
    }

//    @Override
//    public void onSuccessSignUp(GetSignUpResponse getSignUpResponse) {
//        // not in use
//    }
//
//    @Override
//    public void onSuccessUpdateUser(UpdateBusinessProfileResponse getUpdateProfileresponse) {
//        showToast( getUpdateProfileresponse.getMessage() );
////        SharedPreference.getInstance(this).putUser( Constant.USER,getUpdateProfileresponse.getData());
////        ApplicationSingleton.getInstance().getProfileUpdateCallback().onProfileUpdate();
//        initializeData();
//
//    }
//
//    @Override
//    public void onSuccessImageUpdate(UpdateBusinessImageResponse getUserImageResponse) {
//        Glide.with( this ).load( getUserImageResponse.getData().getAvatarUrl() ).into( imageViewUserProfileCard );
//        Glide.with( this ).load( getUserImageResponse.getData().getAvatarUrl() ).into( profileImage );
////        User user = SharedPreference.getInstance( this ).getUser();
////        user.setAvatar( getUserImageResponse.getData().getAvatarUrl() );
////        SharedPreference.getInstance( this ).putUser( Constant.USER,user );
////        ApplicationSingleton.getInstance().getProfileUpdateCallback().onProfileUpdate();
//
//    }


    @Override
    public void onSuccessGetBusinessProfile(GetBusinessProfileResponse getUserProfileResponse) {
        if(null != getUserProfileResponse.getData()) {
            this.businessProfileData = getUserProfileResponse.getData();
            initializeData();
        }
    }

    @Override
    public void onSuccessBusinessImageUpdate(UpdateBusinessProfileImageResponse getUserImageResponse) {
        Glide.with( this ).load( getUserImageResponse.getData().getAvatarUrl() ).into( imageViewUserProfileCard );
        Glide.with( this ).load( getUserImageResponse.getData().getAvatarUrl() ).into( profileImage );
    }

    @Override
    public void onSuccessUpdateBusinessProfile(UpdateBusinessProfileResponse getUpdateProfileresponse) {
        if(null != getUpdateProfileresponse.getData()) {
            this.businessProfileData = getUpdateProfileresponse.getData();
            initializeData();
        }
    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        SharedPreference.getInstance( this ).setBoolean( Constant.IS_USER_LOGIN, false );
        Intent intent = new Intent( this, LoginThroughActivity.class );
        intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
        startActivity( intent );
        AppUtils.startFromRightToLeft(BusinessProfileActivity.this);
    }

    @Override
    public void onError(String errorMessage) {
        showToast( errorMessage );
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }


}
