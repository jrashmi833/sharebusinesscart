package com.lekhajokha.udharkhata.profile;

import com.lekhajokha.udharkhata.profile.business_profile.GetBusinessProfileResponse;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileImageResponse;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileParameter;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface BusinessProfileApi {

    @GET("GetBusinessProfile")
    Call<GetBusinessProfileResponse> callBusinessProfileAPi(@Header("ACCESS-TOKEN") String token);


    @POST("UpdateBusinessProfile")
    Call<UpdateBusinessProfileResponse> callUpdateBusinessProfile(@Header("ACCESS-TOKEN") String token,
                                                          @Body UpdateBusinessProfileParameter updateProfilePrameter);
    @Multipart
    @POST("UpdateBusinessImage")
    Call<UpdateBusinessProfileImageResponse> callUpdateBusinessImage(@Header("ACCESS-TOKEN") String token, @Part MultipartBody.Part file);

}
