
package com.lekhajokha.udharkhata.profile.business_profile;

import com.google.gson.annotations.SerializedName;

public class BusinessImageData {

    @SerializedName("avatar_url")
    private String avatarUrl;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

}
