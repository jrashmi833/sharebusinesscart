package com.lekhajokha.udharkhata.profile;

import com.lekhajokha.udharkhata.base.BaseInterface;
import com.lekhajokha.udharkhata.profile.business_profile.GetBusinessProfileResponse;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileImageResponse;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileResponse;

public class BusinessProfileCallbackListener {

    public interface UserProfileManagerCallBack {
        void onSuccessGetBusinessProfile(GetBusinessProfileResponse getUserProfileResponse);
        void onSuccessBusinessImageUpdate(UpdateBusinessProfileImageResponse getUserImageResponse);
        void onSuccessUpdateBusinessProfile(UpdateBusinessProfileResponse getUpdateProfileresponse);
        void onTokenChangeError(String errorMessage);
        void onError(String errorMessage);
    }


    public interface UserProfileView extends BaseInterface {
        void onSuccessGetBusinessProfile(GetBusinessProfileResponse getUserProfileResponse);
        void onSuccessBusinessImageUpdate(UpdateBusinessProfileImageResponse getUserImageResponse);
        void onSuccessUpdateBusinessProfile(UpdateBusinessProfileResponse getUpdateProfileresponse);
        void onTokenChangeError(String errorMessage);
    }
}
