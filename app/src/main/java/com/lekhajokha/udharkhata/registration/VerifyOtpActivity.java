package com.lekhajokha.udharkhata.registration;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.SignUp.LoginSignUpPresenter;
import com.lekhajokha.udharkhata.SignUp.LoginSignUpView;
import com.lekhajokha.udharkhata.SignUp.model.GetSignUpResponse;
import com.lekhajokha.udharkhata.SignUp.model.SignUpParameter;
import com.lekhajokha.udharkhata.base.BaseActivity;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerifyOtpActivity extends BaseActivity implements LoginSignUpView {
    @BindView(R.id.editText1)
    EditText editText1;
    @BindView(R.id.editText2)
    EditText editText2;
    @BindView(R.id.editText3)
    EditText editText3;
    @BindView(R.id.editText4)
    EditText editText4;
    @BindView(R.id.editText5)
    EditText editText5;
    @BindView(R.id.editText6)
    EditText editText6;
    @BindView(R.id.textViewAutoTime)
    TextView textViewAutoTime;
    @BindView(R.id.textViewResendOtp)
    TextView textViewResendOtp;
    private String VerificationId;
    private FirebaseAuth mAuth;
    private LoginSignUpPresenter loginSignUpPresenter;
    @BindView(R.id.buttonSubmitOtp)
    Button buttonSubmitOtp;
    String fcmTOken;
    String phoneCode, phoneNumber;
    private Runnable runnableCounter;
    private AtomicInteger atomicInteger;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_verify_otp);
        ButterKnife.bind(this);
        editText1.addTextChangedListener(new GenericTextWatcher(editText1));
        editText2.addTextChangedListener(new GenericTextWatcher(editText2));
        editText3.addTextChangedListener(new GenericTextWatcher(editText3));
        editText4.addTextChangedListener(new GenericTextWatcher(editText4));
        editText5.addTextChangedListener(new GenericTextWatcher(editText5));
        editText6.addTextChangedListener(new GenericTextWatcher(editText6));
        mAuth = FirebaseAuth.getInstance();
        atomicInteger = new AtomicInteger(60);
        start60SecondsTimer();
        fcmTOken = FirebaseInstanceId.getInstance().getToken();
        phoneNumber = getIntent().getStringExtra("mobile");
        phoneCode = getIntent().getStringExtra("phoneCode");
        sendVerificationCode("+" + phoneCode + phoneNumber);
        loginSignUpPresenter = new LoginSignUpPresenter(this, VerifyOtpActivity.this);


    }

    private void start60SecondsTimer() {
        final Handler handler = new Handler();
        runnableCounter = new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                textViewAutoTime.setText("You will receive OTP in " + Integer.toString(atomicInteger.get()) + " Seconds");
                if (atomicInteger.getAndDecrement() >= 1)
                    handler.postDelayed(this, 1000);
                else {
                    textViewResendOtp.setVisibility(View.VISIBLE);
                    textViewAutoTime.setVisibility(View.GONE);
                    if (null != handler) {
                        handler.removeCallbacks(runnableCounter);
                    }
                }
            }
        };
        handler.postDelayed(runnableCounter, 1000);
    }
    @OnClick({R.id.textViewResendOtp, R.id.buttonSubmitOtp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.textViewResendOtp:
                textViewResendOtp.setVisibility(View.GONE);
                textViewAutoTime.setVisibility(View.VISIBLE);
                atomicInteger = new AtomicInteger(60);
                start60SecondsTimer();
                resendVerificationCode("+" + phoneCode + phoneNumber, mResendToken);

                break;
            case R.id.buttonSubmitOtp:
                if (validate()) {
                    String code = editText1.getText().toString().trim() + editText2.getText().toString().trim() + editText3.getText().toString().trim() + editText4.getText().toString().trim() + editText5.getText().toString().trim() + editText6.getText().toString().trim();
                    phoneVerifyCode(code);
                } else {
                    showToast("Enter OTP first");
                }
                break;
        }
    }


    private boolean validate() {
        if (editText1.getText().length() == 0) {
            return false;
        } else if (editText2.getText().length() == 0) {
            return false;
        } else if (editText3.getText().length() == 0) {
            return false;
        } else if (editText4.getText().length() == 0) {
            return false;
        } else if (editText5.getText().length() == 0) {
            return false;
        } else if (editText6.getText().length() == 0) {
            return false;
        }
        return true;
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mcallBack,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    private void sendVerificationCode(String number) {
//        progressbar.setVisibility( View.VISIBLE );

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                90,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mcallBack);
    }

    private void phoneVerifyCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(VerificationId, code);
        signInWithCredential(credential);
    }



    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    SignUpParameter signUpParameter = new SignUpParameter();
                    signUpParameter.setPhone(phoneNumber);
                    signUpParameter.setFullname(mAuth.getCurrentUser().getDisplayName());
                    signUpParameter.setPhoneCode(phoneCode);
                    signUpParameter.setEmail("");
                    signUpParameter.setAbout("");
                    signUpParameter.setAddress("");
                    signUpParameter.setLoginType("phone");
                    signUpParameter.setAvatar("");
                    signUpParameter.setFcmToken(fcmTOken);
                    signUpParameter.setDeviceType("1");
                    loginSignUpPresenter.callSendOtpApi(signUpParameter, VerifyOtpActivity.this);
//                    Intent intent = new Intent( VerifyOtpActivity.this, ProfileActivity.class );
//                    startActivity( intent );
                   /* SignUpParameter signUpParameter = new SignUpParameter();
                    signUpParameter.setFullname(account.getDisplayName());
                    signUpParameter.setEmail(account.getEmail());

                    signUpParameter.setLoginType("google");
                    signUpParameter.setAvatar(String.valueOf(account.getPhotoUrl()));
                    signUpParameter.setGoogleId(account.getId());
                    signUpParameter.setDeviceType("1");
                    loginSignUpPresenter.callSendOtpApi(signUpParameter, LoginThroughActivity.this);
                   */
                } else {
                    editText1.setText("");
                    editText2.setText("");
                    editText3.setText("");
                    editText4.setText("");
                    editText5.setText("");
                    editText6.setText("");
                    editText1.requestFocus();
                    Toast.makeText(VerifyOtpActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mcallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            VerificationId = s;
        }

        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                digits(code);
                //  otpEdit.setText(code);
                phoneVerifyCode(code);

            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Toast.makeText(VerifyOtpActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    @SuppressLint("SetTextI18n")
    void digits(String code) {
        if (code.length() == 6) {
            editText1.setText(code.charAt(0) + "");
            editText2.setText(code.charAt(1) + "");
            editText3.setText(code.charAt(2) + "");
            editText4.setText(code.charAt(3) + "");
            editText5.setText(code.charAt(4) + "");
            editText6.setText(code.charAt(5) + "");
        }


    }

    @Override
    public void onSuccessSignUp(GetSignUpResponse getSignUpResponse) {
        SharedPreference.getInstance(VerifyOtpActivity.this).setBoolean(Constant.IS_USER_LOGIN, true);
        SharedPreference.getInstance(this).putUser(Constant.USER, getSignUpResponse.getData());
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        AppUtils.startFromRightToLeft(VerifyOtpActivity.this);
    }


    @Override
    public void onTokenChangeError(String errorMessage) {
        showToast(errorMessage);
    }

    @Override
    public void onError(String errorMessage) {
        showToast(errorMessage);
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onHideLoader() {
        hideLoader();
    }


    public class GenericTextWatcher implements TextWatcher {
        private View view;

        public GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.editText1:
                    if (text.length() == 1)
                        editText2.requestFocus();
                    break;
                case R.id.editText2:
                    if (text.length() == 1)
                        editText3.requestFocus();
                    else if (text.length() == 0)
                        editText1.requestFocus();
                    break;
                case R.id.editText3:
                    if (text.length() == 1)
                        editText4.requestFocus();
                    else if (text.length() == 0)
                        editText2.requestFocus();
                    break;
                case R.id.editText4:
                    if (text.length() == 1)
                        editText5.requestFocus();
                    else if (text.length() == 0)
                        editText3.requestFocus();
                    break;
                case R.id.editText5:
                    if (text.length() == 1)
                        editText6.requestFocus();
                    else if (text.length() == 0)
                        editText4.requestFocus();
                    break;
                case R.id.editText6:
                    if (text.length() == 0)
                        editText5.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    }

}

