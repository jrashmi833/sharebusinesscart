package com.lekhajokha.udharkhata.registration;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationActivity extends AppCompatActivity {

    @BindView(R.id.editTextNumber)
    EditText editTextNumber;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    @BindView(R.id.spineer_Code)
    AppCompatSpinner spineerCode;
    @BindView(R.id.country_code)
    CountryCodePicker countryCode;
    String code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView( R.layout.activity_registration );
        ButterKnife.bind( this );

//        spineerCode.setAdapter( new ArrayAdapter<String>( this, R.layout.support_simple_spinner_dropdown_item, CountryData.countryAreaCodes ) );
        countryCode.setOnCountryChangeListener( new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                 code = selectedCountry.getPhoneCode();
            }
        } );

    }


    @OnClick({R.id.buttonSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonSubmit:
                String number = editTextNumber.getText().toString().trim();
//                String code = CountryData.countryAreaCodes[spineerCode.getSelectedItemPosition()];
                if (number.isEmpty() || number.length() < 10) {
                    editTextNumber.setError( " Valid Number is Required" );
                    editTextNumber.requestFocus();
                    return;
                }
                code = countryCode.getSelectedCountryCode();
                Intent intent = new Intent( RegistrationActivity.this, VerifyOtpActivity.class );
                intent.putExtra( "mobile", number);
                intent.putExtra( "phoneCode", code );
                startActivity( intent );
                AppUtils.startFromRightToLeft(RegistrationActivity.this);
                break;

        }
    }

}
