package com.lekhajokha.udharkhata.notification.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.notification.model.NotificationModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    private ArrayList<NotificationModel> notificationModelArrayList;
    private Context context;
    private NotificationCallbackListener notificationCallbackListener;

    public NotificationAdapter(ArrayList<NotificationModel> notificationModelArrayList, Context context,
                               NotificationCallbackListener notificationCallbackListener) {
        this.notificationModelArrayList = notificationModelArrayList;
        this.context = context;
        this.notificationCallbackListener = notificationCallbackListener ;

    }

    public void notifyAdapter(ArrayList<NotificationModel> notificationModelArrayList){
        this.notificationModelArrayList = notificationModelArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( context ).inflate( R.layout.row_notification, parent, false );
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewTitle.setText( notificationModelArrayList.get( position ).getNotifyTitle() );
        holder.textViewTime.setText( notificationModelArrayList.get( position ).getDateTime() );
        holder.textViewDesc.setText( notificationModelArrayList.get( position ).getNotifyMessage() );

        holder.imgDeleteNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               notificationCallbackListener.onDeleteItemClicked(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return notificationModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgDeleteNotification)
        ImageView imgDeleteNotification;
        @BindView(R.id.textViewTitle)
        TextView textViewTitle;
        @BindView(R.id.textViewDesc)
        TextView textViewDesc;
        @BindView(R.id.textViewTime)
        TextView textViewTime;

        public ViewHolder(@NonNull View itemView) {
            super( itemView );
             ButterKnife.bind( this, itemView );
        }
    }

    public interface NotificationCallbackListener {
        void onDeleteItemClicked(int pos);
    }
}
