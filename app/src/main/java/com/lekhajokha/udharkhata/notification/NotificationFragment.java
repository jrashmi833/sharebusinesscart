package com.lekhajokha.udharkhata.notification;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.database.DBHelperSingleton;
import com.lekhajokha.udharkhata.notification.adapter.NotificationAdapter;
import com.lekhajokha.udharkhata.notification.model.NotificationModel;
import com.lekhajokha.udharkhata.utility.ApplicationSingleton;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class NotificationFragment extends Fragment implements PopupMenu.OnMenuItemClickListener, NotificationAdapter.NotificationCallbackListener {

    public static final String TAG =NotificationFragment.class.getSimpleName() ;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.backButton)
    ImageView backButton;
    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.menuNotification)
    ImageView menuNotification;
    @BindView(R.id.layoutheader)
    RelativeLayout layoutheader;
    @BindView(R.id.recycleView)
    RecyclerView recycleView;
    @BindView(R.id.emptyLayout)
    LinearLayout emptyLayout;


    private String mParam1;
    private String mParam2;
    private Context context;
    private Unbinder unbinder;
    private View view;
    private ArrayList<NotificationModel> notificationModelArrayList = new ArrayList<>();
    private  NotificationAdapter notificationAdapter ;
    public NotificationFragment() {
    }


    public static NotificationFragment newInstance(String param1, String param2) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putString( ARG_PARAM1, param1 );
        args.putString( ARG_PARAM2, param2 );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        if (getArguments() != null) {
            mParam1 = getArguments().getString( ARG_PARAM1 );
            mParam2 = getArguments().getString( ARG_PARAM2 );
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate( R.layout.fragment_notification, container, false );
        unbinder = ButterKnife.bind( this, view );
        notificationModelArrayList = DBHelperSingleton.getInstance().mDBDbHelper.getAllContactFromLocalDb();
        if(null != notificationModelArrayList && notificationModelArrayList.size()!= 0){
            Collections.reverse(notificationModelArrayList);
            DBHelperSingleton.getInstance().mDBDbHelper.updateAllRowsIsSeen();
            ApplicationSingleton.getInstance().getNotificationCountUpdateCallback().onNotificationCountUpdate();
            setDataToAdapter();
            emptyLayout.setVisibility(View.GONE);
            menuNotification.setVisibility(View.VISIBLE);
        }else {
            emptyLayout.setVisibility(View.VISIBLE);
            menuNotification.setVisibility(View.GONE);
        }
        return view;
    }

    private void setDataToAdapter() {
         notificationAdapter = new NotificationAdapter( notificationModelArrayList, context , this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( context, LinearLayoutManager.VERTICAL, false );
        if (null != recycleView) {
            emptyLayout.setVisibility( View.GONE );
            recycleView.setVisibility( View.VISIBLE );
            recycleView.setLayoutManager( layoutManager );
            recycleView.setAdapter( notificationAdapter );
        }else {
            emptyLayout.setVisibility( View.VISIBLE );
            recycleView.setVisibility( View.GONE );
        }

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach( context );
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        unbinder.unbind();

    }


    @OnClick({R.id.backButton, R.id.menuNotification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                getActivity().onBackPressed();
                break;
            case R.id.menuNotification:
                PopupMenu popup = new PopupMenu( context, view );
                popup.setOnMenuItemClickListener( this );
                popup.inflate( R.menu.menu_notification );
                popup.show();
                break;
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.menuClearAll:
                deleteDialog("Are you sure you want to clear all notifications?","0","");
                break;
        }
        return false;
    }

    @Override
    public void onDeleteItemClicked(int pos) {
      deleteDialog("Are you sure you want to delete this notification?","1",notificationModelArrayList.get(pos).getId());
    }

    private void deleteDialog(String text, String type, String notificationID) {
        AlertDialog.Builder  alertDialogBuilder = new AlertDialog.Builder( context,
                R.style.AlertDialogTheme );
        alertDialogBuilder.setTitle( getResources().getString( R.string.app_name ) );
        alertDialogBuilder.setIcon( R.drawable.delete );
        alertDialogBuilder
                .setTitle("Confirm Delete..")
                .setMessage( text )
                .setCancelable( false )
                .setPositiveButton( getResources().getString( R.string.Yes ), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        // 1 for Delete using notificationID
                        // 0 for Clear All notifications
                        if(type.equals("1")){
                            if(DBHelperSingleton.getInstance().mDBDbHelper.deleteNotificationByID(notificationID)){
                              updateNotificationList();
                            }
                        }else {
                            DBHelperSingleton.getInstance().mDBDbHelper.deleteAllDataFromTable();
                            updateNotificationList();

                        }

                    }
                } )
                .setNegativeButton( getResources().getString( R.string.No ), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                } );
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void updateNotificationList() {
        notificationModelArrayList = new ArrayList<>();
        notificationModelArrayList = DBHelperSingleton.getInstance().mDBDbHelper.getAllContactFromLocalDb();
        if(null != notificationModelArrayList){
            Collections.reverse(notificationModelArrayList);
            recycleView.setVisibility(View.VISIBLE);
            emptyLayout.setVisibility(View.GONE);
            menuNotification.setVisibility(View.VISIBLE);
            notificationAdapter.notifyAdapter(notificationModelArrayList);
        }else {
            recycleView.setVisibility(View.GONE);
            emptyLayout.setVisibility(View.VISIBLE);
            menuNotification.setVisibility(View.GONE);
        }
    }

}
