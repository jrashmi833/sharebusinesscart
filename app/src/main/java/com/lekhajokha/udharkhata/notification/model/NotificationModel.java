package com.lekhajokha.udharkhata.notification.model;

public class NotificationModel {

    private String notifyType, notifyTitle, notifyMessage, dateTime, customerId, id ;
    private String isSeen = "0" ;

    public NotificationModel() {
    }

    public NotificationModel(String notifyType, String notifyTitle, String notifyMessage, String dateTime, String customerId, String isSeen ) {
        this.notifyType = notifyType;
        this.notifyTitle = notifyTitle;
        this.notifyMessage = notifyMessage;
        this.dateTime = dateTime;
        this.customerId = customerId;
        this.isSeen = isSeen ;
    }

    public String getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(String notifyType) {
        this.notifyType = notifyType;
    }

    public String getNotifyTitle() {
        return notifyTitle;
    }

    public void setNotifyTitle(String notifyTitle) {
        this.notifyTitle = notifyTitle;
    }

    public String getNotifyMessage() {
        return notifyMessage;
    }

    public void setNotifyMessage(String notifyMessage) {
        this.notifyMessage = notifyMessage;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String  getSeen() {
        return isSeen;
    }

    public void setSeen(String seen) {
        isSeen = seen;
    }
}
