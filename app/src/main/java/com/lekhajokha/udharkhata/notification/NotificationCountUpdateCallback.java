package com.lekhajokha.udharkhata.notification;

/**
 * Created by Ritu Patidar on 2/3/2020.
 */

public interface NotificationCountUpdateCallback {
    void onNotificationCountUpdate();
}
