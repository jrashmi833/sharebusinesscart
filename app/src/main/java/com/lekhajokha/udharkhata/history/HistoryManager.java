package com.lekhajokha.udharkhata.history;

import android.content.Context;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.history.model.HistoryParameter;
import com.lekhajokha.udharkhata.history.model.HistoryResponse;
import com.lekhajokha.udharkhata.rest.ServiceGenerator;
import com.lekhajokha.udharkhata.rest.ServiceGeneratorDownloadPdf;
import com.lekhajokha.udharkhata.utility.SharedPreference;
import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class HistoryManager {
    private HistoryCallBackListener.HistoryManagerCallback historyManagerCallback;
    private Context context ;

    public HistoryManager(Context context , HistoryCallBackListener.HistoryManagerCallback historyManagerCallback) {
        this.historyManagerCallback = historyManagerCallback;
        this.context = context;
    }

    void callHistoryApi(HistoryParameter parameter) {
        HistoryApi transactionApi = ServiceGenerator.createService(HistoryApi.class);
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Call<HistoryResponse> responseCall = transactionApi.callHistoryApi(token, parameter);
        responseCall.enqueue(new Callback<HistoryResponse>() {
            @Override
            public void onResponse(Call<HistoryResponse> call, Response<HistoryResponse> response) {
                if (null!=response.body()){
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                    historyManagerCallback.onSuccessGetHistory(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        historyManagerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        historyManagerCallback.onError(response.body().getMessage());
                    }
                }
                } else {
                historyManagerCallback.onError(context.getResources().getString( R.string.opps_something_went_wrong));
            }


            }

            @Override
            public void onFailure(Call<HistoryResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    historyManagerCallback.onError(context.getResources().getString( R.string.no_internet_connection));
                } else {
                    historyManagerCallback.onError(context.getResources().getString( R.string.opps_something_went_wrong));
                }
            }
        });
    }

    void callDownloadReportApi(String txnType, String startDate, String endDate){
        HistoryApi historyApi = ServiceGeneratorDownloadPdf.createService( HistoryApi.class );
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();

        Call<ResponseBody> call = historyApi.callDownloadReportApi(token,txnType , startDate, endDate);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    historyManagerCallback.onSuccessDownloadReport( response.body());

                } else {
                    historyManagerCallback.onError( context.getResources().getString( R.string.opps_something_went_wrong));                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof IOException) {
                    historyManagerCallback.onError(context.getResources().getString( R.string.no_internet_connection));
                } else {
                    historyManagerCallback.onError(context.getResources().getString( R.string.opps_something_went_wrong));
                }
            }
        });
    }



}
