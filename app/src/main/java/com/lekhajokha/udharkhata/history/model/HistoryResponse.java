
package com.lekhajokha.udharkhata.history.model;


import com.google.gson.annotations.Expose;


@SuppressWarnings("unused")
public class HistoryResponse {

    @Expose
    private Long code;
    @Expose
    private HistoryData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public HistoryData getData() {
        return data;
    }

    public void setData(HistoryData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
