package com.lekhajokha.udharkhata.history.upcoming_return_collection;
import android.content.Context;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.history.upcoming_return_collection.model.UpcomingListParameter;
import com.lekhajokha.udharkhata.history.upcoming_return_collection.model.UpcomingListResponse;
import com.lekhajokha.udharkhata.rest.ServiceGenerator;
import com.lekhajokha.udharkhata.utility.SharedPreference;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class UpcomingListManager {
    private UpcomingListCallBackListener.UpcomingListManagerCallback managerCallback;
    private Context context ;

    public UpcomingListManager(Context context , UpcomingListCallBackListener.UpcomingListManagerCallback managerCallback) {
        this.managerCallback = managerCallback;
        this.context = context;
    }

    void getCollectionReturnApi(UpcomingListParameter parameter) {
        UpcomingListApi transactionApi = ServiceGenerator.createService(UpcomingListApi.class);
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        Call<UpcomingListResponse> responseCall = transactionApi.GetCollectionReturnApi(token, parameter);
        responseCall.enqueue(new Callback<UpcomingListResponse>() {
            @Override
            public void onResponse(Call<UpcomingListResponse> call, Response<UpcomingListResponse> response) {
                if (null!=response.body()){
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                    managerCallback.onSuccessGetCollectionReturn(response.body());
                } else {
                    if (response.body().getCode() == 400) {
                        managerCallback.onTokenChangeError(response.body().getMessage());
                    } else {
                        managerCallback.onError(response.body().getMessage());
                    }
                }
                } else {
                managerCallback.onError(context.getResources().getString( R.string.opps_something_went_wrong));
            }


            }

            @Override
            public void onFailure(Call<UpcomingListResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    managerCallback.onError(context.getResources().getString( R.string.no_internet_connection));
                } else {
                    managerCallback.onError(context.getResources().getString( R.string.opps_something_went_wrong));
                }
            }
        });
    }



}
