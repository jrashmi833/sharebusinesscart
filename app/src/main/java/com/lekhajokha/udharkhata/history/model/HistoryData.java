
package com.lekhajokha.udharkhata.history.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class HistoryData {

    @SerializedName("net_balance_type")
    private String netBalanceType;
    @SerializedName("total_net_balance")
    private String totalNetBalance;
    @SerializedName("total_net_credit")
    private String totalNetCredit;
    @SerializedName("total_net_debit")
    private String totalNetDebit;
    @SerializedName("txn_count")
    private String txnCount;
    @SerializedName("txn_credit")
    private String txnCredit;
    @SerializedName("txn_debit")
    private String txnDebit;
    @Expose
    private ArrayList<TxnArrayList> txns;

    public String getNetBalanceType() {
        return netBalanceType;
    }

    public void setNetBalanceType(String netBalanceType) {
        this.netBalanceType = netBalanceType;
    }

    public String getTotalNetBalance() {
        return totalNetBalance;
    }

    public void setTotalNetBalance(String totalNetBalance) {
        this.totalNetBalance = totalNetBalance;
    }

    public String getTotalNetCredit() {
        return totalNetCredit;
    }

    public void setTotalNetCredit(String totalNetCredit) {
        this.totalNetCredit = totalNetCredit;
    }

    public String getTotalNetDebit() {
        return totalNetDebit;
    }

    public void setTotalNetDebit(String totalNetDebit) {
        this.totalNetDebit = totalNetDebit;
    }

    public String getTxnCount() {
        return txnCount;
    }

    public void setTxnCount(String txnCount) {
        this.txnCount = txnCount;
    }

    public String getTxnCredit() {
        return txnCredit;
    }

    public void setTxnCredit(String txnCredit) {
        this.txnCredit = txnCredit;
    }

    public String getTxnDebit() {
        return txnDebit;
    }

    public void setTxnDebit(String txnDebit) {
        this.txnDebit = txnDebit;
    }

    public ArrayList<TxnArrayList> getTxns() {
        return txns;
    }

    public void setTxns(ArrayList<TxnArrayList> txns) {
        this.txns = txns;
    }

}
