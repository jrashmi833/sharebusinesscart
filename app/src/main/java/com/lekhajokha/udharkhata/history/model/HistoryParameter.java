
package com.lekhajokha.udharkhata.history.model;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class HistoryParameter {

    @SerializedName("end_date")
    private String endDate;
    @SerializedName("start_date")
    private String startDate;
    @SerializedName("txn_type")
    private String txnType;

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

}
