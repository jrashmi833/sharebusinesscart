package com.lekhajokha.udharkhata.history.upcoming_return_collection;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.history.upcoming_return_collection.adapter.UpcomingListAdapter;
import com.lekhajokha.udharkhata.history.upcoming_return_collection.model.UpcomingCollectionReturnList;
import com.lekhajokha.udharkhata.history.upcoming_return_collection.model.UpcomingListParameter;
import com.lekhajokha.udharkhata.history.upcoming_return_collection.model.UpcomingListResponse;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.login_screen.LoginThroughActivity;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * created by Ritu patidar 2/3/2020 .
 */
public class UpcomingListFragment extends Fragment implements UpcomingListCallBackListener.UpcomingListView {
    public static final String TAG = UpcomingListFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    @BindView(R.id.backButton)
    ImageView backButton;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.recycleViewUpcomingList)
    RecyclerView recycleViewUpcomingList;
    @BindView(R.id.emptyRecord)
    TextView emptyRecord;
    @BindView(R.id.emptyLayout)
    LinearLayout emptyLayout;
    private Unbinder unbinder;
    private String type;
    private View view;
    private Context context;
    private ArrayList<UpcomingCollectionReturnList> arrayList;


    public UpcomingListFragment() {
    }

    public static UpcomingListFragment newInstance(String param1) {
        UpcomingListFragment fragment = new UpcomingListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_upcoming_list, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (type.equalsIgnoreCase(Constant.Credit)) {
            title.setText("Upcoming Returns");
            emptyRecord.setText("No upcoming Returns Found!");
        } else {
            title.setText("Upcoming Collections");
            emptyRecord.setText("No upcoming Collections Found!");
        }

        UpcomingListParameter parameter = new UpcomingListParameter();
        parameter.setTxnType(type.toLowerCase());
        new UpcomingListPresenter(context, this).getCollectionReturnApi(parameter);


        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.backButton)
    public void onViewClicked() {
        getActivity().onBackPressed();
    }

    private void setAdapter() {
        UpcomingListAdapter historyAdapter = new UpcomingListAdapter(context, arrayList);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
//                new GridLayoutManager( context, RecyclerView.VERTICAL, false );

        if (null != recycleViewUpcomingList) {
            recycleViewUpcomingList.setAdapter(historyAdapter);
            recycleViewUpcomingList.setLayoutManager(layoutManager);
        }
    }


    @Override
    public void onSuccessGetCollectionReturn(UpcomingListResponse upcomingListResponse) {
        arrayList = new ArrayList<>();
        if (null != upcomingListResponse.getData() && upcomingListResponse.getData().size() != 0) {
            arrayList.addAll(upcomingListResponse.getData());
            setAdapter();
            emptyLayout.setVisibility(View.GONE);
            recycleViewUpcomingList.setVisibility(View.VISIBLE);

        } else {
            recycleViewUpcomingList.setVisibility(View.GONE);
            emptyLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN, false);
        Intent intent = new Intent(context, LoginThroughActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        AppUtils.startFromRightToLeft(context);
    }

    @Override
    public void onError(String errorMessage) {
        ((HomeActivity) context).onError(errorMessage);
    }

    @Override
    public void onShowLoader() {
        ((HomeActivity) context).showLoader();
    }

    @Override
    public void onHideLoader() {
        ((HomeActivity) context).hideLoader();
    }


}
