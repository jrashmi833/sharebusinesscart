package com.lekhajokha.udharkhata.history.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.history.model.TxnArrayList;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private Context context;
    private ArrayList<TxnArrayList> txnArrayLists;
    private int count = 0;
    private String displayCurrency;
    String type = " ";
    private String[] colorArray = new String[]{
            "#266ead", "#FFEBCD", "#A9A9A9", "#DEB887", "#E9967A", "#FA8072", "#778899", "#BDB76B", "#F0E68C", "#90EE90",
            "#8FBC8F", "#00FA9A", "#66CDAA", "#3CB371", "#20B2AA", "#ADD8E6", "#87CEFA", "#D8BFD8", "#DDA0DD", "#DA70D6",
            "#DB7093", "#FFB6C1", "#FFE4C4", "#F5DEB3", "#F4A460", "#BC8F8F", "#FFA07A", "#B0C4DE", "#CD5C5C",
            "#C0C0C0", "#D3D3D3"};

    public HistoryAdapter(Context context, ArrayList<TxnArrayList> txnArrayLists) {
        this.context = context;
        this.txnArrayLists = txnArrayLists;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( context ).inflate( R.layout.row_historylistmodel, parent, false );

        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewCustomerName.setText( txnArrayLists.get( position ).getCustomerName() );
        if (null != txnArrayLists.get( position ).getTxnDate()) {
            holder.TextViewDetail.setText( txnArrayLists.get( position ).getTxnDate() );
        }

        if (SharedPreference.getInstance( context ).getUser().getDisplayCurrency().equals( Constant.symbol )) {
            displayCurrency = SharedPreference.getInstance( context ).getUser().getCurrencySymbol();
        } else {
            displayCurrency = SharedPreference.getInstance( context ).getUser().getCurrencyCode();
        }

        if(Constant.Credit.equalsIgnoreCase(txnArrayLists.get( position ).getTxnType())) {
            holder.textViewSymbol.setText( displayCurrency );
            holder. textViewSymbol.setTextColor(context.getResources().getColor(R.color.green));
            holder.textViewBalance.setTextColor(context.getResources().getColor(R.color.green));
            holder.textViewBalance.setText( txnArrayLists.get( position ).getTxnAmount() );

        } else {
            holder.textViewSymbol.setTextColor( context.getResources().getColor( R.color.red ) );
            holder.textViewSymbol.setText( displayCurrency );
            holder.textViewBalance.setTextColor( context.getResources().getColor( R.color.red ) );
            holder.textViewBalance.setText( txnArrayLists.get( position ).getTxnAmount() );
        }


        holder.imageCustomerProfile.setVisibility( View.GONE );
        if(null !=txnArrayLists.get( position ).getCustomerName() ) {
            char first = txnArrayLists.get(position).getCustomerName().charAt(0);
            holder.textViewId.setText(first + "");
            setCololur(holder, position);
        }
//        holder.textViewBalance.setText( txnArrayLists.get( position ).getTxnAmount() );


    }

    private void setCololur(ViewHolder viewHolder, int position) {
        if (count > position) {
            count = 0;
        }
        viewHolder.linearLayoutId.setBackground( context.getResources().getDrawable( R.drawable.shape ) );
//        Drawable unwrappedDrawable = AppCompatResources.getDrawable( context, R.drawable.shape );
//        Drawable wrappedDrawable = DrawableCompat.wrap( unwrappedDrawable );
//        DrawableCompat.setTint( wrappedDrawable, Color.parseColor( colorArray[count] ) );
//        viewHolder.imageCustomerProfile.setBackgroundColor(Color.parseColor(colorArray[count]));
        count++;
    }

    @Override
    public int getItemCount() {
        return txnArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewId)
        TextView textViewId;
        @BindView(R.id.imageCustomerProfile)
        CircleImageView imageCustomerProfile;
        @BindView(R.id.linearLayoutId)
        RelativeLayout linearLayoutId;
        @BindView(R.id.textViewCustomerName)
        TextView textViewCustomerName;
        @BindView(R.id.textViewBalance)
        TextView textViewBalance;
        @BindView(R.id.TextViewDetail)
        TextView TextViewDetail;
        @BindView(R.id.layoutUser)
        LinearLayout layoutUser;
        @BindView(R.id.textViewSymbol)
        TextView textViewSymbol;

        public ViewHolder(@NonNull View itemView) {
            super( itemView );
            ButterKnife.bind( this, itemView );
        }
    }


}
