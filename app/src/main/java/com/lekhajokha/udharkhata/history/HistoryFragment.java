package com.lekhajokha.udharkhata.history;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.history.adapter.HistoryAdapter;
import com.lekhajokha.udharkhata.history.model.HistoryParameter;
import com.lekhajokha.udharkhata.history.model.HistoryResponse;
import com.lekhajokha.udharkhata.history.model.TxnArrayList;
import com.lekhajokha.udharkhata.history.upcoming_return_collection.UpcomingListFragment;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.login_screen.LoginThroughActivity;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.SimpleTimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.ResponseBody;


public class HistoryFragment extends Fragment implements HistoryCallBackListener.HistoryView, PopupMenu.OnMenuItemClickListener {
    public static final String TAG = HistoryFragment.class.getSimpleName();
    ;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.crossButton)
    ImageView crossButton;
    @BindView(R.id.tvTotalCreditSymbol)
    TextView tvTotalCreditSymbol;
    @BindView(R.id.tvTotalCredit)
    TextView tvTotalCredit;
    @BindView(R.id.tvTotalDebitSymbol)
    TextView tvTotalDebitSymbol;
    @BindView(R.id.tvTotalDebit)
    TextView tvTotalDebit;
    @BindView(R.id.tvNetBalance)
    TextView tvNetBalance;
    @BindView(R.id.recycleViewHistoryList)
    RecyclerView recycleViewHistoryList;
    @BindView(R.id.imageViewaddUser)
    ImageView imageViewaddUser;
    @BindView(R.id.downloadReportButton)
    LinearLayout downloadReportButton;
    @BindView(R.id.layoutContactList)
    RelativeLayout layoutContactList;
    @BindView(R.id.textViewRecord)
    TextView textViewRecord;
    @BindView(R.id.textViewTotalCredit)
    TextView textViewTotalCredit;
    @BindView(R.id.textViewTotalDebit)
    TextView textViewTotalDebit;
    @BindView(R.id.tvNetBalanceSymbol)
    TextView tvNetBalanceSymbol;
    @BindView(R.id.layoutFilter)
    LinearLayout layoutFilter;
    @BindView(R.id.buttonMenu)
    ImageView buttonMenu;
    @BindView(R.id.textViewEmptyHistory)
    TextView textViewEmptyHistory;
    private int Year, Month, Day;
    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;
    private Unbinder unbinder;
    private HistoryPresenter historyPresenter;
    private ArrayList<TxnArrayList> txnArrayLists;
    private String type = Constant.All;
    private Date TODAY_DATE = Calendar.getInstance().getTime();
    private String startDate = "", endDate = "";


    public HistoryFragment() {
    }


    public static HistoryFragment newInstance(String param1, String param2) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_history, container, false);
        unbinder = ButterKnife.bind(this, view);
        historyPresenter = new HistoryPresenter(context, this);
        crossButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        String displayCurrency;
        if (SharedPreference.getInstance(context).getUser().getDisplayCurrency().equals(Constant.symbol)) {
            displayCurrency = SharedPreference.getInstance(context).getUser().getCurrencySymbol();
        } else {
            displayCurrency = SharedPreference.getInstance(context).getUser().getCurrencyCode();
        }

        tvTotalCreditSymbol.setText(displayCurrency);
        tvTotalDebitSymbol.setText(displayCurrency);
        tvNetBalanceSymbol.setText(displayCurrency);

        callApi();

        return view;

    }

    private void callApi() {
        HistoryParameter historyParameter = new HistoryParameter();
        historyParameter.setTxnType(type.toLowerCase());
        historyParameter.setStartDate(startDate);
        historyParameter.setEndDate(endDate);
        historyPresenter.callHistoryApi(historyParameter);

    }

    private void setAdapter() {
        HistoryAdapter historyAdapter = new HistoryAdapter(context, txnArrayLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);

        if (null != recycleViewHistoryList) {
            recycleViewHistoryList.setAdapter(historyAdapter);
            recycleViewHistoryList.setLayoutManager(layoutManager);

            recycleViewHistoryList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0) {
                        downloadReportButton.setVisibility(View.GONE);

                    } else {
                        downloadReportButton.setVisibility(View.VISIBLE);
                    }


                }
            });
        }

    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onSuccessGetHistory(HistoryResponse historyResponse) {

        txnArrayLists = new ArrayList<>();
        if (null != historyResponse.getData() ) {
            txnArrayLists.addAll(historyResponse.getData().getTxns());
            textViewRecord.setText(historyResponse.getData().getTxnCount());
            tvTotalCredit.setText(historyResponse.getData().getTotalNetCredit());
            tvTotalDebit.setText(historyResponse.getData().getTotalNetDebit());

            textViewTotalCredit.setText(historyResponse.getData().getTxnCredit());
            textViewTotalDebit.setText(historyResponse.getData().getTxnDebit());

            if (historyResponse.getData().getNetBalanceType().equals("debit")) {
                tvNetBalanceSymbol.setTextColor(context.getResources().getColor(R.color.red));
                tvNetBalance.setTextColor(context.getResources().getColor(R.color.red));
                tvNetBalance.setText(historyResponse.getData().getTotalNetBalance());


            } else {
                tvNetBalanceSymbol.setTextColor(context.getResources().getColor(R.color.green));
                tvNetBalance.setTextColor(context.getResources().getColor(R.color.green));
                tvNetBalance.setText(historyResponse.getData().getTotalNetBalance());


            }

            setAdapter();

            if(null != txnArrayLists && txnArrayLists.size() != 0){
                textViewEmptyHistory.setVisibility(View.GONE);
                downloadReportButton.setVisibility(View.VISIBLE);
            }else {
                textViewEmptyHistory.setVisibility(View.VISIBLE);
                downloadReportButton.setVisibility(View.GONE);
            }



        }

    }

    @Override
    public void onSuccessDownloadReport(ResponseBody responseBody) {
        writeResponseBodyToDisk(responseBody);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN, false);
        Intent intent = new Intent(context, LoginThroughActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        AppUtils.startFromRightToLeft(context);
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShowLoader() {
        ((HomeActivity) context).showLoader();
    }

    @Override
    public void onHideLoader() {
        ((HomeActivity) context).hideLoader();
    }

    @OnClick({R.id.crossButton, R.id.layoutFilter, R.id.buttonMenu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.crossButton:
                getActivity().onBackPressed();
                break;
            case R.id.layoutFilter:
                filter();
                break;
            case R.id.buttonMenu:
                PopupMenu popup = new PopupMenu(context, view);
                popup.setOnMenuItemClickListener(this);
                popup.inflate(R.menu.history_menu);
                popup.show();
                break;
        }
    }

    private void filter() {
        final Dialog dialog = new Dialog(context);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.layout_dialog_filter);
        dialog.show();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        ImageView buttonCancel = dialog.findViewById(R.id.buttonCancel);
        LinearLayout linearLayoutStart = dialog.findViewById(R.id.layoutStartDate);
        LinearLayout linearLayoutEnd = dialog.findViewById(R.id.layotEndDate);
        Button buttonSelecAll = dialog.findViewById(R.id.buttonAll);
        Button buttonDebit = dialog.findViewById(R.id.buttonDebit);
        Button buttonCredit = dialog.findViewById(R.id.buttonCredit);
        Button buttonClear = dialog.findViewById(R.id.buttonClear);
        Button buttonDone = dialog.findViewById(R.id.buttonDone);
        TextView textViewStartDate = dialog.findViewById(R.id.textViewStartDate);
        TextView textViewEndDate = dialog.findViewById(R.id.textViewEndDate);

        if (!startDate.equalsIgnoreCase("")) {
            try {
                textViewStartDate.setText(Constant.DisplayFormat.format(Constant.ServerDateTimeFormat.parse(startDate)));
            } catch (ParseException e) {
                textViewStartDate.setText(startDate);
                e.printStackTrace();
            }
        }

        if (!endDate.equalsIgnoreCase("")) {
            try {
                textViewEndDate.setText(Constant.DisplayFormat.format(Constant.ServerDateTimeFormat.parse(endDate)));
            } catch (ParseException e) {
                textViewEndDate.setText(startDate);
                e.printStackTrace();
            }
        }

        switch (type) {
            case Constant.All:
                buttonSelecAll.setBackground(getResources().getDrawable(R.drawable.shape_rectangle_baground_accent));
                buttonSelecAll.setTextColor(getResources().getColor(R.color.white));

                buttonCredit.setBackground(getResources().getDrawable(R.drawable.shape_edit_text));
                buttonCredit.setTextColor(getResources().getColor(R.color.textColorGrey));

                buttonDebit.setBackground(getResources().getDrawable(R.drawable.shape_edit_text));
                buttonDebit.setTextColor(getResources().getColor(R.color.textColorGrey));
                break;
            case Constant.Credit:
                buttonCredit.setBackground(getResources().getDrawable(R.drawable.shape_rectangle_baground_accent));
                buttonCredit.setTextColor(getResources().getColor(R.color.white));

                buttonDebit.setBackground(getResources().getDrawable(R.drawable.shape_edit_text));
                buttonDebit.setTextColor(getResources().getColor(R.color.textColorGrey));

                buttonSelecAll.setBackground(getResources().getDrawable(R.drawable.shape_edit_text));
                buttonSelecAll.setTextColor(getResources().getColor(R.color.textColorGrey));
                break;
            case Constant.Debit:
                buttonDebit.setBackground(getResources().getDrawable(R.drawable.shape_rectangle_baground_accent));
                buttonDebit.setTextColor(getResources().getColor(R.color.white));

                buttonCredit.setBackground(getResources().getDrawable(R.drawable.shape_edit_text));
                buttonCredit.setTextColor(getResources().getColor(R.color.textColorGrey));

                buttonSelecAll.setBackground(getResources().getDrawable(R.drawable.shape_edit_text));
                buttonSelecAll.setTextColor(getResources().getColor(R.color.textColorGrey));
                break;
        }

        buttonCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Constant.Credit.equals(type)) {
                    type = Constant.Credit;
                    buttonCredit.setBackground(getResources().getDrawable(R.drawable.shape_rectangle_baground_accent));
                    buttonCredit.setTextColor(getResources().getColor(R.color.white));

                    buttonDebit.setBackground(getResources().getDrawable(R.drawable.shape_edit_text));
                    buttonDebit.setTextColor(getResources().getColor(R.color.textColorGrey));

                    buttonSelecAll.setBackground(getResources().getDrawable(R.drawable.shape_edit_text));
                    buttonSelecAll.setTextColor(getResources().getColor(R.color.textColorGrey));


                }
            }
        });

        buttonDebit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Constant.Debit.equals(type)) {

                    type = Constant.Debit;

                    buttonDebit.setBackground(getResources().getDrawable(R.drawable.shape_rectangle_baground_accent));
                    buttonDebit.setTextColor(getResources().getColor(R.color.white));

                    buttonCredit.setBackground(getResources().getDrawable(R.drawable.shape_edit_text));
                    buttonCredit.setTextColor(getResources().getColor(R.color.textColorGrey));

                    buttonSelecAll.setBackground(getResources().getDrawable(R.drawable.shape_edit_text));
                    buttonSelecAll.setTextColor(getResources().getColor(R.color.textColorGrey));

                }

            }
        });

        buttonSelecAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Constant.All.equals(type)) {
                    type = Constant.All;

                    buttonSelecAll.setBackground(getResources().getDrawable(R.drawable.shape_rectangle_baground_accent));
                    buttonSelecAll.setTextColor(getResources().getColor(R.color.white));

                    buttonCredit.setBackground(getResources().getDrawable(R.drawable.shape_edit_text));
                    buttonCredit.setTextColor(getResources().getColor(R.color.textColorGrey));

                    buttonDebit.setBackground(getResources().getDrawable(R.drawable.shape_edit_text));
                    buttonDebit.setTextColor(getResources().getColor(R.color.textColorGrey));
                }
            }
        });

        linearLayoutStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                Year = c.get(Calendar.YEAR);
                Month = c.get(Calendar.MONTH);
                Day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                c.set(year, (monthOfYear), dayOfMonth);
                                String temp = Constant.DisplayFormat.format(c.getTime());
                                textViewStartDate.setText(temp);
                                startDate = Constant.ServerDateTimeFormat.format(c.getTime());
                                try {
                                    TODAY_DATE = Constant.DisplayFormat.parse(temp);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, Year, Month, Day);
                datePickerDialog.show();
            }
        });

        linearLayoutEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                Year = c.get(Calendar.YEAR);
                Month = c.get(Calendar.MONTH);
                Day = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                c.set(year, (monthOfYear), dayOfMonth);
                                endDate = Constant.ServerDateTimeFormat.format(c.getTime());
                                textViewEndDate.setText(Constant.DisplayFormat.format(c.getTime()));

                            }
                        }, Year, Month, Day);
                datePickerDialog.getDatePicker().setMinDate(TODAY_DATE.getTime());
                datePickerDialog.show();

            }
        });
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                type = Constant.All;
                startDate = "";
                endDate = "";
                callApi();
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textViewStartDate.getText().toString().equals(context.getResources().getString(R.string.filter_start_date))) {
                    startDate = "";
                }
                if (textViewEndDate.getText().toString().equals(context.getResources().getString(R.string.filter_end_date))) {
                    endDate = "";
                }

                callApi();
                dialog.dismiss();
            }
        });


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != unbinder) {
            unbinder.unbind();
        }
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuUpcomingReturns:
                ((HomeActivity) context).addFragmentFragment(UpcomingListFragment.newInstance(Constant.Credit.toLowerCase()), UpcomingListFragment.TAG, true);
                return true;
            case R.id.menuUpcomingCollection:
                ((HomeActivity) context).addFragmentFragment(UpcomingListFragment.newInstance(Constant.Debit.toLowerCase()), UpcomingListFragment.TAG, true);
                return true;

            default:
                return false;

        }
    }

    @OnClick(R.id.downloadReportButton)
    public void onViewClicked() {
       historyPresenter.callDownloadReportApi(type.toLowerCase() ,startDate, endDate);
    }

    @SuppressLint("SimpleDateFormat")
    private String getCurrentDateTime(){
      Date c = Calendar.getInstance().getTime();
        return new SimpleDateFormat("yyyy_MM_dd hh_mm_aa").format(c);
    }


    private void writeResponseBodyToDisk(ResponseBody body) {
        try {
            File futureStudioIconFile = new File(context.getExternalFilesDir(null) + File.separator + "LekhaJokhaReport "+getCurrentDateTime()+".pdf");
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                byte[] fileReader = new byte[4096];
                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;
                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);
                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;
                    Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
                }
                outputStream.flush();
                try {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    intent.setType("application/pdf");
                    intent.putExtra(Intent.EXTRA_SUBJECT,"Lekha Jokha Report "+getCurrentDateTime());
                    intent.putExtra(Intent.EXTRA_TEXT, "Share Transaction Report");
                    intent.putExtra(Intent.EXTRA_STREAM, Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                            FileProvider.getUriForFile(context, "com.lekhajokha.udharkhata.fileprovider", futureStudioIconFile) : Uri.fromFile(futureStudioIconFile));
                    startActivity(Intent.createChooser(intent, "Share Transaction Report"));
                } catch (Exception e){
                    e.printStackTrace();
                }


//                return true;
            } catch (IOException e) {
//                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
//            return false;
        }


    }

}

