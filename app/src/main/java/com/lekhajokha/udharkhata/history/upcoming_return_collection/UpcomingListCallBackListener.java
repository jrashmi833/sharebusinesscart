package com.lekhajokha.udharkhata.history.upcoming_return_collection;

import com.lekhajokha.udharkhata.base.BaseInterface;
import com.lekhajokha.udharkhata.history.model.HistoryResponse;
import com.lekhajokha.udharkhata.history.upcoming_return_collection.model.UpcomingListResponse;

public class UpcomingListCallBackListener {

    public interface UpcomingListView extends BaseInterface {
        void onSuccessGetCollectionReturn(UpcomingListResponse upcomingListResponse);
        void onTokenChangeError(String errorMessage);
    }

    public interface UpcomingListManagerCallback {
        void onSuccessGetCollectionReturn(UpcomingListResponse upcomingListResponse);
        void onTokenChangeError(String errorMessage);
        void onError(String errorMessage);
    }

}
