
package com.lekhajokha.udharkhata.history.upcoming_return_collection.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class UpcomingListResponse {
    @Expose
    private Long code;
    @Expose
    private ArrayList<UpcomingCollectionReturnList> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<UpcomingCollectionReturnList> getData() {
        return data;
    }

    public void setData(ArrayList<UpcomingCollectionReturnList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
