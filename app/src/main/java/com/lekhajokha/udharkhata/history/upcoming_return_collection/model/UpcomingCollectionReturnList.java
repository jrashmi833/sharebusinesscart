
package com.lekhajokha.udharkhata.history.upcoming_return_collection.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UpcomingCollectionReturnList {

    @SerializedName("bill_photo")
    private String billPhoto;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("customer_id")
    private String customerId;
    @SerializedName("customer_name")
    private String customerName;
    @SerializedName("customer_phone")
    private String customerPhone;
    @SerializedName("customer_photo")
    private String customerPhoto;
    @SerializedName("deleted_at")
    private String deletedAt;
    @Expose
    private String description;
    @SerializedName("email_delivered")
    private String emailDelivered;
    @SerializedName("is_delete")
    private String isDelete;
    @SerializedName("reminder_date")
    private String reminderDate;
    @SerializedName("return_date")
    private String returnDate;
    @SerializedName("sms_delivered")
    private String smsDelivered;
    @SerializedName("txn_amount")
    private String txnAmount;
    @SerializedName("txn_date")
    private String txnDate;
    @SerializedName("txn_id")
    private String txnId;
    @SerializedName("txn_type")
    private String txnType;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("user_id")
    private String userId;

    public String getBillPhoto() {
        return billPhoto;
    }

    public void setBillPhoto(String billPhoto) {
        this.billPhoto = billPhoto;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerPhoto() {
        return customerPhoto;
    }

    public void setCustomerPhoto(String customerPhoto) {
        this.customerPhoto = customerPhoto;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmailDelivered() {
        return emailDelivered;
    }

    public void setEmailDelivered(String emailDelivered) {
        this.emailDelivered = emailDelivered;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getReminderDate() {
        return reminderDate;
    }

    public void setReminderDate(String reminderDate) {
        this.reminderDate = reminderDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getSmsDelivered() {
        return smsDelivered;
    }

    public void setSmsDelivered(String smsDelivered) {
        this.smsDelivered = smsDelivered;
    }

    public String getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(String txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }



}
