package com.lekhajokha.udharkhata.history.upcoming_return_collection;

import android.content.Context;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.history.upcoming_return_collection.model.UpcomingListParameter;
import com.lekhajokha.udharkhata.history.upcoming_return_collection.model.UpcomingListResponse;
import com.lekhajokha.udharkhata.home.HomeActivity;


public class UpcomingListPresenter implements UpcomingListCallBackListener.UpcomingListManagerCallback {
    private Context context ;
    private UpcomingListCallBackListener.UpcomingListView customerView;
    private UpcomingListManager upcomingListManager;

    public UpcomingListPresenter(Context context , UpcomingListCallBackListener.UpcomingListView customerView) {
        this.context = context ;
        this.customerView = customerView;
        this.upcomingListManager = new UpcomingListManager(context ,this);
    }

    public void getCollectionReturnApi(UpcomingListParameter parameter){
        if(((HomeActivity) this.context).isInternetConneted()) {
            customerView.onShowLoader();
            upcomingListManager.getCollectionReturnApi(parameter);
        }else {
            customerView.onError(context.getResources().getString( R.string.no_internet_connection));
        }

    }


    @Override
    public void onSuccessGetCollectionReturn(UpcomingListResponse upcomingListResponse) {
        customerView.onHideLoader();
        customerView.onSuccessGetCollectionReturn(upcomingListResponse);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        customerView.onHideLoader();
        customerView.onTokenChangeError(errorMessage);
    }

    @Override
    public void onError(String errorMessage) {
        customerView.onHideLoader();
        customerView.onError(errorMessage);
    }

}
