
package com.lekhajokha.udharkhata.history.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class TxnArrayList {

    @SerializedName("bill_photo")
    private String billPhoto;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("customer_id")
    private String customerId;
    @SerializedName("customer_name")
    private String customerName;
    @Expose
    private String description;
    @SerializedName("email_delivered")
    private String emailDelivered;
    @SerializedName("reminder_date")
    private Object reminderDate;
    @SerializedName("return_date")
    private Object returnDate;
    @SerializedName("sms_delivered")
    private String smsDelivered;
    @SerializedName("txn_amount")
    private String txnAmount;
    @SerializedName("txn_date")
    private String txnDate;
    @SerializedName("txn_id")
    private String txnId;
    @SerializedName("txn_type")
    private String txnType;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("user_id")
    private String userId;

    public String getBillPhoto() {
        return billPhoto;
    }

    public void setBillPhoto(String billPhoto) {
        this.billPhoto = billPhoto;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmailDelivered() {
        return emailDelivered;
    }

    public void setEmailDelivered(String emailDelivered) {
        this.emailDelivered = emailDelivered;
    }

    public Object getReminderDate() {
        return reminderDate;
    }

    public void setReminderDate(Object reminderDate) {
        this.reminderDate = reminderDate;
    }

    public Object getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Object returnDate) {
        this.returnDate = returnDate;
    }

    public String getSmsDelivered() {
        return smsDelivered;
    }

    public void setSmsDelivered(String smsDelivered) {
        this.smsDelivered = smsDelivered;
    }

    public String getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(String txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


}
