package com.lekhajokha.udharkhata.history;

import android.content.Context;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.history.model.HistoryParameter;
import com.lekhajokha.udharkhata.history.model.HistoryResponse;
import com.lekhajokha.udharkhata.home.HomeActivity;

import okhttp3.ResponseBody;


public class HistoryPresenter implements HistoryCallBackListener.HistoryManagerCallback {
    private Context context ;
    private HistoryCallBackListener.HistoryView customerView;
    private HistoryManager customerManager;

    public HistoryPresenter(Context context , HistoryCallBackListener.HistoryView customerView) {
        this.context = context ;
        this.customerView = customerView;
        this.customerManager = new HistoryManager(context ,this);
    }

    public void callHistoryApi(HistoryParameter parameter){
        if(((HomeActivity) this.context).isInternetConneted()) {
            customerView.onShowLoader();
            customerManager.callHistoryApi(parameter);
        }else {
            customerView.onError(context.getResources().getString( R.string.no_internet_connection));
        }

    }

    public void callDownloadReportApi(String txnType, String startDate, String endDate){
        if(((HomeActivity) this.context).isInternetConneted()) {
            customerView.onShowLoader();
            customerManager.callDownloadReportApi(txnType, startDate, endDate);
        }else {
            customerView.onError(context.getResources().getString( R.string.no_internet_connection));
        }

    }


    @Override
    public void onSuccessGetHistory(HistoryResponse historyResponse) {
        customerView.onHideLoader();
        customerView.onSuccessGetHistory(historyResponse);
    }

    @Override
    public void onSuccessDownloadReport(ResponseBody responseBody) {
        customerView.onHideLoader();
        customerView.onSuccessDownloadReport(responseBody);
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        customerView.onHideLoader();
        customerView.onTokenChangeError(errorMessage);
    }

    @Override
    public void onError(String errorMessage) {
        customerView.onHideLoader();
        customerView.onError(errorMessage);
    }

}
