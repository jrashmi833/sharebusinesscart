
package com.lekhajokha.udharkhata.history.upcoming_return_collection.model;

import com.google.gson.annotations.SerializedName;
public class UpcomingListParameter {

    @SerializedName("txn_type")
    private String txnType;

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

}
