package com.lekhajokha.udharkhata.history.upcoming_return_collection;

import com.lekhajokha.udharkhata.history.model.HistoryParameter;
import com.lekhajokha.udharkhata.history.model.HistoryResponse;
import com.lekhajokha.udharkhata.history.upcoming_return_collection.model.UpcomingListParameter;
import com.lekhajokha.udharkhata.history.upcoming_return_collection.model.UpcomingListResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UpcomingListApi {

    @POST("GetCollectionReturn")
    Call<UpcomingListResponse> GetCollectionReturnApi(@Header("ACCESS-TOKEN") String token, @Body UpcomingListParameter parameter);

}
