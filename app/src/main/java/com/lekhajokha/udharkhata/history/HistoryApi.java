package com.lekhajokha.udharkhata.history;

import com.lekhajokha.udharkhata.history.model.HistoryParameter;
import com.lekhajokha.udharkhata.history.model.HistoryResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface HistoryApi {

    @POST("khatabookDashboard")
    Call<HistoryResponse> callHistoryApi(@Header("ACCESS-TOKEN") String token, @Body HistoryParameter parameter);

    @GET("report")
    Call<ResponseBody> callDownloadReportApi(@Query("token")String token, @Query("txn_type") String txnType,
                                             @Query("start_date")String startDate, @Query("end_date") String endDate);

}
