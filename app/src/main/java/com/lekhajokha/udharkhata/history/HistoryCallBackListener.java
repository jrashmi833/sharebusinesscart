package com.lekhajokha.udharkhata.history;

import com.lekhajokha.udharkhata.base.BaseInterface;
import com.lekhajokha.udharkhata.history.model.HistoryResponse;
import com.lekhajokha.udharkhata.home.transaction.add_transaction.TransactionAddResponse;
import com.lekhajokha.udharkhata.home.transaction.transaction_list.TransactionListResponse;

import okhttp3.ResponseBody;

public class HistoryCallBackListener {

    public interface HistoryView extends BaseInterface {
        void onSuccessGetHistory(HistoryResponse historyResponse);
        void onSuccessDownloadReport(ResponseBody responseBody);
        void onTokenChangeError(String errorMessage);
    }

    public interface HistoryManagerCallback {
        void onSuccessGetHistory(HistoryResponse historyResponse);
        void onSuccessDownloadReport(ResponseBody responseBody);
        void onTokenChangeError(String errorMessage);
        void onError(String errorMessage);
    }

}
