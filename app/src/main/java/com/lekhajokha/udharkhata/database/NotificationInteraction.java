package com.lekhajokha.udharkhata.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;


import com.lekhajokha.udharkhata.notification.model.NotificationModel;
import com.lekhajokha.udharkhata.utility.Constant;

import java.util.ArrayList;

/**
 * Created by Ritu Patidar 2/3/2020
 */

public class NotificationInteraction {

    private SQLiteDatabase dbReadable, dbWritable;

    public NotificationInteraction(DatabaseHandler databaseHandler) {
        this.dbReadable = databaseHandler.getReadableDatabase();
        this.dbWritable = databaseHandler.getWritableDatabase();
    }

    public boolean deleteNotificationByID(String ID) {
//        dbWritable.execSQL("delete from "+ Constant.CONTACT_TABLE + " WHERE " +Constant.ID + " = " +ID+"");
       return dbWritable.delete(Constant.CONTACT_TABLE, Constant.ID + "=" + ID, null)>0 ;

    }

    public void deleteAllDataFromTable() {
        dbWritable.execSQL("delete from "+ Constant.CONTACT_TABLE);
    }

    public boolean insertContact(NotificationModel datum) {
            long updateId = dbWritable.insert(Constant.CONTACT_TABLE, null, this.getObject(datum));
        return updateId != -1;

    }

    private ContentValues getObject(NotificationModel datum) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.ID, datum.getId());
        contentValues.put(Constant.NOTIFY_TYPE, datum.getNotifyType());
        contentValues.put(Constant.NOTIFY_TITLE, datum.getNotifyTitle());
        contentValues.put(Constant.NOTIFY_MESSAGE, datum.getNotifyMessage());
        contentValues.put(Constant.NOTIFY_DATE_TIME, datum.getDateTime());
        contentValues.put(Constant.CUSTOMER_ID, datum.getCustomerId());
        contentValues.put(Constant.IS_SEEN, datum.getSeen());
        return contentValues;
    }





    public ArrayList<NotificationModel> getAllContactFromLocalDb() {
        ArrayList<NotificationModel> datumArrayList = new ArrayList<>();
        Cursor cursor = dbReadable.rawQuery(Constant.SELECT_TABLE_CONTACT, null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    NotificationModel datum = new NotificationModel();
                    datum.setId(cursor.getString(cursor.getColumnIndex(Constant.ID)));
                    datum.setCustomerId(cursor.getString(cursor.getColumnIndex(Constant.CUSTOMER_ID)));
                    datum.setNotifyType(cursor.getString(cursor.getColumnIndex(Constant.NOTIFY_TYPE)));
                    datum.setNotifyTitle(cursor.getString(cursor.getColumnIndex(Constant.NOTIFY_TITLE)));
                    datum.setNotifyMessage(cursor.getString(cursor.getColumnIndex(Constant.NOTIFY_MESSAGE)));
                    datum.setDateTime(cursor.getString(cursor.getColumnIndex(Constant.NOTIFY_DATE_TIME)));
                    datum.setSeen(cursor.getString(cursor.getColumnIndex(Constant.IS_SEEN)));
                    datumArrayList.add(datum);
                } while (cursor.moveToNext());
            }
            return datumArrayList;
        }
        cursor.close();
        return null;
    }



//    public int getUnreadNotificationCount(){
//        int count = 0  ;
//        Cursor cursor = dbWritable.rawQuery("SELECT COUNT(*) FROM " + Constant.CONTACT_TABLE + " WHERE " + Constant.IS_SEEN + " = "+"0", null);
//        count = cursor.getCount();
//        cursor.close();
//        return count;
//    }

    public int getUnreadNotificationCount(){

        long numRows = DatabaseUtils.longForQuery(dbReadable, "SELECT COUNT(*) FROM "+Constant.CONTACT_TABLE + " WHERE " + Constant.IS_SEEN + " = '0'", null);
        return (int) numRows;
    }

    public void updateAllRowsIsSeen(){

        ContentValues values = new ContentValues();

        values.put(Constant.IS_SEEN, "1");

        dbWritable.update(Constant.CONTACT_TABLE, values, Constant.IS_SEEN + "=" + "0", null);

//        dbWritable.execSQL("UPDATE " + Constant.CONTACT_TABLE + " SET " + Constant.IS_SEEN +" = '1' "+ " WHERE " + Constant.IS_SEEN + "='0'", null);

    }

}
