package com.lekhajokha.udharkhata.database;

import android.content.Context;

/**
 * Created by Ritu Patidar 2/3/2020
 */

public class DBHelperSingleton {
    public static DatabaseHandler mDBDbHelper = null;
    private static final DBHelperSingleton ourInstance = new DBHelperSingleton();

    public static DBHelperSingleton getInstance() {
        return ourInstance;
    }

    private DBHelperSingleton() {
    }

    public void initDB(Context context){
        mDBDbHelper = new DatabaseHandler(context);
    }
}
