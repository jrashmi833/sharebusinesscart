package com.lekhajokha.udharkhata.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.lekhajokha.udharkhata.notification.model.NotificationModel;
import com.lekhajokha.udharkhata.utility.Constant;
import java.util.ArrayList;

/**
 * Created by Ritu Patidar 2/3/2020
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "LekhaJokha.db";
    private static final int DATABASE_VERSION = 1;
    private NotificationInteraction contactInteraction;


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.contactInteraction = new NotificationInteraction(this);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constant.CREATE_TABLE_NOTIFICATION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newViersion) {
        db.execSQL(Constant.DROP_TABLE_CONTACT);
    }


    /*---------------------------------------- function for contact -----------------------------------------------------------------*/

    public void deleteAllDataFromTable(){
        contactInteraction.deleteAllDataFromTable();
    }

    public boolean insertNotification(NotificationModel datum) {
        return contactInteraction.insertContact(datum);
    }

    public ArrayList<NotificationModel> getAllContactFromLocalDb() {
        return contactInteraction.getAllContactFromLocalDb();
    }

    public boolean deleteNotificationByID(String notificationID) {
       return   contactInteraction.deleteNotificationByID(notificationID);
    }

    public int getUnreadNotificationCount() {
        return contactInteraction.getUnreadNotificationCount();
    }

    public void updateAllRowsIsSeen() {
       contactInteraction.updateAllRowsIsSeen();
    }
}
