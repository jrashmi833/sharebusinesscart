package com.lekhajokha.udharkhata.sidemenu.security;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PasswordActivity extends AppCompatActivity {

    @BindView(R.id.indicator_dots)
    IndicatorDots indicatorDots;
    @BindView(R.id.pin_lock_view)
    PinLockView pinLockView;
    boolean result = false;
    @BindView(R.id.layout)
    LinearLayout layout;
    private String whereFrom = null;
    private Toast toast = null ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_password );
        ButterKnife.bind( this );

        if(null != getIntent() && getIntent().hasExtra( Constant.WHERE_FROM )){
            whereFrom = getIntent().getStringExtra( Constant.WHERE_FROM );
        }

        pinLockView.attachIndicatorDots( indicatorDots );
        pinLockView.setPinLength( 4 );
        pinLockView.setPinLockListener( new PinLockListener() {
            @Override
            public void onComplete(String pin) {

                if(!result) {
                    if (pin.equalsIgnoreCase(SharedPreference.getInstance(PasswordActivity.this).getString(Constant.PASSWORD))) {

                        if (whereFrom.equalsIgnoreCase(Constant.AppLock)) {
                            result = true;
                            Intent intent = new Intent(PasswordActivity.this, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            AppUtils.startFromRightToLeft(PasswordActivity.this);
                            startActivity(intent);
                            finish();
                        } else if (whereFrom.equalsIgnoreCase(Constant.TransactionLock)) {
                            result = true;
                            setResult(Activity.RESULT_OK);
                            finish();
                        } else {
                            onBackPressed();
                            result = true;
                        }

                    } else {
                        showAToast("invalid password");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                pinLockView.resetPinLockView();
                            }
                        }, 200);

                    }
                }


            }

            @Override
            public void onEmpty() {

            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {

            }
        } );
    }

    public void showAToast (String st){
        if (toast == null)
            toast = Toast.makeText(this, st, Toast.LENGTH_SHORT);
        else
            toast.setText(st);
        toast.show();
    }


}
