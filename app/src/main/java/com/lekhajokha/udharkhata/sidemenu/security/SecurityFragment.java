package com.lekhajokha.udharkhata.sidemenu.security;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.sidemenu.security.password.SetPasswordFragment;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class SecurityFragment extends Fragment {

    public static final String TAG = SecurityFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.backButton)
    ImageView backButton;
    @BindView(R.id.switchAppLockPassword)
    Switch switchAppLockPassword;
    @BindView(R.id.switchTransactionPassword)
    Switch switchTransactionPassword;
    private Unbinder unbinder;
    @BindView(R.id.buttonSetPassword)
    Button buttonSetPassword;

    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;


    public SecurityFragment() {
    }


    public static SecurityFragment newInstance(String param1, String param2) {
        SecurityFragment fragment = new SecurityFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_security, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (SharedPreference.getInstance(context).getBoolean(Constant.AppLock, false)) {
           switchAppLockPassword.setChecked(true);
        }


        switchAppLockPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (!"".equals(SharedPreference.getInstance(context).getString(Constant.PASSWORD))) {
                        SharedPreference.getInstance(context).setBoolean(Constant.AppLock, true);
                    } else {
                        ((HomeActivity) context).replaceFragmentFragment(new SetPasswordFragment(), SetPasswordFragment.TAG, true);
                    }
                } else {
                    SharedPreference.getInstance(context).setBoolean(Constant.AppLock, false);
                }
            }
        });

        if (SharedPreference.getInstance(context).getBoolean(Constant.TransactionLock, false)) {
            switchTransactionPassword.setChecked(true);
        }


        switchTransactionPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (!"".equals(SharedPreference.getInstance(context).getString(Constant.PASSWORD))) {
                        SharedPreference.getInstance(context).setBoolean(Constant.TransactionLock, true);
                    } else {
                        ((HomeActivity) context).replaceFragmentFragment(new SetPasswordFragment(), SetPasswordFragment.TAG, true);
                    }
                } else {
                    SharedPreference.getInstance(context).setBoolean(Constant.TransactionLock, false);
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @OnClick(R.id.backButton)
    void onViewClickedBack() {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.buttonSetPassword)
    public void onViewClicked() {
        ((HomeActivity) context).replaceFragmentFragment(new SetPasswordFragment(), SetPasswordFragment.TAG, false);

    }
}
