package com.lekhajokha.udharkhata.sidemenu;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.lekhajokha.udharkhata.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Ritu Patidar on 10/2/2020.
 */
public class MoreAppsFragment extends Fragment {

    @BindView(R.id.backButton)
    ImageView backButton;
    @BindView(R.id.layout4glte)
    LinearLayout layout4glte;
    @BindView(R.id.layoutExpenseManager)
    LinearLayout layoutExpenseManager;
    private View view;
    private Context context;
    private Unbinder unbinder ;


    public static final String TAG = MoreAppsFragment.class.getSimpleName();

    public MoreAppsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_more_apps, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private final String lteAppLink = "com.smtgroup.lte4g3gnetworkandsecretsettings";
    private final String expenseManagerAppLink = "com.lekhajokha.expensemanager";
    @OnClick({R.id.backButton, R.id.layout4glte, R.id.layoutExpenseManager})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                getActivity().onBackPressed();
                break;
            case R.id.layout4glte:
                appIntent(lteAppLink);
                break;
            case R.id.layoutExpenseManager:
                appIntent(expenseManagerAppLink);
                break;
        }
    }

    private void appIntent(String appPackageName) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
