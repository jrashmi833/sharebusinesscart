package com.lekhajokha.udharkhata.sidemenu.security.password;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class SetPasswordFragment extends Fragment {
    public static final String TAG = SetPasswordFragment.class.getSimpleName();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.indicator_dots)
    IndicatorDots indicatorDots;
    @BindView(R.id.pin_lock_view)
    PinLockView pinLockView;
    Unbinder unbinder;
    @BindView(R.id.textViewForgetPasswordButton)
    TextView textViewForgetPasswordButton;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    private Context context;


    public SetPasswordFragment() {
        // Required empty public constructor
    }


    public static SetPasswordFragment newInstance(String param1, String param2) {
        SetPasswordFragment fragment = new SetPasswordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_set_password, container, false);
        unbinder = ButterKnife.bind(this, view);

        pinLockView.attachIndicatorDots(indicatorDots);
        pinLockView.setPinLength(4);
        pinLockView.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {
                ((HomeActivity)context).replaceFragmentFragment(ConfirmPasswordFragment.newInstance(pin),ConfirmPasswordFragment.TAG,false);
            }

            @Override
            public void onEmpty() {

            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {

            }
        });
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @OnClick(R.id.textViewForgetPasswordButton)
    public void onViewClicked() {

    }
}
