package com.lekhajokha.udharkhata.sidemenu.security.password;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ConfirmPasswordFragment extends Fragment {

    public static final String TAG = ConfirmPasswordFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @BindView(R.id.indicator_dots)
    IndicatorDots indicatorDots;
    @BindView(R.id.pin_lock_view)
    PinLockView pinLockView;
    Unbinder unbinder;

    // TODO: Rename and change types of parameters
    private String Pin;
    private String mParam2;
    private Context context;
    private View view;


    public ConfirmPasswordFragment() {
        // Required empty public constructor
    }


    public static ConfirmPasswordFragment newInstance(String param1) {
        ConfirmPasswordFragment fragment = new ConfirmPasswordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Pin = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_confirm_password, container, false);
        unbinder = ButterKnife.bind(this, view);

        pinLockView.attachIndicatorDots(indicatorDots);
        pinLockView.setPinLength(4);
        pinLockView.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {
                if (pin.equalsIgnoreCase(Pin)){
                    SharedPreference.getInstance(context).setString(Constant.PASSWORD,pin);
                    getActivity().finish();
                    getActivity().startActivity(getActivity().getIntent());
                    AppUtils.startFromRightToLeft(context);
                }else {
                    Toast.makeText(context, "Password Do Not Match", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onEmpty() {

            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {

            }
        });
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
