
package com.lekhajokha.udharkhata.account.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class TimeZoneData {

    @SerializedName("country_name")
    private String mCountryName;
    @SerializedName("offset_value")
    private String mOffsetValue;
    @SerializedName("tzone")
    private String mTzone;

    public String getCountryName() {
        return mCountryName;
    }

    public void setCountryName(String countryName) {
        mCountryName = countryName;
    }

    public String getOffsetValue() {
        return mOffsetValue;
    }

    public void setOffsetValue(String offsetValue) {
        mOffsetValue = offsetValue;
    }

    public String getTzone() {
        return mTzone;
    }

    public void setTzone(String tzone) {
        mTzone = tzone;
    }

}
