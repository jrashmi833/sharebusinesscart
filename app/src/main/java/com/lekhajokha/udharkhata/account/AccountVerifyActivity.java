package com.lekhajokha.udharkhata.account;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.utility.Constant;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AccountVerifyActivity extends AppCompatActivity {

    private static final String TAG = AccountVerifyActivity.class.getSimpleName();
    @BindView(R.id.country_code)
    CountryCodePicker countryCode;
    @BindView(R.id.spineer_Code)
    AppCompatSpinner spineerCode;
    @BindView(R.id.editTextNumber)
    EditText editTextNumber;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    @BindView(R.id.layoutAddNo)
    LinearLayout layoutAddNo;
    @BindView(R.id.editText1)
    EditText editText1;
    @BindView(R.id.editText2)
    EditText editText2;
    @BindView(R.id.editText3)
    EditText editText3;
    @BindView(R.id.editText4)
    EditText editText4;
    @BindView(R.id.editText5)
    EditText editText5;
    @BindView(R.id.editText6)
    EditText editText6;
    @BindView(R.id.textViewAutoTime)
    TextView textViewAutoTime;
    @BindView(R.id.textViewResendOtp)
    TextView textViewResendOtp;
    @BindView(R.id.buttonSubmitOtp)
    Button buttonSubmitOtp;
    @BindView(R.id.layoutVerifyNumber)
    LinearLayout layoutVerifyNumber;
    @BindView(R.id.relativeLayoutFragmentContainer)
    LinearLayout relativeLayoutFragmentContainer;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.buttonSubmitEmail)
    Button buttonSubmitEmail;
    @BindView(R.id.layoutAddNoEmail)
    LinearLayout layoutAddNoEmail;
    @BindView(R.id.backButton)
    ImageView backButton;
    private Context context = this;
    private Unbinder unbinder;
    private FirebaseAuth mAuth;
    String code = "";
    String number;
    private Runnable runnableCounter;
    private AtomicInteger atomicInteger;
    private String VerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    String TYPE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verify);
        unbinder = ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent.hasExtra("type")) {
            TYPE = intent.getStringExtra("type");
        }

        if (TYPE.equalsIgnoreCase(Constant.EMAIL)) {
            layoutAddNo.setVisibility(View.GONE);
            layoutAddNoEmail.setVisibility(View.VISIBLE);
        } else {
            layoutAddNoEmail.setVisibility(View.GONE);
            layoutAddNo.setVisibility(View.VISIBLE);
        }

        countryCode.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                code = selectedCountry.getPhoneCode();
            }
        });
        editText1.addTextChangedListener(new GenericTextWatcher(editText1));
        editText2.addTextChangedListener(new GenericTextWatcher(editText2));
        editText3.addTextChangedListener(new GenericTextWatcher(editText3));
        editText4.addTextChangedListener(new GenericTextWatcher(editText4));
        editText5.addTextChangedListener(new GenericTextWatcher(editText5));
        editText6.addTextChangedListener(new GenericTextWatcher(editText6));
        mAuth = FirebaseAuth.getInstance();


    }


    @OnClick({R.id.buttonSubmit, R.id.textViewResendOtp, R.id.buttonSubmitOtp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonSubmit:
                number = editTextNumber.getText().toString().trim();
//                String code = CountryData.countryAreaCodes[spineerCode.getSelectedItemPosition()];
                if (number.isEmpty() || number.length() < 10) {
                    editTextNumber.setError(" Valid Number is Required");
                    editTextNumber.requestFocus();
                    return;
                }
                code = countryCode.getSelectedCountryCode();
                layoutAddNo.setVisibility(View.GONE);
                layoutVerifyNumber.setVisibility(View.VISIBLE);
//                atomicInteger = new AtomicInteger(60);
//                start60SecondsTimer();

                sendVerificationCode("+" + code + number);
                break;
            case R.id.textViewResendOtp:
//                textViewResendOtp.setVisibility(View.GONE);
//                textViewAutoTime.setVisibility(View.VISIBLE);
//               atomicInteger = new AtomicInteger(60);
                resendVerificationCode("+" + code + number, mResendToken);
//               start60SecondsTimer();

                break;
            case R.id.buttonSubmitEmail:
                break;
            case R.id.buttonSubmitOtp:
                if (validate()) {
                    String code = editText1.getText().toString().trim() + editText2.getText().toString().trim() + editText3.getText().toString().trim() + editText4.getText().toString().trim() + editText5.getText().toString().trim() + editText6.getText().toString().trim();
                    phoneVerifyCode(code);
                } else {
                    Toast.makeText(context, "Enter OTP first", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    private boolean validate() {
        if (editText1.getText().length() == 0) {
            return false;
        } else if (editText2.getText().length() == 0) {
            return false;
        } else if (editText3.getText().length() == 0) {
            return false;
        } else if (editText4.getText().length() == 0) {
            return false;
        } else if (editText5.getText().length() == 0) {
            return false;
        } else if (editText6.getText().length() == 0) {
            return false;
        }
        return true;
    }


//    private void start60SecondsTimer() {
//        final Handler handler = new Handler();
//        runnableCounter = new Runnable() {
//            @SuppressLint("SetTextI18n")
//            @Override
//            public void run() {
//                textViewAutoTime.setText("Your will receive OTP in " + Integer.toString(atomicInteger.get()) + " Seconds");
//                if (atomicInteger.getAndDecrement() >= 1)
//                    handler.postDelayed(this, 1000);
//                else {
//                    textViewResendOtp.setVisibility(View.VISIBLE);
//                    textViewAutoTime.setVisibility(View.GONE);
//                    handler.removeCallbacks(runnableCounter);
//                }
//            }
//        };
//        handler.postDelayed(runnableCounter, 1000);
//
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    private void sendVerificationCode(String number) {
//        progressbar.setVisibility( View.VISIBLE );

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                90,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mcallBack);
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // TYPE_PHONE number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mcallBack,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    private void phoneVerifyCode(String code) {
        try {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(VerificationId, code);
            signInWithCredential(credential);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Intent i = new Intent();
                    i.putExtra("key", number);
                    i.putExtra("type", TYPE);
                    i.putExtra("code", code);
                    setResult(RESULT_OK, i);
                    finish();

                } else {
//                    editText1.setText("");
//                    editText2.setText("");
//                    editText3.setText("");
//                    editText4.setText("");
//                    editText5.setText("");
//                    editText6.setText("");
//                    editText1.requestFocus();
                    Toast.makeText(AccountVerifyActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mcallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            VerificationId = s;
        }

        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                digits(code);
                //  otpEdit.setText(code);
                phoneVerifyCode(code);

            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Toast.makeText(AccountVerifyActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    @SuppressLint("SetTextI18n")
    void digits(String code) {
        if (code.length() == 6) {
            editText1.setText(code.charAt(0) + "");
            editText2.setText(code.charAt(1) + "");
            editText3.setText(code.charAt(2) + "");
            editText4.setText(code.charAt(3) + "");
            editText5.setText(code.charAt(4) + "");
            editText6.setText(code.charAt(5) + "");
        }


    }

    public class GenericTextWatcher implements TextWatcher {
        private View view;

        public GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.editText1:
                    if (text.length() == 1)
                        editText2.requestFocus();
                    break;
                case R.id.editText2:
                    if (text.length() == 1)
                        editText3.requestFocus();
                    else if (text.length() == 0)
                        editText1.requestFocus();
                    break;
                case R.id.editText3:
                    if (text.length() == 1)
                        editText4.requestFocus();
                    else if (text.length() == 0)
                        editText2.requestFocus();
                    break;
                case R.id.editText4:
                    if (text.length() == 1)
                        editText5.requestFocus();
                    else if (text.length() == 0)
                        editText3.requestFocus();
                    break;
                case R.id.editText5:
                    if (text.length() == 1)
                        editText6.requestFocus();
                    else if (text.length() == 0)
                        editText4.requestFocus();
                    break;
                case R.id.editText6:
                    if (text.length() == 0)
                        editText5.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    }
}
