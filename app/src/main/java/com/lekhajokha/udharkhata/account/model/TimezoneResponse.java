
package com.lekhajokha.udharkhata.account.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class TimezoneResponse {

    @SerializedName("code")
    private Long mCode;
    @SerializedName("data")
    private ArrayList<TimeZoneData> mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private String mStatus;

    public Long getCode() {
        return mCode;
    }

    public void setCode(Long code) {
        mCode = code;
    }

    public ArrayList<TimeZoneData> getData() {
        return mData;
    }

    public void setData(ArrayList<TimeZoneData> data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

}
