
package com.lekhajokha.udharkhata.account.model;

import com.google.gson.annotations.Expose;

public class UpdateAccountImageResponse {

    @Expose
    private Long code;
    @Expose
    private AccountImageData data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public AccountImageData getData() {
        return data;
    }

    public void setData(AccountImageData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
