
package com.lekhajokha.udharkhata.account.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class GetCountryResponse {

    @Expose
    private Long code;
    @Expose
    private ArrayList<CountryData> data;
    @Expose
    private String message;
    @Expose
    private String status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public ArrayList<CountryData> getData() {
        return data;
    }

    public void setData(ArrayList<CountryData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
