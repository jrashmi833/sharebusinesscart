package com.lekhajokha.udharkhata.account;


import android.content.Context;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.account.model.GetCountryResponse;
import com.lekhajokha.udharkhata.account.model.TimezoneResponse;
import com.lekhajokha.udharkhata.account.model.UpdateAccountImageResponse;
import com.lekhajokha.udharkhata.account.model.UpdateAccountParameter;
import com.lekhajokha.udharkhata.account.model.UpdateAccountResponse;
import com.lekhajokha.udharkhata.currency.CurrencyApi;
import com.lekhajokha.udharkhata.currency.model.CurrencyResponse;
import com.lekhajokha.udharkhata.rest.ServiceGenerator;
import com.lekhajokha.udharkhata.utility.SharedPreference;
import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 3/20/2018.
 */

public class AccountManager {
    private AccountCallbackListener.AccountManagerCallBack managerCallBack;
    private Context context;

    AccountManager(AccountCallbackListener.AccountManagerCallBack accountManagerCallBack, Context context) {
        this.managerCallBack = accountManagerCallBack;
        this.context = context;
    }
    public void callCountryList() {
        AccountApi accountApi = ServiceGenerator.createService( AccountApi.class );
        Call<GetCountryResponse> currencyResponseCall = accountApi.callCountryList();
        currencyResponseCall.enqueue( new Callback<GetCountryResponse>() {
            @Override
            public void onResponse(Call<GetCountryResponse> call, Response<GetCountryResponse> response) {
                if (null!=response.body()){
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        managerCallBack.onSuccessAddCountryList(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            managerCallBack.onTokenChangeError(response.body().getMessage());
                        } else {
                            managerCallBack.onError(response.body().getMessage());
                        }
                    } } else {
                    managerCallBack.onError(context.getResources().getString( R.string.opps_something_went_wrong));
                }


            }

            @Override
            public void onFailure(Call<GetCountryResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    managerCallBack.onError( context.getResources().getString( R.string.no_internet_connection) );
                } else {
                    managerCallBack.onError( context.getResources().getString( R.string.opps_something_went_wrong));
                }
            }
        } );
    }
    public void callTimeZoneList() {
        AccountApi accountApi = ServiceGenerator.createService( AccountApi.class );
        Call<TimezoneResponse> currencyResponseCall = accountApi.callTimezoneList();
        currencyResponseCall.enqueue( new Callback<TimezoneResponse>() {
            @Override
            public void onResponse(Call<TimezoneResponse> call, Response<TimezoneResponse> response) {

                    if (null!=response.body()){
                        if (response.body().getStatus().equalsIgnoreCase("success")) {
                            managerCallBack.onSuccessAddTimeZoneList(response.body());
                        } else {
                            if (response.body().getCode() == 400) {
                                managerCallBack.onTokenChangeError(response.body().getMessage());
                            } else {
                                managerCallBack.onError(response.body().getMessage());
                            }
                } } else {
                        managerCallBack.onError(context.getResources().getString( R.string.opps_something_went_wrong));
                    }

            }

            @Override
            public void onFailure(Call<TimezoneResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    managerCallBack.onError( context.getResources().getString( R.string.no_internet_connection) );
                } else {
                    managerCallBack.onError( context.getResources().getString( R.string.opps_something_went_wrong));
                }
            }
        } );
    }
//    void callSendOtpApi(SignUpParameter enterMobileNumberParamer){
//        LogInApi loginApi = ServiceGenerator.createService(LogInApi.class);
//        final Call<GetSignUpResponse> loginResponseCall = loginApi.callEnterMobileNumberApi(enterMobileNumberParamer);
//        loginResponseCall.enqueue(new Callback<GetSignUpResponse>() {
//            @Override
//            public void onResponse(Call<GetSignUpResponse> call, Response<GetSignUpResponse> response) {
//                if(null != response.body()) {
//                    if (response.body().getStatus().equalsIgnoreCase("success")) {
//                        managerCallBack.onSuccessSignUp(response.body());
//                    } else {
//                        if (response.body().getCode() == 400) {
//                            managerCallBack.onTokenChangeError(response.body().getMessage());
//                        } else {
//                            managerCallBack.onError(response.body().getMessage());
//                        }
//                    }
//                }else {
//                    managerCallBack.onError(context.getResources().getString(R.string.opps_something_went_wrong));
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<GetSignUpResponse> call, Throwable t) {
//                if(t instanceof IOException){
//                    managerCallBack.onError(context.getResources().getString(R.string.no_internet_connection));
//                }else {
//                    managerCallBack.onError(context.getResources().getString(R.string.opps_something_went_wrong));
//                }
//            }
//        });
//    }


    void callUpdateAccountApi(UpdateAccountParameter parameter){
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        AccountApi accountApi = ServiceGenerator.createService(AccountApi.class);
        final Call<UpdateAccountResponse> loginResponseCall = accountApi.callUpdateAccountApi(token, parameter);
        loginResponseCall.enqueue(new Callback<UpdateAccountResponse>() {
            @Override
            public void onResponse(Call<UpdateAccountResponse> call, Response<UpdateAccountResponse> response) {
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        managerCallBack.onSuccessUpdateAccount(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            managerCallBack.onTokenChangeError(response.body().getMessage());
                        } else {
                            managerCallBack.onError(response.body().getMessage());
                        }
                    }
                }else {
                    managerCallBack.onError(context.getResources().getString(R.string.opps_something_went_wrong));
                }



            }

            @Override
            public void onFailure(Call<UpdateAccountResponse> call, Throwable t) {
               if(t instanceof IOException){
                   managerCallBack.onError(context.getResources().getString(R.string.no_internet_connection));
               }else {
                   managerCallBack.onError(context.getResources().getString(R.string.opps_something_went_wrong));
               }
            }
        });
    }

    void callUpdateAccountImageApi(File file) {
        String token = SharedPreference.getInstance(context).getUser().getAccessToken();
        AccountApi accountApi = ServiceGenerator.createService(AccountApi.class);
        RequestBody requestBodyy = RequestBody.create(MediaType.parse("image"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), requestBodyy);

        Call<UpdateAccountImageResponse> imageUploadResponseCall = accountApi.callUpdateAccountImageApi(token, body);
        imageUploadResponseCall.enqueue(new Callback<UpdateAccountImageResponse>() {
            @Override
            public void onResponse(Call<UpdateAccountImageResponse> call, Response<UpdateAccountImageResponse> response) {
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        managerCallBack.onSuccessUpdateImage(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            managerCallBack.onTokenChangeError(response.body().getMessage());
                        } else {
                            managerCallBack.onError(response.body().getMessage());
                        }
                    }
                }else {
                    managerCallBack.onError(context.getResources().getString(R.string.opps_something_went_wrong));

                }

            }

            @Override
            public void onFailure(Call<UpdateAccountImageResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    managerCallBack.onError(context.getResources().getString(R.string.no_internet_connection));
                } else {
                    managerCallBack.onError(context.getResources().getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }



}
