package com.lekhajokha.udharkhata.account;



import com.lekhajokha.udharkhata.account.model.GetCountryResponse;
import com.lekhajokha.udharkhata.account.model.TimezoneResponse;
import com.lekhajokha.udharkhata.account.model.UpdateAccountImageResponse;
import com.lekhajokha.udharkhata.account.model.UpdateAccountParameter;
import com.lekhajokha.udharkhata.account.model.UpdateAccountResponse;
import com.lekhajokha.udharkhata.currency.model.CurrencyResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface AccountApi {

    @POST("UpdateUserProfile")
    Call<UpdateAccountResponse> callUpdateAccountApi(@Header("ACCESS-TOKEN") String token,
                                                  @Body UpdateAccountParameter parameter);
    @Multipart
    @POST("UpdateUserProfileImage")
    Call<UpdateAccountImageResponse> callUpdateAccountImageApi(@Header("ACCESS-TOKEN") String token, @Part MultipartBody.Part file);

    @GET("GetTimeZone")
    Call<TimezoneResponse> callTimezoneList();

    @GET("GetCountry")
    Call<GetCountryResponse> callCountryList();
}
