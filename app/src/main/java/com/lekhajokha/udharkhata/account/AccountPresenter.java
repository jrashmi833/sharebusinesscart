package com.lekhajokha.udharkhata.account;

import android.content.Context;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.account.model.GetCountryResponse;
import com.lekhajokha.udharkhata.account.model.TimezoneResponse;
import com.lekhajokha.udharkhata.account.model.UpdateAccountImageResponse;
import com.lekhajokha.udharkhata.account.model.UpdateAccountParameter;
import com.lekhajokha.udharkhata.account.model.UpdateAccountResponse;
import com.lekhajokha.udharkhata.home.HomeActivity;
import java.io.File;


/**
 * Created by lenovo on 3/20/2018.
 */

public class AccountPresenter implements AccountCallbackListener.AccountManagerCallBack {
    private AccountCallbackListener.AccountView accountView;
    private AccountManager loginManager;
    private Context context;

    public AccountPresenter(AccountCallbackListener.AccountView accountView, Context context) {
        this.accountView = accountView;
        this.context = context;
        this.loginManager = new AccountManager( this, context );
    }
    public void callCountryList(){
        if(((HomeActivity)context).isInternetConneted()) {
            accountView.onShowLoader();
            loginManager.callCountryList();
        }else {
            accountView.onError(context.getResources().getString( R.string.no_internet_connection));
        }

    }
    public void callTimeZoneList(){
        if(((HomeActivity)context).isInternetConneted()) {
            accountView.onShowLoader();
            loginManager.callTimeZoneList();
        }else {
            accountView.onError(context.getResources().getString( R.string.no_internet_connection));
        }

    }
    public void callUpdateAccountApi(UpdateAccountParameter parameter) {
        if (((HomeActivity) context).isInternetConneted()) {
            accountView.onShowLoader();
            loginManager.callUpdateAccountApi(parameter);
        } else {
            accountView.onError( context.getResources().getString( R.string.no_internet_connection) );
        }
    }

    public void callUpdateAccountImageApi(File file) {
        if (((HomeActivity) context).isInternetConneted()) {
            accountView.onShowLoader();
            loginManager.callUpdateAccountImageApi(  file );
        } else {
            accountView.onError( context.getResources().getString( R.string.no_internet_connection) );
        }
    }


    @Override
    public void onError(String errorMessage) {
        accountView.onHideLoader();
        accountView.onError( errorMessage );
    }

    @Override
    public void onSuccessUpdateAccount(UpdateAccountResponse accountResponse) {
        accountView.onHideLoader();
        accountView.onSuccessUpdateAccount( accountResponse );
    }

    @Override
    public void onSuccessAddCountryList(GetCountryResponse getCountryResponse) {
        accountView.onHideLoader();
        accountView.onSuccessAddCountryList( getCountryResponse );
    }

    @Override
    public void onSuccessAddTimeZoneList(TimezoneResponse timezoneResponse) {
        accountView.onHideLoader();
        accountView.onSuccessAddTimeZoneList( timezoneResponse );
    }

    @Override
    public void onSuccessUpdateImage(UpdateAccountImageResponse accountImageResponse) {
        accountView.onHideLoader();
        accountView.onSuccessUpdateImage( accountImageResponse );
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        accountView.onHideLoader();
        accountView.onTokenChangeError( errorMessage );
    }


}
