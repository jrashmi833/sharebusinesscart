package com.lekhajokha.udharkhata.account;

import com.lekhajokha.udharkhata.account.model.GetCountryResponse;
import com.lekhajokha.udharkhata.account.model.TimezoneResponse;
import com.lekhajokha.udharkhata.account.model.UpdateAccountImageResponse;
import com.lekhajokha.udharkhata.account.model.UpdateAccountParameter;
import com.lekhajokha.udharkhata.account.model.UpdateAccountResponse;
import com.lekhajokha.udharkhata.base.BaseInterface;
import com.lekhajokha.udharkhata.profile.business_profile.GetBusinessProfileResponse;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileImageResponse;
import com.lekhajokha.udharkhata.profile.business_profile.UpdateBusinessProfileResponse;

public class AccountCallbackListener {

    public interface AccountManagerCallBack {
        void onSuccessUpdateAccount(UpdateAccountResponse accountResponse);
        void onSuccessAddCountryList(GetCountryResponse getCountryResponse);
        void onSuccessAddTimeZoneList(TimezoneResponse timezoneResponse);
        void onSuccessUpdateImage(UpdateAccountImageResponse accountImageResponse);
        void onTokenChangeError(String errorMessage);
        void onError(String errorMessage);
    }


    public interface AccountView extends BaseInterface {
        void onSuccessUpdateAccount(UpdateAccountResponse accountResponse);
        void onSuccessAddTimeZoneList(TimezoneResponse timezoneResponse);
        void onSuccessAddCountryList(GetCountryResponse getCountryResponse);
        void onSuccessUpdateImage(UpdateAccountImageResponse accountImageResponse);
        void onTokenChangeError(String errorMessage);
    }
}
