package com.lekhajokha.udharkhata.account;

/**
 * Created by Ritu Patidar on 2/3/2020.
 */

public interface ProfileUpdateCallback {
    void onProfileUpdate();
}
