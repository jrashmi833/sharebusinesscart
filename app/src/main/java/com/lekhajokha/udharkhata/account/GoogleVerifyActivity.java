package com.lekhajokha.udharkhata.account;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.SignUp.model.SignUpParameter;
import com.lekhajokha.udharkhata.login_screen.LoginThroughActivity;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.truecaller.android.sdk.TruecallerSDK;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GoogleVerifyActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 101;
    private static final String TAG = GoogleVerifyActivity.class.getSimpleName() ;
    @BindView(R.id.linearGoogle)
    LinearLayout linearGoogle;
    GoogleSignInClient mGoogleSignInClient;
    private String typeString = "EMAIL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_verify);
        ButterKnife.bind(this);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(GoogleVerifyActivity.this.getResources().getString(R.string.server_client_id))
                .requestEmail().build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @OnClick(R.id.linearGoogle)
    public void onViewClicked() {
        signIn();
    }
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
        AppUtils.startFromRightToLeft(GoogleVerifyActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == RC_SIGN_IN) {
            try {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                handleSignInResult(task);
            } catch (Exception e) {
                // The ApiException status code indicates the detailed failure reason.
                e.printStackTrace();
            }

        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticat (account);
        } catch (ApiException e) {

            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            Toast.makeText(this, e.getStatusCode(), Toast.LENGTH_SHORT).show();
            // updateUI(null);
        }

    }

    private void onLoggedIn(GoogleSignInAccount account) {
            String EMAIL =  account.getEmail();
        Intent i = new Intent();
        i.putExtra("key", EMAIL );
        i.putExtra("type", typeString);
        setResult(110,i);
//        SignUpParameter signUpParameter = new SignUpParameter();
//        signUpParameter.setFullname(account.getDisplayName());
//        signUpParameter.setEmail(account.getEmail());
//        signUpParameter.setFcmToken(fcmTOken);
//        signUpParameter.setLoginType("google");
//        signUpParameter.setAvatar(String.valueOf(account.getPhotoUrl()));
//        signUpParameter.setGoogleId(account.getId());
//        signUpParameter.setDeviceType("1");
//        loginSignUpPresenter.callSendOtpApi(signUpParameter, LoginThroughActivity.this);
    }
}