package com.lekhajokha.udharkhata.account;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.SignUp.model.User;
import com.lekhajokha.udharkhata.account.model.CountryData;
import com.lekhajokha.udharkhata.account.model.GetCountryResponse;
import com.lekhajokha.udharkhata.account.model.TimeZoneData;
import com.lekhajokha.udharkhata.account.model.TimezoneResponse;
import com.lekhajokha.udharkhata.account.model.UpdateAccountImageResponse;
import com.lekhajokha.udharkhata.account.model.UpdateAccountParameter;
import com.lekhajokha.udharkhata.account.model.UpdateAccountResponse;
import com.lekhajokha.udharkhata.home.HomeActivity;
import com.lekhajokha.udharkhata.login_screen.LoginThroughActivity;
import com.lekhajokha.udharkhata.utility.AppUtils;
import com.lekhajokha.udharkhata.utility.ApplicationSingleton;
import com.lekhajokha.udharkhata.utility.Constant;
import com.lekhajokha.udharkhata.utility.SharedPreference;
import com.lekhajokha.udharkhata.utility.crop_image.CropImage;
import com.lekhajokha.udharkhata.utility.crop_image.CropImageView;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.Manifest.permission.CAMERA;
import static android.os.Build.VERSION_CODES.M;


public class AccountFragment extends Fragment implements AccountCallbackListener.AccountView {
    public static final String TAG = AccountFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQUEST_NUMBER = 100;
    private static final int REQUEST_ACCOUNT = 110;
    @BindView(R.id.backButton)
    ImageView backButton;
    @BindView(R.id.textViewSaveButton)
    TextView textViewSaveButton;
    @BindView(R.id.profileImage)
    CircleImageView profileImage;
    @BindView(R.id.CameraButton)
    ImageView CameraButton;
    @BindView(R.id.relativeEditProfile)
    RelativeLayout relativeEditProfile;
    @BindView(R.id.textViewUserId)
    TextView textViewUserId;
    @BindView(R.id.textViewFullName)
    TextView textViewFullName;
    @BindView(R.id.textViewNickName)
    TextView textViewNickName;
    @BindView(R.id.tvMobileNumberTABToAdd)
    TextView tvMobileNumberTABToAdd;
    @BindView(R.id.textViewMobileNumber)
    TextView textViewMobileNumber;
    @BindView(R.id.textViewChangeMobile)
    TextView textViewChangeMobile;
    @BindView(R.id.layoutMobileNumber)
    LinearLayout layoutMobileNumber;
    @BindView(R.id.tvEmailTABToAdd)
    TextView tvEmailTABToAdd;
    @BindView(R.id.textViewEmail)
    TextView textViewEmail;
    @BindView(R.id.textViewChangeEmail)
    TextView textViewChangeEmail;
    @BindView(R.id.layoutEmail)
    LinearLayout layoutEmail;
    @BindView(R.id.layoutVerifyGoogleAccount)
    TextView layoutVerifyGoogleAccount;
    @BindView(R.id.spinnerCountry)
    SearchableSpinner spinnerCountry;
    @BindView(R.id.layoutSelectCountry)
    RelativeLayout layoutSelectCountry;
    @BindView(R.id.textViewCountry)
    TextView textViewCountry;
    @BindView(R.id.textViewChangCountry)
    TextView textViewChangCountry;
    @BindView(R.id.layoutCountry)
    LinearLayout layoutCountry;
    @BindView(R.id.spinnerPart)
    SearchableSpinner spinnerPart;
    @BindView(R.id.layoutSelectTimeZone)
    RelativeLayout layoutSelectTimeZone;
    @BindView(R.id.textViewTimeZone)
    TextView textViewTimeZone;
    @BindView(R.id.textViewChangTimeZone)
    TextView textViewChangTimeZone;
    @BindView(R.id.layouttimeZone)
    LinearLayout layouttimeZone;


    private ArrayList<CountryData> countryDataArrayList;
    private ArrayList<TimeZoneData> timeZoneDataArrayList;

    private Context context;
    private View view;
    String type;
    String Country_Name;
    private Unbinder unbinder;
    private static final int CROP_INTENT_PERMISSION = 104;

    Uri mImageCaptureUri;
    private AccountPresenter accountPresenter;
    private int CategoryPosition = 0;
    private int TimeZonePosition = 0;

    public AccountFragment() {
        // Required empty public constructor
    }

    public static AccountFragment newInstance(String number) {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, number);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account, container, false);
        unbinder = ButterKnife.bind(this, view);
        accountPresenter = new AccountPresenter(this, context);
        initialize();
        accountPresenter.callTimeZoneList();
        accountPresenter.callCountryList();

        return view;
    }

    private void initialize() {
        User user = SharedPreference.getInstance(context).getUser();
        System.out.println("Access token - " + SharedPreference.getInstance(context).getUser().getAccessToken() + "\t");
        textViewUserId.setText(user.getUsername());

        if (null != user.getAvatar() && !user.getAvatar().equalsIgnoreCase("")) {
            Glide.with(context).load(user.getAvatar()).into(profileImage);
        }

        if (null != user.getFullname() && !user.getFullname().isEmpty()) {
            textViewFullName.setText(user.getFullname());
            textViewFullName.setTextColor(getResources().getColor(R.color.textColorBlack));
        } else {
            textViewFullName.setText("Enter Full name");
            textViewFullName.setTextColor(getResources().getColor(R.color.textColorGrey));
        }

        if (null != user.getNickname() && !user.getNickname().isEmpty()) {
            textViewNickName.setText(user.getNickname());
            textViewNickName.setTextColor(getResources().getColor(R.color.textColorBlack));
        } else {
            textViewNickName.setText("Enter Nick name");
            textViewNickName.setTextColor(getResources().getColor(R.color.textColorGrey));
        }

        if (null != user.getEmail() && !user.getEmail().isEmpty()) {
            layoutEmail.setVisibility(View.VISIBLE);
            tvEmailTABToAdd.setVisibility(View.GONE);
            textViewEmail.setText(user.getEmail());
        } else {
            layoutEmail.setVisibility(View.GONE);
            tvEmailTABToAdd.setVisibility(View.VISIBLE);
        }

        if (null != user.getPhone() && !user.getPhone().isEmpty()) {
            layoutMobileNumber.setVisibility(View.VISIBLE);
            tvMobileNumberTABToAdd.setVisibility(View.GONE);
            textViewMobileNumber.setText(user.getPhone());
        } else {
            layoutMobileNumber.setVisibility(View.GONE);
            tvMobileNumberTABToAdd.setVisibility(View.VISIBLE);
        }
        if (null != user.getTimeZone() && !user.getTimeZone().isEmpty()) {
            layoutCountry.setVisibility(View.VISIBLE);
            layoutSelectTimeZone.setVisibility(View.GONE);
            textViewTimeZone.setText(user.getTimeZone());
        } else {
            layoutCountry.setVisibility(View.GONE);
            layoutSelectTimeZone.setVisibility(View.VISIBLE);
        }

        if (null != user.getCountry() && !user.getCountry().isEmpty()) {
            layoutCountry.setVisibility(View.VISIBLE);
            layoutSelectCountry.setVisibility(View.GONE);
            textViewCountry.setText(user.getCountry());
        } else {
            layoutCountry.setVisibility(View.GONE);
            layoutSelectCountry.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        unbinder.unbind();
    }


    @OnClick({R.id.backButton, R.id.profileImage, R.id.CameraButton, R.id.relativeEditProfile, R.id.textViewFullName, R.id.textViewNickName,
            R.id.textViewChangeMobile, R.id.tvEmailTABToAdd, R.id.textViewChangeEmail, R.id.layoutVerifyGoogleAccount, R.id.layoutSelectCountry,
            R.id.textViewChangCountry, R.id.layoutSelectTimeZone, R.id.textViewChangTimeZone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                getActivity().onBackPressed();
                break;
            case R.id.relativeEditProfile:
            case R.id.CameraButton:
                if (Build.VERSION.SDK_INT >= M) {
                    if (checkCropIntentPermission())
                        openCropIntent();
                    else
                        requestCropIntentPermission();
                } else
                    openCropIntent();
                break;

            case R.id.textViewFullName:
                dialogName(SharedPreference.getInstance(context).getUser().getFullname());
                break;
            case R.id.textViewNickName:
                dialogEnterNickName(SharedPreference.getInstance(context).getUser().getNickname());
                break;
            case R.id.textViewChangeMobile:
                Intent i = new Intent(getActivity(), AccountVerifyActivity.class);
                i.putExtra("type", Constant.PHONE);
                startActivityForResult(i, REQUEST_NUMBER);
                break;
            case R.id.tvEmailTABToAdd:
            case R.id.textViewChangeEmail:
                break;
            case R.id.layoutVerifyGoogleAccount:
                break;
            case R.id.layoutSelectCountry:
                break;
            case R.id.textViewChangCountry:
                layoutCountry.setVisibility(View.GONE);
                layoutSelectCountry.setVisibility(View.VISIBLE);
                break;
            case R.id.layoutSelectTimeZone:
                break;
            case R.id.textViewChangTimeZone:
                layouttimeZone.setVisibility(View.GONE);
                layoutSelectTimeZone.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void dialogName(String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.layout_dialog_name);
        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        EditText editTextName = dialog.findViewById(R.id.editTextName);
        Button buttonSave = dialog.findViewById(R.id.buttonSave);
        ImageView buttonCancel = dialog.findViewById(R.id.buttonCancel);

        editTextName.setText(text);
        editTextName.requestFocus();

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editTextName.getWindowToken(), 0);
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextName.getText().toString().length() != 0) {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editTextName.getWindowToken(), 0);
                    UpdateAccountParameter parameter = new UpdateAccountParameter();
                    parameter.setFullname(editTextName.getText().toString().trim());
                    callUpdateAccountApi(parameter);
                    dialog.dismiss();

                }

            }
        });

    }

    private void dialogEnterNickName(String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.layout_dialog_enter_nick_name);
        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        EditText editTextNickName = dialog.findViewById(R.id.editTextNickName);
        Button buttonSave = dialog.findViewById(R.id.buttonSave);
        ImageView buttonCancel = dialog.findViewById(R.id.buttonCancel);
        editTextNickName.getText().toString();
        editTextNickName.setText(text);
        editTextNickName.requestFocus();

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editTextNickName.getWindowToken(), 0);
                dialog.dismiss();

            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextNickName.getText().toString().trim().length() != 0) {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editTextNickName.getWindowToken(), 0);
                    UpdateAccountParameter parameter = new UpdateAccountParameter();
                    parameter.setNickname(editTextNickName.getText().toString().trim());
                    callUpdateAccountApi(parameter);
                    dialog.dismiss();

                }
            }
        });
    }

    private void callUpdateAccountApi(UpdateAccountParameter parameter) {
        accountPresenter.callUpdateAccountApi(parameter);
    }


    private boolean checkCropIntentPermission() {
        int result1 = ContextCompat.checkSelfPermission(context, CAMERA);
        int result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result1 == PackageManager.PERMISSION_GRANTED
                && result2 == PackageManager.PERMISSION_GRANTED
                && result3 == PackageManager.PERMISSION_GRANTED;
    }

    private void openCropIntent() {
        try {
            CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .setMinCropResultSize(400, 400)
                    .start(context, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = M)
    private void requestCropIntentPermission() {
        requestPermissions(new String[]{CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, CROP_INTENT_PERMISSION);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CROP_INTENT_PERMISSION:
                if (grantResults.length > 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    openCropIntent();
                }
                break;

        }
    }


    public File getUserImageFile(Bitmap bitmap) {
        try {
            File f = new File(context.getCacheDir(), "images.png");
            f.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @SuppressLint("CheckResult")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK && null != data) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Bitmap productImageBitmap = null;
            try {
                productImageBitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), result.getUri());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (null != productImageBitmap) {
                profileImage.setImageBitmap(productImageBitmap);
                accountPresenter.callUpdateAccountImageApi(getUserImageFile(productImageBitmap));
            }
        }
        if (requestCode == REQUEST_NUMBER && resultCode == Activity.RESULT_OK && null != data) {
            String mobileOREmail = data.getStringExtra("key");
            String type = data.getStringExtra("type");
            UpdateAccountParameter parameter = new UpdateAccountParameter();
            if (null != type && type.equals("PHONE")) {
                parameter.setPhone(mobileOREmail);
                parameter.setPhoneCode(data.getStringExtra("code"));
                callUpdateAccountApi(parameter);

            } else {
                parameter.setEmail(mobileOREmail);
                callUpdateAccountApi(parameter);
            }
        }


    }


    @Override
    public void onSuccessUpdateAccount(UpdateAccountResponse accountResponse) {
        ((HomeActivity) context).showToast(accountResponse.getMessage());
        SharedPreference.getInstance(context).putUser(Constant.USER, accountResponse.getData());
        ApplicationSingleton.getInstance().getProfileUpdateCallback().onProfileUpdate();
        initialize();
    }

    @Override
    public void onSuccessAddTimeZoneList(TimezoneResponse timezoneResponse) {

        final ArrayList<String> stringArrayList = new ArrayList<>();
        timeZoneDataArrayList = new ArrayList<>();
        timeZoneDataArrayList.addAll(timezoneResponse.getData());
//        stringArrayList.add(0, "Select TimeZone");
        for (int i = 0; i < timeZoneDataArrayList.size(); i++) {
            stringArrayList.add(timeZoneDataArrayList.get(i).getTzone()+" "+ timeZoneDataArrayList.get(i).getOffsetValue());
        }
        spinnerPart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TimeZonePosition = position;
//                String Timezone = spinnerPart.getSelectedItem().toString();
                String Timezone = timeZoneDataArrayList.get(position).getOffsetValue();
                textViewTimeZone.setText(Timezone);
                UpdateAccountParameter parameter = new UpdateAccountParameter();
                parameter.setTimeZone(Timezone);
                callUpdateAccountApi(parameter);
                layouttimeZone.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, stringArrayList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerPart.setAdapter(adapter);

//        String timeZone = SharedPreference.getInstance(context).getUser().getTimeZone();
//        int position = 0;
//
////        if (null != timeZoneDataArrayList) {
////            if (null != timeZoneDataArrayList.get(position).getOffsetValue()) {
////                spinnerPart.setSelection(adapter.getPosition( timeZoneDataArrayList.get(position).getOffsetValue()));
////
////            }
////        }
//        if (null != timeZone && !timeZone.isEmpty()) {
////           layoutTimeZone.setVisibility(View.VISIBLE);
////           layoutSelectTimeZone.setVisibility(View.GONE);
//
//
//            for (int i = 0; i < stringArrayList.size(); i++) {
//
//                if (stringArrayList.get(i).equalsIgnoreCase(timeZone)) {
//                    position = i;
////                    spinnerPart.setSelection(adapter.getPosition(timeZoneDataArrayList.get(position).getOffsetValue()));
//                }
//            }
//
//            if (position == 0) {
//            }
//        }

    }

    @Override
    public void onSuccessAddCountryList(GetCountryResponse getCountryResponse) {
        final ArrayList<String> stringArrayList = new ArrayList<>();
        countryDataArrayList = new ArrayList<>();
        countryDataArrayList.addAll(getCountryResponse.getData());
//        stringArrayList.add(0, "Select Country First");
        for (int i = 0; i < countryDataArrayList.size(); i++) {
            stringArrayList.add(countryDataArrayList.get(i).getName());
        }
        spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CategoryPosition = position;
                String Country = spinnerCountry.getSelectedItem().toString();
                textViewCountry.setText(Country);
                UpdateAccountParameter parameter = new UpdateAccountParameter();
                parameter.setCountry(Country);
                callUpdateAccountApi(parameter);
                layoutCountry.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter adapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, stringArrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerCountry.setAdapter(adapter);

//        try {
//            if (null != SharedPreference.getInstance(context).getUser().getCountry()) {
//                String country = SharedPreference.getInstance(context).getUser().getCountry();
//                spinnerCountry.setSelection(adapter.getPosition(country));
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


//        if(stringArrayList.size() > 0){
//            for (int i = 0; i < stringArrayList.size(); i++) {
//                if (null != SharedPreference.getInstance(context).getUser().getCountry() &&
//                        SharedPreference.getInstance(context).getUser().getCountry().toLowerCase().equalsIgnoreCase(stringArrayList.get(i).toLowerCase()) ) {
//                    String CountryName = SharedPreference.getInstance(context).getUser().getCountry().toLowerCase();
//                    spinnerCountry.setSelection(adapter.getPosition(CountryName));
//                    break;
//                }
//            }
//        }


    }

    @Override
    public void onSuccessUpdateImage(UpdateAccountImageResponse accountImageResponse) {
        Glide.with(this).load(accountImageResponse.getData().getAvatarUrl()).into(profileImage);
        User user = SharedPreference.getInstance(context).getUser();
        user.setAvatar(accountImageResponse.getData().getAvatarUrl());
        SharedPreference.getInstance(context).putUser(Constant.USER, user);
        ApplicationSingleton.getInstance().getProfileUpdateCallback().onProfileUpdate();
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        SharedPreference.getInstance(context).setBoolean(Constant.IS_USER_LOGIN, false);
        Intent intent = new Intent(context, LoginThroughActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
        startActivity(intent);
        AppUtils.startFromRightToLeft(context);
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShowLoader() {
        ((HomeActivity) context).showLoader();
    }

    @Override
    public void onHideLoader() {
        ((HomeActivity) context).hideLoader();
    }


}
