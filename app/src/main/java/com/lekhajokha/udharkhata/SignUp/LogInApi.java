package com.lekhajokha.udharkhata.SignUp;



import com.lekhajokha.udharkhata.SignUp.model.GetSignUpResponse;
import com.lekhajokha.udharkhata.SignUp.model.SignUpParameter;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LogInApi {
    @POST("UserLogin")
    Call<GetSignUpResponse> callEnterMobileNumberApi(@Body SignUpParameter signUpParameter);

}
