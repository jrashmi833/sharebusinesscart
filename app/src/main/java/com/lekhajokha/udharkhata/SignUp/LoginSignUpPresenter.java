package com.lekhajokha.udharkhata.SignUp;

import android.content.Context;
import com.lekhajokha.udharkhata.SignUp.model.GetSignUpResponse;
import com.lekhajokha.udharkhata.SignUp.model.SignUpParameter;

import java.io.File;


/**
 * Created by lenovo on 3/20/2018.
 */

public class LoginSignUpPresenter implements LoginSignUpManagerCallback {
    private LoginSignUpView loginSignUpView;
    private LoginSignUpManager loginManager;
    private Context context;

    public LoginSignUpPresenter(LoginSignUpView loginView, Context context) {
        this.loginSignUpView = loginView;
        this.context = context;
        this.loginManager = new LoginSignUpManager( this, context );
    }

    public void callSendOtpApi(SignUpParameter signUpParameter, Context context) {
        loginSignUpView.onShowLoader();
        loginManager.callSendOtpApi( signUpParameter );

    }



//    public void callUpdateprofileImageApi(String token, File file, Context context) {
//        if (((BusinessProfileActivity) context).isInternetConneted()) {
//            loginSignUpView.onShowLoader();
//            loginManager.callImageUploadApi( token, file );
//        } else {
//            loginSignUpView.onError( context.getResources().getString( R.string.no_internet_connection) );
//        }
//    }


    @Override
    public void onSuccessSignUp(GetSignUpResponse getSignUpResponse) {
        loginSignUpView.onSuccessSignUp( getSignUpResponse );
        loginSignUpView.onHideLoader();
    }


    @Override
    public void onError(String errorMessage) {
        loginSignUpView.onHideLoader();
        loginSignUpView.onError( errorMessage );
    }

    @Override
    public void onTokenChangeError(String errorMessage) {
        loginSignUpView.onHideLoader();
        loginSignUpView.onTokenChangeError( errorMessage );
    }


}
