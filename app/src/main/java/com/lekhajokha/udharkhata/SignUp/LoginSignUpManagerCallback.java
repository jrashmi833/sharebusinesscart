package com.lekhajokha.udharkhata.SignUp;


import com.lekhajokha.udharkhata.SignUp.model.GetSignUpResponse;

/**
 * Created by lenovo on 3/20/2018.
 */

public interface LoginSignUpManagerCallback {
    void onSuccessSignUp(GetSignUpResponse getSignUpResponse);

    void onError(String errorMessage);
    void onTokenChangeError(String errorMessage);
}
