package com.lekhajokha.udharkhata.SignUp;


import com.lekhajokha.udharkhata.SignUp.model.GetSignUpResponse;
import com.lekhajokha.udharkhata.base.BaseInterface;

/**
 * Created by lenovo on 3/20/2018.
 */

public interface LoginSignUpView extends BaseInterface {
    void onSuccessSignUp(GetSignUpResponse getSignUpResponse);
    void onTokenChangeError(String errorMessage);
}
