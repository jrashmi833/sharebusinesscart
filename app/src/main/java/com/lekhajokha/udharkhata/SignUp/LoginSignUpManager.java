package com.lekhajokha.udharkhata.SignUp;


import android.content.Context;

import com.lekhajokha.udharkhata.R;
import com.lekhajokha.udharkhata.SignUp.model.GetSignUpResponse;
import com.lekhajokha.udharkhata.SignUp.model.SignUpParameter;
import com.lekhajokha.udharkhata.rest.ServiceGenerator;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 3/20/2018.
 */

public class LoginSignUpManager {
    private LoginSignUpManagerCallback loginManagerCallback;
    private Context context;

    LoginSignUpManager(LoginSignUpManagerCallback loginManagerCallback, Context context) {
        this.loginManagerCallback = loginManagerCallback;
        this.context = context;
    }

    void callSendOtpApi(SignUpParameter enterMobileNumberParamer){
        LogInApi loginApi = ServiceGenerator.createService(LogInApi.class);
        final Call<GetSignUpResponse> loginResponseCall = loginApi.callEnterMobileNumberApi(enterMobileNumberParamer);
        loginResponseCall.enqueue(new Callback<GetSignUpResponse>() {
            @Override
            public void onResponse(Call<GetSignUpResponse> call, Response<GetSignUpResponse> response) {
                if(null != response.body()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        loginManagerCallback.onSuccessSignUp(response.body());
                    } else {
                        if (response.body().getCode() == 400) {
                            loginManagerCallback.onTokenChangeError(response.body().getMessage());
                        } else {
                            loginManagerCallback.onError(response.body().getMessage());
                        }
                    }
                }else {
                    loginManagerCallback.onError(context.getResources().getString(R.string.opps_something_went_wrong));
                }


            }

            @Override
            public void onFailure(Call<GetSignUpResponse> call, Throwable t) {
                if(t instanceof IOException){
                    loginManagerCallback.onError(context.getResources().getString(R.string.no_internet_connection));
                }else {
                    loginManagerCallback.onError(context.getResources().getString(R.string.opps_something_went_wrong));
                }
            }
        });
    }



}
